<?php

/**
 * @file
 *
 * UW Student Fee Estimator install file.
 */

/**
 * List of variables used by module.
 *
 * @param bool $keys_only
 *    If TRUE resulting array will hold only keys, used in uninstall hook.
 *
 * @return array
 *    List (array) of variables used by this module.
 */
function uw_student_fee_estimator_variables($keys_only = FALSE) {
  $variables = [
    'uw_student_fee_estimator_form_published' => FALSE,
    'uw_student_fee_estimator_maintenance_mode_message' => t('Form is closed for submissions.'),
    'uw_student_fee_estimator_current_term' => '1209',
    'uw_student_fee_estimator_terms' => ['1209'],
    'uw_student_fee_estimator_export_path' => 'sfe_export',
    'uw_student_fee_estimator_pdf_path' => 'sfe_pdf',
    'uw_student_fee_estimator_total_rounding' => 1000,
    'uw_student_fee_estimator_pdf_signature_text' => "
Loron A. Pellowe, CPA, CMA
Manager, Student Financial Services
lpellowe@uwaterloo.ca
    ",
    'uw_student_fee_estimator_pdf_footer_text' => '519-888-4567 | uwaterloo.ca | 200 UNIVERSITY AVENUE WEST, WATERLOO, ON, CANADA N2L 3G1',
    'uw_student_fee_estimator_pdf_signature_image' => 'images/uw_student_estimator_signature_loron.png',
    'uw_student_fee_estimator_email_override_status' => FALSE,
    'uw_student_fee_estimator_email_override_email' => '',
    'uw_student_fee_estimator_email_subject' => 'University of Waterloo - Student Fee Estimate',
    'uw_student_fee_estimator_email_body' => 'Thank you for using the fee estimator. Your request number is @sub. You will find your PDF estimate letter attached.
If you have questions about the estimate, please email Student Financial Services (sfs@uwaterloo.ca)

Kind regards,

Student Financial Services
University of Waterloo
',
    'uw_student_fee_estimator_mode_ajax' => FALSE,
    'uw_student_fee_estimator_payment_option_canadian' => ['format' => 'uw_tf_basic', 'value' =>'<p>Please use one of our approved <a href="https://uwaterloo.ca/finance/student-financial-services/how-become-fees-arranged/payment-options" target="_blank">payment options</a> to make your payment to University of Waterloo.</p>'],
    'uw_student_fee_estimator_payment_option_international' => ['format' => 'uw_tf_basic', 'value' =>'<p>We have authorized two <a href="http://uwaterloo.ca/finance/student-financial-services/how-become-fees-arranged/payment-options/making-payment-bank-outside-canada" target="_blank">payment options</a> for incoming international student payments to University of Waterloo. Please use the bank instructions provided by the GlobalPay for Students or International Student Pay process to make your payment.</p>'],
    'uw_student_fee_estimator_email_from' => 'sfs@uwaterloo.ca',
    'uw_student_fee_estimator_thank_you_url' => '',
  ];

  return $keys_only ? array_keys($variables) : $variables;
}

/**
 * Implements hook_schema().
 */
function uw_student_fee_estimator_schema() {
  $schema = [];

  $schema['uw_student_fee_estimator_residence'] = [
    'description' => 'Student fee estimator residence costs.',
    'fields' => [
      'rid' => [
        'description' => 'Residence identifier, automatically assigned.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'career' => [
        'description' => 'Career path (UG or GRD).',
        'type' => 'varchar',
        'length' => 5,
        'not null' => TRUE,
        'default' => '',
      ],
      'name' => [
        'description' => 'Name of a residence.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'price' => [
        'description' => 'Cost amount per year (two terms) in Canadian dollars. Value stored in cents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'meal_plan_included' => [
        'description' => 'Boolean indicating whether meal plan is included in the price of residence.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'meal_plan_required' => [
        'description' => 'Boolean indicating whether meal plan is required in this residence.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'created' => [
        'description' => 'The Unix timestamp when the residence record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'updated' => [
        'description' => 'The Unix timestamp when the residence record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'status' => [
        'description' => 'Boolean indicating whether the content is published (visible to non-administrators).',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ],
    ],
    'indexes' => [
      'status' => ['status'],
      'meal_plan_included' => ['meal_plan_included'],
    ],
    'primary key' => ['rid'],
  ];

  $schema['uw_student_fee_estimator_mealplan'] = [
    'description' => 'Student fee estimator meal plan costs.',
    'fields' => [
      'mid' => [
        'description' => 'Meal plan identifier, automatically assigned.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'name' => [
        'description' => 'Name of a meal plan.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'price' => [
        'description' => 'Cost amount per year (two terms) in Canadian dollars. Value stored in cents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'created' => [
        'description' => 'The Unix timestamp when the meal plan record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'updated' => [
        'description' => 'The Unix timestamp when the meal plan record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'status' => [
        'description' => 'Boolean indicating whether the content is published (visible to non-administrators).',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ],
    ],
    'indexes' => [
      'status' => ['status'],
    ],
    'primary key' => ['mid'],
  ];

  $schema['uw_student_fee_estimator_resmeal'] = [
    'description' => 'Connecting residence and meal plans.',
    'fields' => [
      'mid' => [
        'description' => 'Meal plan identifier, automatically assigned.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'rid' => [
        'description' => 'Residence identifier, automatically assigned.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'status' => [
        'description' => 'Boolean indicating whether the content is published.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ],
      'created' => [
        'description' => 'The Unix timestamp when a record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['mid', 'rid',],
    'foreign keys' => [
      'residence_rid' => [
        'table' => 'uw_student_fee_estimator_residence',
        'columns' => ['rid' => 'rid'],
      ],
      'mealplan_mid' => [
        'table' => 'uw_student_fee_estimator_mealplan',
        'columns' => ['mid' => 'mid'],
      ],
    ],
  ];

  $schema['uw_student_fee_estimator_tuition'] = [
    'description' => 'Tuition record',
    'fields' => [
      'tid' => [
        'description' => 'Tuition unique identifier, automatically assigned.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'online' => [
        'description' => 'Is program online.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ],
      'calc_formula' => [
        'description' => 'Which formula to use when calculating fees.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
        'default' => 'default',
      ],
      'term_code' => [
        'description' => 'UW Term code',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
      ],
      'program_description' => [
        'description' => 'Short description of a program.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'career' => [
        'description' => 'Career path (UG or GRD).',
        'type' => 'varchar',
        'length' => 5,
        'not null' => TRUE,
      ],
      'academic_load' => [
        'description' => 'Academic load (full time or part time) for GRD.',
        'type' => 'varchar',
        'length' => 50,
        'default' => '',
      ],
      'form_of_study' => [
        'description' => 'Form of study (regular or co-op) for UG.',
        'type' => 'varchar',
        'length' => 50,
        'default' => '',
      ],
      'increase' => [
        'description' => 'Percentage of assumed increase.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'grd_type' => [
        'description' => 'Type of GRD program',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'citizenship' => [
        'description' => 'Legal status of person attending program.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'default' => '',
      ],
      'per_term' => [
        'description' => 'For GRD per term fee, in cents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'per_course' => [
        'description' => 'For GRD per course fee, in cents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'per_term_inc' => [
        'description' => 'For GRD per term incidental fee, in cents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'work_term_per_course' => [
        'description' => 'Per course fee in work term, in cents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'work_term_inc' => [
        'description' => 'Incremental fee for per course in work term, value in cents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'T_total' => [
        'description' => 'Tuition costs for whole program. Value in cents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'IF_total' => [
        'description' => 'Sum of all incidental fees for the program. Value in cents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'WRM_total' => [
        'description' => 'Sum of all WRM fees for the program. Value in cents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'created' => [
        'description' => 'The Unix timestamp when the tuition record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'updated' => [
        'description' => 'The Unix timestamp when the tuition record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'status' => [
        'description' => 'Boolean indicating whether the content is published (visible to non-administrators).',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'pdf_template' => [
        'description' => 'PDF Template id that will override default template. ',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],

    ],
    'primary key' => ['tid'],
    'indexes' => [
      'status' => ['status'],
      'citizenship' => ['citizenship'],
      'term_code' => ['term_code'],
      'career' => ['career'],
      'academic_load' => ['academic_load'],
      'form_of_study' => ['form_of_study'],
    ],
  ];

  // Each year has A or B designated term. Therefore 1A and 1B are terms
  // for first year. Empty string is used to store year total (1 = 1A + 1B)
  // This will generate following for first year:
  // T1 - tuition total (combined T1A + T1B)
  // IF1, IF1A, IF1B represent incidental fees per year/terms
  // WMR1, WRM1A, WRM1B are co-op fees per year/term
  // PC1, PC1A, PC1B, per course fees.
  foreach (range(1, 5) as $term) {
    foreach (['', 'A', 'B'] as $code) {
      foreach (['T' => 'Tuition costs per term', 'IF' => 'Incidental fees per term', 'WRM' => 'WRM fees per term', 'PC' => 'Per course fees per term.'] as $group => $label) {
        $schema['uw_student_fee_estimator_tuition']['fields'][$group . $term . $code] = [
          'description' => $label,
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ];
      }
    }
  }

  $schema['uw_student_fee_estimator_submission'] = [
    'description' => 'Submission records',
    'fields' => [
      'sid' => [
        'description' => 'Submission unique identifier, automatically assigned.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'term_code' => [
        'description' => 'UW Term code',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
      ],
      'name' => [
        'description' => 'Full name of person requesting estimate.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'first_name' => [
        'description' => 'First name of person requesting estimate.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'last_name' => [
        'description' => 'Last name of person requesting estimate.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'email' => [
        'description' => 'Email of person requesting estimate.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'student_number' => [
        'description' => 'Student number of person requesting estimate.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
        'default' => NULL,
      ],
      'student_career' => [
        'description' => 'Academic career of person requesting estimate.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'mid' => [
        'description' => 'Meal plan identifier.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
      ],
      'rid' => [
        'description' => 'Residence identifier.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
      ],
      'citizenship' => [
        'description' => 'Legal status of person attending program.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'tid' => [
        'description' => 'Tuition identifier.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'requested_interval' => [
        'description' => 'Interval for estimate, it could be term, year, or whole program.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'pdf_file' => [
        'description' => 'URI of generated PDF file in public namespace.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => NULL,
      ],
      'notification' => [
        'description' => 'The Unix timestamp when email notification was sent.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'form_of_study' => [
        'description' => 'For UG students, available options are regular (REG) and co-op (CO-OP) studies.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ],
      'academic_load' => [
        'description' => 'For GRD students, available options are full time (FT) and part time (PT).',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ],
      'number_of_courses' => [
        'description' => 'Some requested intervals require student to provide number of courses in order to calculate estimate.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'number_of_terms' => [
        'description' => 'Some requested intervals require student to provide number of terms for selected program.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'created' => [
        'description' => 'The Unix timestamp when the submission record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'updated' => [
        'description' => 'The Unix timestamp when the submission record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'status' => [
        'description' => 'Boolean indicating whether the content is published (visible to non-administrators).',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ],
    ],
    'primary key' => ['sid'],
    'indexes' => [
      'status' => ['status'],
      'tid' => ['tid'],
      'student_number' => ['student_number'],
      'term_code' => ['term_code'],
    ],
  ];

  $schema['uw_student_fee_estimator_pdf_template'] = [
    'description' => 'PDF file templates',
    'fields' => [
      'eid' => [
        'description' => 'PDF template unique identifier, automatically assigned.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'name' => [
        'description' => 'Template name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'template' => [
        'description' => 'Body of PDF template',
        'type' => 'text',
        'not null' => TRUE,
      ],
      'created' => [
        'description' => 'The Unix timestamp when the PDF template record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'updated' => [
        'description' => 'The Unix timestamp when the PDF template record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'status' => [
        'description' => 'Boolean indicating whether the content is published (visible to non-administrators).',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ],
      'default_template' => [
        'description' => 'Boolean indicating whether the template is default for specific citizenship level.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['eid'],
    'indexes' => [
      'status' => ['status'],
      'default_template' => ['default_template'],
      'name' => ['name'],
    ],
  ];

  return $schema;
}

/**
 * Implements hook_install().
 */
function uw_student_fee_estimator_install() {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/pdf/uw_student_fee_estimator_pdf_templates');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $used_variables = uw_student_fee_estimator_variables();

  foreach ($used_variables as $used_variable => $value) {
    variable_set($used_variable, $value);
  }

  $template = [
    'default_template' => 1,
    'template' => uw_student_fee_estimator_pdf_template_domestic(),
    'status' => 1,
    'name' => 'Default',
  ];

  _uw_student_fee_estimator_pdf_template_merge($template);

  // Create vocabulary.
  $grd_type = new stdClass();
  $grd_type->name = 'UW Student Fee Estimator GRD Program Types';
  $grd_type->description = 'Vocabulary to hold all graduate program types/classes for UW Student Fee Estimator module.';
  $grd_type->machine_name = 'uw_student_fee_estimator_grd_program_types';

  taxonomy_vocabulary_save($grd_type);

  $module_roles = ['UW Student Fee Administrator'];

  foreach ($module_roles as $new_role_name) {
    if (!user_role_load_by_name($new_role_name)) {
      $role = new stdClass();
      $role->name = $new_role_name;

      user_role_save($role);

      if (!isset($role->rid)) {
        $role = user_role_load_by_name($new_role_name);
      }

      if ($role->rid) {
        $permissions[] = 'administer student fee estimator';
        $permissions[] = 'use text format uw_tf_standard';
        $permissions[] = 'access navbar';
        $permissions[] = 'select navbar language';
        $permissions[] = 'access administration pages';
        $permissions[] = 'view the administration theme';
        $permissions[] = 'edit terms in ' . $grd_type->vid;
        $permissions[] = 'delete terms in ' . $grd_type->vid;

        user_role_grant_permissions($role->rid, $permissions);
      }
    }
  }

  // Add captcha/recaptcha if module exists.
  if (module_exists('captcha') && db_table_exists('captcha_points')) {
    db_merge('captcha_points')
      ->key(['form_id' => 'uw_student_fee_estimator_form_wizard'])
      ->fields([
        'module' => NULL,
        'captcha_type' => 'default',
      ])
      ->execute();
  }
}

/**
 * Implements hook_uninstall().
 *
 * Removes module variables, views, roles.
 *
 * @throws \Exception
 */
function uw_student_fee_estimator_uninstall() {
  // Delete views created by this module.
  $delete_views = [
    'uw_student_fee_estimator_meals',
    'uw_student_fee_estimator_residences',
    'uw_student_fee_estimator_tuition',
    'uw_student_fee_estimator_submissions',
    'uw_student_fee_estimator_pdf_templates',
  ];

  foreach ($delete_views as $delete_view) {
    if ($view = views_get_view($delete_view)) {
      views_delete_view($view);
    }
  }

  $used_variables = uw_student_fee_estimator_variables(TRUE);

  foreach ($used_variables as $used_variable) {
    variable_del($used_variable);
  }

  // Removing vocabulary and terms.
  $grd_types = taxonomy_vocabulary_machine_name_load('uw_student_fee_estimator_grd_program_types');
  taxonomy_vocabulary_delete($grd_types->vid);

  $module_roles = ['UW Student Fee Administrator'];

  foreach ($module_roles as $role) {
    user_role_delete($role);
  }

  // Remove captcha configuration if used
  if (module_exists('captcha') && db_table_exists('captcha_points')) {
    db_delete('captcha_points')
      ->condition('form_id', 'uw_student_fee_estimator_form_wizard')
      ->execute();
  }
}

/**
 * Adds first_name and last_name fields to submission table. Adds career field
 * to residence table.
 */
function uw_student_fee_estimator_update_7100() {
  $first_name = [
    'description' => 'First name of person requesting estimate.',
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
    'default' => '',
  ];

  $last_name = [
    'description' => 'Last name of person requesting estimate.',
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
    'default' => '',
  ];

  $career = [
    'description' => 'Career path (UG or GRD).',
    'type' => 'varchar',
    'length' => 5,
    'not null' => TRUE,
    'default' => '',
  ];

  db_add_field('uw_student_fee_estimator_submission', 'first_name', $first_name);
  db_add_field('uw_student_fee_estimator_submission', 'last_name', $last_name);
  db_add_field('uw_student_fee_estimator_residence', 'career', $career);

  // Setting rounding to 1000.
  variable_set('uw_student_fee_estimator_total_rounding', '1000');

  // Setting from email address to new value.
  variable_set('uw_student_fee_estimator_email_from', 'sfs@uwateroo.ca');
}

/**
 * Adds online field to program table.
 */
function uw_student_fee_estimator_update_7101() {
  $field = [
    'description' => 'Is program online.',
    'type' => 'int',
    'not null' => TRUE,
    'default' => 0,
    'size' => 'tiny',
  ];

  if (!db_field_exists('uw_student_fee_estimator_tuition', 'online')) {
    db_add_field('uw_student_fee_estimator_tuition', 'online', $field);
  }
}

/**
 * Adds formula field to program fees.
 */
function uw_student_fee_estimator_update_7102() {
  $calc_formula = [
    'description' => 'Which formula to use when calculating fees.',
    'type' => 'varchar',
    'length' => 20,
    'not null' => TRUE,
    'default' => 'default',
  ];

  if (!db_field_exists('uw_student_fee_estimator_tuition', 'calc_formula')) {
    db_add_field('uw_student_fee_estimator_tuition', 'calc_formula', $calc_formula);
  }
}
