<?php

/**
 * @file
 * Database related operations.
 */

/**
 * Load submission database record.
 *
 * @param array|string|null $sid
 *   Submission id.
 * @param array|null $options
 *   Additional options.
 *
 * @return array|mixed
 *   Loaded submissions.
 */
function uw_student_fee_estimator_load_submission($sid = NULL, array $options = []) {
  if ($sid === 0 || $sid === '0') {
    return [];
  }

  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_SUBMISSIONS,
    'sort_key' => 'sid',
  ] + $options;

  if ($sid) {
    $input['fields'] = ['sid' => $sid];
    $input['reset'] = TRUE;
    $input['current_term'] = FALSE;
  }

  $submission_data = _uw_student_fee_estimator_load_record($input);

  return $submission_data;
}

/**
 * Load residence database record.
 *
 * @param array|string|null $rid
 *   Residence id.
 * @param array|null $options
 *   Additional options.
 *
 * @return array|mixed
 *   Residence array.
 */
function uw_student_fee_estimator_load_residence($rid = NULL, array $options = []) {
  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESIDENCES,
    'sort_key' => 'rid',
  ] + $options;

  if ($rid) {
    $input['fields'] = ['rid' => $rid];
    $input['reset'] = TRUE;
  }

  return _uw_student_fee_estimator_load_record($input);
}

/**
 * Load meal plan database record.
 *
 * @param null|array|string $mid
 *   Meal plan id.
 * @param array|null $options
 *   Additional options as array.
 * @param bool $reset
 *   If one record is retrieved, use it as array.
 *
 * @return array|mixed
 *   Meal plan record.
 */
function uw_student_fee_estimator_load_meal_plan($mid = NULL, array $options = [], $reset = TRUE) {
  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_MEALS,
    'sort_key' => 'mid',
    'reset' => $reset,
  ] + $options;

  if ($mid) {
    $input['fields'] = ['mid' => $mid];
    $input['current_term'] = FALSE;
  }

  return _uw_student_fee_estimator_load_record($input);
}

/**
 * Load tuition record based on provided tuition id.
 *
 * @param array|string|null $tid
 *   Tuition record database id.
 * @param array|null $options
 *   Additional options.
 *
 * @return array|mixed|null
 *   Result tuition record or NULL.
 */
function uw_student_fee_estimator_load_tuition($tid = NULL, array $options = []) {
  if ($tid === 0 || $tid === '0') {
    return [];
  }

  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION,
    'sort_key' => 'tid',
  ] + $options;

  if ($tid) {
    $input['fields'] = ['tid' => $tid];
    $input['reset'] = TRUE;
    $input['current_term'] = FALSE;
  }

  $tuition_data = _uw_student_fee_estimator_load_record($input);

  return $tuition_data;
}

/**
 * PDF Template load function on provided template id (eid).
 *
 * @param array|string|null $eid
 *   Template id.
 * @param array|null $options
 *   Additional options.
 *
 * @return array|mixed
 *   PDF Template value.
 */
function uw_student_fee_estimator_load_pdf_template($eid = NULL, array $options = []) {
  if ($eid === 0 || $eid === '0') {
    return [];
  }

  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_PDF_TEMPLATE,
    'sort_key' => 'eid',
    'reset' => TRUE,
  ] + $options;

  if ($eid) {
    $input['fields'] = ['eid' => $eid];
    $input['current_term'] = FALSE;
  }

  $template = _uw_student_fee_estimator_load_record($input);

  return $template;
}

/**
 * Load database records based on provided params.
 *
 * This is generic function, that can be used for loading,
 * but there are entity specific functions that load data for records.
 *
 * @param array $args
 *   All arguments are passed in array.
 *
 * @return mixed|array
 *   Result as array|object|null.
 *    Parameters explained:
 *    - "table" (required) - name of database table, tables in this
 *        modules are constants.
 *    - "sort_key" (optional) - database field name, result will be
 *        associative array with sort_key as array key.
 *    - "fields" (optional) - conditional fields passed as assoc array,
 *        if value is an array IN clause will be used.
 *    - "limit" (optional) - limit result set to a specific number,
 *        used with combination with random.
 *    - "reset" (optional) - if there is only one result, instead of
 *        returning assoc array return record if TRUE.
 *    - "random" (optional) - apply random order or results, this has been
 *        used by random record generator (development file).
 *    - "status" (optional) - if TRUE, result set will include only
 *        published records (with status 1).
 *    Example use case:
 *    $residences = _uw_student_fee_estimator_load_record([
 *    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESIDENCES,
 *    'sort_key' => 'rid',
 *    ]).
 *    Above example will load all residence records regardless of
 *    status (published or unpublished), and result will be associated
 *    array where key is 'rid' and value is array database record.
 */
function _uw_student_fee_estimator_load_record(array $args) {
  // Setting default values in case there are not provided,
  // so I don't have to check if there are set or not.
  $args += [
    'sort_key' => FALSE,
    'fields' => NULL,
    'limit' => 0,
    'reset' => TRUE,
    'random' => FALSE,
    'status' => FALSE,
    'current_term' => TRUE,
    'always_array' => FALSE,
  ];

  if (!isset($args['table'])) {
    throw new InvalidArgumentException('table is required argument.');
  }

  // Not using extract($args) because of better code readability without it.
  $fields = $args['fields'];
  $limit = $args['limit'];
  $random = $args['random'];
  // Reset result if count is 1. FALSE will always return array.
  $reset = $args['reset'];
  $sort_key = $args['sort_key'];
  $status = $args['status'];
  $current_term = $args['current_term'];
  $term_code = isset($args['fields']['term_code']) ? $args['fields']['term_code'] : FALSE;
  $always_array = $args['always_array'];

  $query = db_select($args['table'], 't')->fields('t');

  if ($fields) {
    foreach ($fields as $field_name => $field_value) {
      $query->condition($field_name, $field_value, is_array($field_value) ? 'IN' : '=');
    }

    // Applies only to submissions and tuition records. Since other tables
    // don't have term code.
    if (in_array($args['table'], [UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION, UW_STUDENT_FEE_ESTIMATOR_TABLE_SUBMISSIONS])) {
      // If term code is not provided in fields, make sure to use current
      // term code, to ensure only one records be loaded.
      if ($term_code) {
        $query->condition('term_code', $term_code);
      }
      elseif ($current_term) {
        $query->condition('term_code',
          variable_get('uw_student_fee_estimator_current_term'));
      }
    }
  }

  if ($status) {
    $query->condition('status', $status);
  }

  if ($random) {
    $query->orderRandom();
  }

  if ($limit !== 0 && is_numeric($limit)) {
    $query->range(0, $limit);
  }

  if ($sort_key) {
    $results = $query->execute()->fetchAllAssoc($sort_key, PDO::FETCH_ASSOC);
  }
  else {
    $results = $query->execute()->fetchAll();
  }

  if (count($results) === 1 && $reset && !$always_array) {
    // Using reset since we don't know the key.
    $results = reset($results);
  }

  return $results;
}

/**
 * Merges (updates or inserts) data for entity.
 *
 * @param array $values
 *   Record to merge.
 *
 * @return int|string
 *   Assigned id, or number of updated records.
 */
function _uw_student_fee_estimator_merge_record(array $values) {
  try {
    _uw_student_fee_estimator_merge_record_validate_input($values);

    $table = $values['table'];
    $keys = isset($values['keys']) ? $values['keys'] : NULL;
    $fields = $values['fields'];
    $conditions = isset($values['conditions']) ? $values['conditions'] : NULL;

    if (empty($fields['term_code']) && $table !== UW_STUDENT_FEE_ESTIMATOR_TABLE_PDF_TEMPLATE) {
      $term = variable_get('uw_student_fee_estimator_current_term');

      if ($term) {
        $fields['term_code'] = $term;
      }
    }

    // Get schema and do array intersect to validate fields from input.
    $schema_definition = drupal_get_schema_unprocessed('uw_student_fee_estimator', $table);
    $schema_fields = $schema_definition['fields'];
    $fields['updated'] = REQUEST_TIME;

    $valid_fields = array_intersect_key($fields, $schema_fields);

    if ($keys) {
      $query = db_merge($table)->key($keys);
    }
    elseif ($conditions) {
      $query = db_update($table);
    }
    else {
      $query = db_insert($table);
      $valid_fields['created'] = REQUEST_TIME;
    }

    $query->fields($valid_fields);
    if ($conditions) {
      foreach ($conditions as $condition_key => $condition_value) {
        $query->condition($condition_key, $condition_value);
      }
    }

    $result = $query->execute();
  }
  catch (Exception $ex) {
    drupal_set_message(t('There was an error while creating/updating a record. Check logs for more details.'), 'error');
    watchdog(
      'uw_student_fee_estimator',
      'Exception during insert or update: @error',
      ['@error' => $ex->getMessage()],
      WATCHDOG_CRITICAL
    );
    $result = 0;
  }

  return $result;
}

/**
 * Validates input params for it to be used in load/merge generic functions.
 *
 * @param array $values
 *   Input parameter as array to be validated.
 * @param bool $fields_required
 *   Conditional fields for validation.
 *
 * @throws \Exception
 *    If any issue is detected with data exception will be thrown.
 */
function _uw_student_fee_estimator_merge_record_validate_input(array $values, $fields_required = TRUE) {
  $table = isset($values['table']) ? $values['table'] : NULL;
  $keys = isset($values['keys']) ? $values['keys'] : NULL;
  $fields = isset($values['fields']) ? $values['fields'] : NULL;
  $conditions = isset($values['conditions']) ? $values['conditions'] : NULL;

  if (($fields_required && !$fields) || !$table) {
    throw new Exception(t('Missing required parts of merge record'));
  }

  if (!db_table_exists($table) || !is_string($table) || strpos($table, 'uw_student_fee_estimator_') !== 0) {
    throw new Exception(t('Wrong table name provided. Make sure table names are correct.'));
  }

  if (($fields_required && !is_array($fields))) {
    throw new Exception(t('Data format provided is incorrect. Please update.'));
  }

  if ($keys) {
    if (!is_array($keys)) {
      throw new Exception(t('Data format provided for keys is incorrect. Please update.'));
    }

    if (count($keys) !== count(array_filter(array_keys($keys), 'is_string'))) {
      throw new Exception(t('Passed array of keys must be associative array.'));
    }
  }

  if ($conditions) {
    if (!is_array($conditions)) {
      throw new Exception(t('Data format provided for conditions is incorrect. Please update.'));
    }

    _uw_student_fee_estimator_merge_record_validate_input_string($conditions, t('Passed array of conditions must be associative array.'));
  }

  _uw_student_fee_estimator_merge_record_validate_input_string($values, t('Passed array of values must be associative array.'));

  if ($fields_required) {
    _uw_student_fee_estimator_merge_record_validate_input_string($fields, t('Passed array of fields must be associative array.'));
  }

}

/**
 * Validates input fields.
 *
 * @param array $values
 *   Array of values.
 * @param string $error_message
 *   Default error message.
 *
 * @throws \Exception
 *    Exception when array fields are not string types.
 */
function _uw_student_fee_estimator_merge_record_validate_input_string(array $values, $error_message) {
  if (!empty($values) && (count($values) !== count(array_filter(array_keys($values), 'is_string')))) {
    throw new Exception($error_message);
  }
}

/**
 * Central function for deletion, all deleting function should invoke it.
 *
 * @param array $values
 *   Params as array with values to specify which entity record to delete.
 *
 * @return bool
 *   Returns TRUE on successful deletion, FALSE otherwise.
 */
function _uw_student_fee_estimator_database_delete_record(array $values) {
  try {
    _uw_student_fee_estimator_merge_record_validate_input($values, FALSE);

    $table = $values['table'];
    $conditions = isset($values['conditions']) ? $values['conditions'] : NULL;

    $query = db_delete($table);

    if ($conditions) {
      foreach ($conditions as $condition_key => $condition_value) {
        $query->condition($condition_key, $condition_value);
      }
    }

    $result = $query->execute();
  }
  catch (Exception $ex) {
    drupal_set_message(t('There was an error while deleting a record. Check logs for more details.'), 'error');
    watchdog(
      'uw_student_fee_estimator',
      'Exception during delete: @error',
      ['error' => $ex->getMessage()],
      WATCHDOG_CRITICAL);
    $result = FALSE;
  }

  return $result;
}

/**
 * Deleting all records.
 *
 * @return int
 *   Number of deleted records.
 */
function _uw_student_fee_estimator_delete_all_records() {
  // Create an order which to delete records.
  $tables = [
    UW_STUDENT_FEE_ESTIMATOR_TABLE_SUBMISSIONS,
    UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION,
    UW_STUDENT_FEE_ESTIMATOR_TABLE_RESMEAL,
    UW_STUDENT_FEE_ESTIMATOR_TABLE_MEALS,
    UW_STUDENT_FEE_ESTIMATOR_TABLE_RESIDENCES,
  ];

  $deleted = 0;

  try {
    foreach ($tables as $table) {
      $deleted += db_delete($table)->execute();
    }
  }
  catch (Exception $ex) {
    drupal_set_message(t('There was an error while deleting all submission records. Check logs for more details.'), 'error');
    watchdog(
      'uw_student_fee_estimator',
      'Exception during delete all records: @error',
      ['@error' => $ex->getMessage()],
      WATCHDOG_CRITICAL
    );
  }

  return $deleted;
}

/**
 * Submission form program load.
 *
 * @param string $career
 *   Student career.
 * @param string $citizenship
 *   Student citizenship.
 * @param array|null $options
 *   Additional options.
 *
 * @return array
 *   Returns array has tid => program_description (note: not PID).
 */
function _uw_student_fee_estimator_program_select($career, $citizenship, array $options = NULL) {
  $programs = [];
  $current_term = variable_get('uw_student_fee_estimator_current_term');

  // Create program/tuition join, make sure to use career level and previously
  // created conditions, do left join on GRD programs to get sort and group name.
  $query = db_select(UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION, 't');
  $query->fields('t', ['tid', 'program_description']);

  if ($career === 'GRD') {
    $query->join('taxonomy_term_data', 'x', 't.grd_type = x.tid');
    $query->fields('x', ['weight', 'name', 'tid']);
  }

  $query->condition('t.career', $career)
    ->condition('t.citizenship', $citizenship);

  if ($current_term) {
    $query->condition('t.term_code', $current_term);
  }

  if (!empty($options)) {
    foreach ($options as $key => $value) {
      $query->condition('t.' . $key, $value);
    }
  }

  // Adding sort by taxonomy weight and by term name only for GRD.
  if ($career === 'GRD') {
    $query->orderBy('x.weight', 'ASC');
  }

  $query->orderBy('t.program_description', 'ASC');

  $result = $query->execute()->fetchAllAssoc('tid', PDO::FETCH_ASSOC);

  if ($result) {
    foreach ($result as $tid => $program) {
      $programs[$tid] = t('@name', ['@name' => $program['program_description']]);
    }
  }

  return $programs;
}

/**
 * Meal plan delete, that removes resmeals records all well.
 *
 * @param string $mid
 *   Meal plan id.
 *
 * @return bool
 *   TRUE on successful delete.
 */
function uw_student_fee_estimator_meals_delete($mid) {
  $resmeals = _uw_student_fee_estimator_database_delete_record([
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESMEAL,
    'conditions' => [
      'mid' => $mid,
    ],
  ]);

  $meals = _uw_student_fee_estimator_database_delete_record([
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_MEALS,
    'conditions' => [
      'mid' => $mid,
    ],
  ]);

  return $resmeals && $meals;
}

/**
 * Residence delete that also deletes records from resmeal table.
 *
 * @param string $rid
 *   Residence id.
 *
 * @return bool
 *   Outcome of action.
 */
function uw_student_fee_estimator_residences_delete($rid) {
  $resmeals = _uw_student_fee_estimator_database_delete_record([
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESMEAL,
    'conditions' => [
      'rid' => $rid,
    ],
  ]);

  $residences = _uw_student_fee_estimator_database_delete_record([
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESIDENCES,
    'conditions' => [
      'rid' => $rid,
    ],
  ]);

  return $resmeals && $residences;
}

/**
 * Delete tuition database record.
 *
 * @param string $tid
 *   Tuition id.
 *
 * @return bool
 *   Was action successful when deleting.
 */
function uw_student_fee_estimator_tuition_delete($tid) {
  $result = FALSE;

  if ($tid && is_numeric($tid)) {
    $result = _uw_student_fee_estimator_database_delete_record([
      'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION,
      'conditions' => [
        'tid' => $tid,
      ],
    ]);
  }

  return $result;
}

/**
 * Delete template database record.
 *
 * @param string $eid
 *   Template id.
 *
 * @return bool
 *   Was action successful when deleting.
 */
function uw_student_fee_estimator_pdf_template_delete($eid) {
  $result = FALSE;

  if ($eid && is_numeric($eid)) {
    $result = _uw_student_fee_estimator_database_delete_record([
      'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_PDF_TEMPLATE,
      'conditions' => [
        'eid' => $eid,
      ],
    ]);
  }

  return $result;
}

/**
 * Deletes all tuition records.
 *
 * @param string $table
 *   Table name.
 *
 * @param string $career
 *   Conditions for query as associative array.
 *
 * @return int
 *   Number of records deleted.
 */
function uw_student_fee_estimator_delete_table($table, $career = NULL) {
  $result = 0;

  try {
    if (db_table_exists($table) && strpos($table, 'uw_student_fee_estimator') === 0) {
      if ($career) {
        $result += db_delete($table)->condition('career', $career)->execute();
      }
      else {
        $result += db_delete($table)->execute();
      }
    }
  }
  catch (Exception $ex) {
    drupal_set_message(t('There was an error while deleting records. Check logs for more details.'), 'error');
    watchdog(
      'uw_student_fee_estimator',
      'Exception during delete: @error',
      ['@error' => $ex->getMessage()],
      WATCHDOG_CRITICAL);
  }

  return $result;
}

/**
 * Counts records for generator overview.
 *
 * @param array $tables
 *   List of tables to get count.
 *
 * @return mixed
 *   Returns count on how many records are deleted.
 */
function _uw_student_fee_estimator_database_record_count(array $tables) {
  $result = [];

  foreach ($tables as $table) {
    $count = 0;

    if (db_table_exists($table)) {
      $count = db_select($table, 't')
        ->fields('t')
        ->condition('status', 1)
        ->countQuery()
        ->execute()
        ->fetchField();
    }

    $result[$table] = $count;
  }

  return $result;
}

/**
 * Meal plan merge (insert or update) of database record.
 *
 * @param array $meal_plan
 *   Meal plan data.
 *
 * @return bool
 *   TRUE if merge was successful.
 */
function _uw_student_fee_estimator_mealplan_merge(array $meal_plan) {
  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_MEALS,
    'fields' => [
      'name' => $meal_plan['name'],
      'price' => $meal_plan['price'],
      'updated' => REQUEST_TIME,
      'status' => $meal_plan['status'],
    ],
    'keys' => [
      'mid' => $meal_plan['mid'],
    ],
  ];

  if (isset($meal_plan['created']) && $meal_plan['created']) {
    $input['fields']['created'] = REQUEST_TIME;
  }

  $result = _uw_student_fee_estimator_merge_record($input);

  // Insert meals plans, but first flag all previous
  // and later delete not used ones.
  if (isset($meal_plan['mid']) && $meal_plan['mid'] && isset($meal_plan['residences'])) {
    $residences = array_filter($meal_plan['residences']);

    uw_student_fee_estimator_reasmeal_merge('mid', $meal_plan['mid'], $residences);
  }

  return $result;
}

/**
 * Residence merge of database record.
 *
 * @param array $residence
 *   Residence data.
 *
 * @return bool
 *   TRUE if merge is successful.
 */
function _uw_student_fee_estimator_residence_merge(array $residence) {
  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESIDENCES,
    'fields' => [
      'name' => $residence['name'],
      'price' => $residence['price'],
      'updated' => REQUEST_TIME,
      'status' => $residence['status'],
      'meal_plan_included' => $residence['meal_plan_included'],
      'meal_plan_required' => $residence['meal_plan_required'],
      'career' => $residence['career'],
    ],
    'keys' => [
      'rid' => $residence['rid'],
    ],
  ];

  if (isset($residence['created']) && $residence['created']) {
    $input['fields']['created'] = REQUEST_TIME;
  }

  $result = _uw_student_fee_estimator_merge_record($input);

  if ($result && (!isset($residence['rid']) || $residence['rid'] === FALSE)) {
    $residence['rid'] = $result;
  }

  // Insert meals plans, but first flag all previous and
  // later delete not used ones.
  if (isset($residence['rid']) && $residence['rid'] && isset($residence['meal_plans'])) {
    $meal_plans = array_filter($residence['meal_plans']);

    // Delete all existing records if meal plan is included.
    if ($residence['meal_plan_included']) {
      $meal_plans = [];
    }

    uw_student_fee_estimator_reasmeal_merge('rid', $residence['rid'], $meal_plans);
  }

  return $result;
}

/**
 * Merges record is mapping table - resmeal.
 *
 * @param string $condition
 *   Condition name.
 * @param string|mixed $condition_value
 *   Condition value.
 * @param array $values
 *   List of dependent records.
 *
 * @return int|string
 *   Number of affected records.
 */
function uw_student_fee_estimator_reasmeal_merge($condition, $condition_value, array $values) {
  $result = 0;
  // First disable all previous records in database (flag them).
  $old_records = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESMEAL,
    'fields' => ['status' => 0],
    'conditions' => [$condition => $condition_value],
  ];

  _uw_student_fee_estimator_merge_record($old_records);
  $dependant = ($condition === 'rid') ? 'mid' : 'rid';

  foreach ($values as $value) {
    $resmeal_params = [
      'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESMEAL,
      'fields' => [
        'created' => REQUEST_TIME,
        'status' => 1,
      ],
      'keys' => [
        $condition => $condition_value,
        $dependant => $value,
      ],
    ];

    $result += _uw_student_fee_estimator_merge_record($resmeal_params);
  }

  // Delete unused records.
  $old_records = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESMEAL,
    'conditions' => ['status' => 0],
  ];

  _uw_student_fee_estimator_database_delete_record($old_records);

  return $result;
}

/**
 * Creates or updates tuition record.
 *
 * @param array $values
 *   Tuition values.
 *
 * @return bool
 *   Was merge successful.
 */
function _uw_student_fee_estimator_tuition_merge(array $values) {
  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION,
    'fields' => $values,
  ];

  if (isset($values['tid']) && $values['tid']) {
    $input['conditions'] = ['tid' => $values['tid']];
  }

  $result = _uw_student_fee_estimator_merge_record($input);

  return $result;
}

/**
 * Creates submission record.
 *
 * This function is invoked by development file,
 * dynamic call in _uw_student_fee_estimator_generate_data.
 *
 * @param array $submission
 *   Submission data as array.
 *
 * @return int|string
 *   Number of new/affected records.
 */
function _uw_student_fee_estimator_submission_merge(array $submission) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.tuition');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $result = FALSE;
  $tuition = FALSE;
  $tuition_id = _uw_student_fee_estimator_array_value($submission, 'tuition');
  if ($tuition_id) {
    $tuition = uw_student_fee_estimator_load_tuition($tuition_id);
  }

  $first_name = _uw_student_fee_estimator_array_value($submission, 'first_name');
  $last_name = _uw_student_fee_estimator_array_value($submission, 'last_name');
  $full_name = $first_name . ' ' . $last_name;

  if ($tuition && is_array($tuition)) {
    $input = [
      'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_SUBMISSIONS,
      'fields' => [
        'term_code' => variable_get('uw_student_fee_estimator_current_term'),
        'name' => $full_name,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'email' => _uw_student_fee_estimator_array_value($submission, 'email'),
        'student_number' => _uw_student_fee_estimator_array_value($submission, 'student_number', NULL),
        'student_career' => _uw_student_fee_estimator_array_value($submission, 'student_career'),
        'mid' => _uw_student_fee_estimator_array_value($submission, 'mealplan', 0),
        'rid' => _uw_student_fee_estimator_array_value($submission, 'residence', 0),
        'citizenship' => _uw_student_fee_estimator_array_value($submission, 'citizenship'),
        'tid' => $tuition_id,
        'requested_interval' => _uw_student_fee_estimator_array_value($submission, 'requested_interval'),
        'pdf_file' => NULL,
        'notification' => 0,
        'status' => 1,
        'form_of_study' => _uw_student_fee_estimator_array_value($submission, 'form_of_study'),
        'academic_load' => _uw_student_fee_estimator_array_value($submission, 'academic_load'),
        'number_of_courses' => _uw_student_fee_estimator_array_value($submission, 'number_of_courses', 0),
        'number_of_terms' => _uw_student_fee_estimator_array_value($submission, 'number_of_terms', 0),
      ],
    ];

    $result = _uw_student_fee_estimator_merge_record($input);
  }

  return $result;
}

/**
 * Update an existing submission with new values.
 *
 * @param string $sid
 *   Submission id.
 * @param array $values
 *   Update values.
 *
 * @return bool|int|string
 *   Number of how many records were updated.
 */
function _uw_student_fee_estimator_submission_update($sid, array $values) {
  $result = FALSE;

  if (is_numeric($sid) && !empty($values)) {
    $input = [
      'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_SUBMISSIONS,
      'fields' => $values,
      'keys' => [
        'sid' => $sid,
      ],
    ];

    $result = _uw_student_fee_estimator_merge_record($input);
  }

  return $result;
}

/**
 * Loads Resmeal records (mapping table).
 *
 * Used when editing residences and meal plans.
 *
 * @param string $condition
 *   Condition for resmeal table.
 * @param string $condition_value
 *   Condition value.
 *
 * @return mixed
 *   Result of load.
 */
function _uw_student_fee_estimator_resmeals($condition, $condition_value) {
  $result = _uw_student_fee_estimator_load_record([
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESMEAL,
    'fields' => [$condition => $condition_value],
    'reset' => FALSE,
    'sort_key' => $condition === 'rid' ? 'mid' : 'rid',
    'current_term' => FALSE,
  ]);

  return $result;
}

/**
 * Inserts or updates template database record.
 *
 * @param array $values
 *   Template data as array.
 *
 * @return int|string
 *   Result of merge.
 */
function _uw_student_fee_estimator_pdf_template_merge(array $values) {
  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_PDF_TEMPLATE,
    'fields' => [
      'template' => $values['template'],
      'updated' => REQUEST_TIME,
      'status' => $values['status'],
      'default_template' => $values['default_template'],
      'name' => $values['name'],
    ],
  ];

  if (isset($values['eid']) && $values['eid']) {
    $input['conditions'] = ['eid' => $values['eid']];
  }

  $result = _uw_student_fee_estimator_merge_record($input);

  return $result;
}

/**
 * Updates existing default template.
 */
function _uw_student_fee_estimator_pdf_template_default_unset() {
  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_PDF_TEMPLATE,
    'fields' => [
      'default_template' => 0,
    ],
    'conditions' => [
      'default_template' => 1,
    ],
  ];

  $result = _uw_student_fee_estimator_merge_record($input);

  return $result;
}

/**
 * Export tuition data.
 *
 * @param string|null $term
 *   Term as optional parameter.
 * @param string $career
 *   Student career.
 * @param string $citizenship
 *   Student citizenship status.
 *
 * @return array
 *   Tuition ids that match query as array.
 */
function uw_student_fee_estimator_export_tuition($term = NULL, $career = NULL, $citizenship = NULL) {
  if (!$term) {
    $term = variable_get('uw_student_fee_estimator_current_term', '1191');
  }

  $tuition_query = db_select(UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION, 't')->fields('t', ['tid'])
    ->condition('t.career', $career)
    ->condition('t.term_code', $term)
    ->condition('t.status', 1);

  if ($citizenship && $citizenship !== 'all') {
    $tuition_query->condition('t.citizenship', $citizenship);
  }

  $result = $tuition_query->execute()->fetchAllAssoc('tid', PDO::FETCH_ASSOC);
  $tuition_ids = array_keys($result);

  return $tuition_ids;
}
