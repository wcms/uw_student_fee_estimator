<?php

/**
 * @file
 * PDF file generator.
 */

use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Html2Pdf;

/**
 * PDF Create form.
 *
 * @param array $values
 *   Token values, calculated in previous steps.
 * @param string $sid
 *   Submission id.
 * @param string $template_id
 *   Template id (eid) that will be used for PDF create.
 *
 * @return string
 *   File URI of generated PDF file.
 *
 * @throws Exception
 *    If there is any permission issues when trying to create PDF file.
 */
function _uw_student_fee_estimator_pdf_create(array $values, $sid, $template_id) {
  $return_file = '';
  $module_folder = drupal_get_path('module', 'uw_student_fee_estimator');
  $term_code = variable_get('uw_student_fee_estimator_current_term', '0000');

  if (file_exists($module_folder . '/vendor/autoload.php')) {
    require_once $module_folder . '/vendor/autoload.php';

    $pdf_document = new Html2Pdf('P', 'A4', 'en', FALSE, 'UTF-8', 10);

    if ($template_id) {
      $template_record = uw_student_fee_estimator_load_pdf_template($template_id);
    }
    else {
      $template_record = uw_student_fee_estimator_load_pdf_template(NULL, ['fields' => ['default_template' => 1]]);
    }

    if ($template_record && isset($template_record['template']) && $template_record['template']) {
      $template = check_markup($template_record['template'], 'uw_tf_basic');

      $test = isset($values['test']) ? $values['test'] : FALSE;
      $pdf_file_path = _uw_student_fee_estimator_prepare_pdf_paths($term_code, $sid, $values['student_email'], $test);
      $pdf_real_file_path = drupal_realpath($pdf_file_path);

      if ($pdf_file_path) {
        $header = _uw_student_fee_estimator_pdf_header();
        $footer = _uw_student_fee_estimator_pdf_footer();
        $output = token_replace($template, ['token_values' => $values]);

        try {
          $pdf_document->writeHTML('<page backtop="40mm" backbottom="60mm">' . $header . $footer . $output . '</page>');

          $pdf_document->output($pdf_real_file_path, 'F');

          $return_file = $pdf_file_path;
        }
        catch (Html2PdfException $exception) {
          $pdf_document->clean();
          throw $exception;
        }
      }
    }
  }
  else {
    throw new Exception("PDF Required library not found. Make sure you run composer install.");
  }

  return $return_file;
}

/**
 * Prepares folder structure for PDF files.
 *
 * @param string $term_code
 *   Term code.
 * @param string $sid
 *   Submission id.
 * @param string $email
 *   Email value used to calculate hash for file path.
 * @param bool $test
 *   If test is conducted.
 *
 * @return string
 *   File URL.
 */
function _uw_student_fee_estimator_prepare_pdf_paths($term_code, $sid, $email, $test = FALSE) {
  $filename = NULL;
  $pdf_path = '/' . $term_code . '/' . date('m');
  $folder = variable_get('uw_student_fee_estimator_pdf_path', 'pdf');
  $file_path = 'private://' . $folder . $pdf_path;

  if (file_prepare_directory($file_path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {

    $hash = substr(hash('md5', $email . $sid), 0, 10);
    $filename = $hash . '.pdf';

    if ($test) {
      $filename = 'test_' . $hash . '.pdf';
    }
  }

  return $file_path . '/' . $filename;
}

/**
 * PDF Header.
 *
 * @param bool $date
 *   Custom date to be places to PDF header.
 *
 * @return string
 *   Header markup.
 */
function _uw_student_fee_estimator_pdf_header($date = FALSE) {
  $module_folder = drupal_get_path('module', 'uw_student_fee_estimator');

  $logo_path = $module_folder . '/images/uw_student_estimator_logo.jpg';
  $logo = '<img src="' . $logo_path . '" style="width:200px;"/>';
  $date = date('F d, Y');

  $output = "
  <page_header>
  <div>
    {$logo}
  </div><div>" . $date . "</div></page_header>";

  return $output;
}

/**
 * PDF Footer.
 *
 * @return string
 *   Footer markup for PDF file.
 */
function _uw_student_fee_estimator_pdf_footer() {
  $module_folder = drupal_get_path('module', 'uw_student_fee_estimator');

  $logo2 = '<img src="' . $module_folder . '/images/uw_student_estimator_footer.gif' . '" style="width:60px;"/>';
  $signature = '<img src="' . $module_folder . '/' . variable_get('uw_student_fee_estimator_pdf_signature_image', 'images/uw_student_estimator_signature.png') . '" width="200px" />';
  $signature_text = nl2br(variable_get('uw_student_fee_estimator_pdf_signature_text'));
  $footer = variable_get('uw_student_fee_estimator_pdf_footer_text', '519-888-4567 | uwaterloo.ca	| 200 UNIVERSITY AVENUE WEST, WATERLOO, ON, CANADA N2L 3G1');

  $output = "<page_footer>
  <p>Kind regards,</p>
  <div>
    {$signature}
  </div>
  <p>{$signature_text}</p>
  <p align='right'>
    {$footer} {$logo2}
  </p></page_footer>";

  return $output;
}

/**
 * Template form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 * @param int|null $template
 *   Template file id.
 *
 * @return mixed
 *   Template form.
 */
function uw_student_fee_estimator_admin_pdf_template_form($form, &$form_state, $template = NULL) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Template settings'),
  ];

  $form['group1']['eid'] = [
    '#type' => 'hidden',
    '#value' => $template ? $template['eid'] : '',
  ];

  $form['group1']['name'] = [
    '#type' => 'textfield',
    '#title' => t('Template name'),
    '#description' => t('PDF template name that will be used on tuition form as part of select box options.'),
    '#default_value' => $template ? $template['name'] : '',
    '#required' => TRUE,
  ];

  $form['group1']['template'] = [
    '#type' => 'text_format',
    '#title' => t('PDF template'),
    '#format' => 'uw_tf_basic',
    '#default_value' => $template ? $template['template'] : '',
    '#rows' => 20,
    '#required' => TRUE,
  ];

  $form['group1']['container'] = [
    '#type' => 'fieldset',
    '#title' => t('Placeholders'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  if (module_exists('token')) {
    $form['group1']['container']['tokens'] = [
      '#theme' => 'token_tree',
      '#token_types' => ['uw-fee-estimator'],
      '#global_types' => FALSE,
      '#click_insert' => FALSE,
    ];
  }

  $default_template = _uw_student_fee_estimator_pdf_template_default(TRUE);
  $current_default_template = FALSE;

  if (isset($template['eid']) && $template['eid'] === $default_template) {
    $current_default_template = TRUE;
  }

  $form['group1']['default_template'] = [
    '#type' => 'checkbox',
    '#title' => t('Default'),
    '#description' => t('Should this template be used as default for selected citizenship? This will overwrite existing default if it exists.'),
    '#default_value' => $template ? $template['default_template'] : '',
    '#disabled' => $current_default_template,
  ];

  $form['group1']['status'] = [
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#description' => t('Is this content published, in active use. If not, all tuition using this template will default to default template.'),
    '#default_value' => $template ? $template['status'] : '',
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
  ];

  return $form;
}

/**
 * Template submit handler.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_pdf_template_form_submit($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  form_state_values_clean($form_state);
  $values = $form_state['values'];

  $values['template'] = $values['template']['value'];

  if ($values['default_template']) {
    _uw_student_fee_estimator_pdf_template_default_unset();
  }

  if (_uw_student_fee_estimator_pdf_template_merge($values)) {
    drupal_set_message(t('PDF template successfully saved.'));

    $form_state['redirect'] = '/admin/student-fee-estimator/configuration/pdf-templates';
  }
}

/**
 * Template delete form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 * @param int|null $template
 *   Template id.
 *
 * @return mixed
 *   Conformation form for delete.
 */
function uw_student_fee_estimator_admin_pdf_template_delete_form($form, &$form_state, $template = NULL) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  if (!$template || !isset($template['eid']) || !is_numeric($template['eid'])) {
    return $form;
  }

  if ($template['default_template']) {
    $form['not_allowed'] = [
      '#type' => 'markup',
      '#markup' => t('Deletion of default PDF template is not allowed. Make sure to declare another template as default and then delete this one.'),
    ];

    $form['go_back'] = [
      '#type' => 'submit',
      '#value' => t('Go back'),
      '#prefix' => '<div><br/>',
      '#suffix' => '</div>',
    ];

  }
  else {
    $form['eid'] = [
      '#type' => 'value',
      '#value' => $template['eid'],
    ];

    $form = confirm_form($form,
      t('Are you sure you want to delete template @name (id: @id)?', [
        '@name' => $template['name'],
        '@id' => $template['eid'],
      ]),
      '/admin/student-fee-estimator',
      t('You are deleting <strong>@name</strong> template record that might be used by tuition records.', [
        '@name' => $template['name'],
      ]),
      t('Delete'),
      t('Cancel')
    );
  }

  return $form;
}

/**
 * Actual template delete form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_pdf_template_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  if (isset($values['eid']) && ctype_alnum($values['eid'])) {
    module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

    // Update tuition record that use this template with default.
    $default_template = _uw_student_fee_estimator_pdf_template_default(TRUE);

    if ($default_template) {
      $update = [
        'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION,
        'fields' => [
          'pdf_template' => $default_template,
        ],
        'conditions' => [
          'pdf_template' => $values['eid'],
        ],
      ];

      $update_status = _uw_student_fee_estimator_merge_record($update);
    }
    $result = uw_student_fee_estimator_pdf_template_delete($values['eid']);

    if ($result) {
      drupal_set_message(t('PDF template record was successfully deleted.'));
    }
  }

  $form_state['redirect'] = '/admin/student-fee-estimator/configuration/pdf-templates';
}

/**
 * Header / Footer form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 *
 * @return mixed
 *   Header and footer form.
 */
function uw_student_fee_estimator_admin_pdf_template_hf_form($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $form['group1']['subgroup1'] = [
    '#type' => 'fieldset',
    '#title' => t('Header/Footer'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];

  $form['group1']['subgroup1']['uw_student_fee_estimator_pdf_signature_text'] = [
    '#type' => 'textarea',
    '#title' => t('Signature'),
    '#default_value' => variable_get('uw_student_fee_estimator_pdf_signature_text', ''),
    '#description' => t('This content will be place on bottom left on generated PDF file.'),
    '#required' => TRUE,
  ];

  $form['group1']['subgroup1']['uw_student_fee_estimator_pdf_signature_image'] = [
    '#type' => 'textfield',
    '#title' => t('Signature image path'),
    '#default_value' => variable_get('uw_student_fee_estimator_pdf_signature_image', 'images/uw_student_estimator_signature.png'),
    '#description' => t('Signature image will be placed below the signature content. If not provided default will be used.'),
  ];

  $form['group1']['subgroup1']['uw_student_fee_estimator_pdf_footer_text'] = [
    '#type' => 'textfield',
    '#title' => t('Footer'),
    '#default_value' => variable_get('uw_student_fee_estimator_pdf_footer_text', ''),
    '#description' => t('This content will be place on page footer, default value - <em>519-888-4567 | uwaterloo.ca	| 200 UNIVERSITY AVENUE WEST, WATERLOO, ON, CANADA N2L 3G1</em>.'),
    '#size' => 100,
  ];

  return system_settings_form($form);
}

/**
 * Admin PDF Template form.
 */
function uw_student_fee_estimator_admin_pdf_template_token_form($form, &$form_state) {
  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Token - Student payment options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $payment_option_canadian = variable_get('uw_student_fee_estimator_payment_option_canadian');

  $form['group1']['uw_student_fee_estimator_payment_option_canadian'] = [
    '#type' => 'text_format',
    '#title' => t('<strong>Canadian/Permanent Resident</strong>'),
    '#default_value' => isset($payment_option_canadian['value']) ? $payment_option_canadian['value'] : '',
    '#format' => 'uw_tf_basic',
    '#rows' => 10,
  ];

  $payment_option_international = variable_get('uw_student_fee_estimator_payment_option_international');

  $form['group1']['uw_student_fee_estimator_payment_option_international'] = [
    '#type' => 'text_format',
    '#title' => t('<strong>International</strong>'),
    '#default_value' => isset($payment_option_international['value']) ? $payment_option_international['value'] : '',
    '#format' => 'uw_tf_basic',
    '#rows' => 10,
  ];

  return system_settings_form($form);
}
