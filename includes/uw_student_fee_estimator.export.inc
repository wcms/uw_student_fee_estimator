<?php

/**
 * @file
 * Export logic for estimator module.
 */

/**
 * Export data form.
 *
 * @param array $form
 *   Export form definition.
 * @param array $form_state
 *   Export form values and controls.
 *
 * @return mixed
 *   Export form.
 */
function uw_student_fee_estimator_admin_export_data_form(array $form, array $form_state) {
  global $user;
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Export programs & tuition data'),
  ];

  $all_terms = variable_get('uw_student_fee_estimator_terms');

  if ($all_terms) {
    $options = array_combine($all_terms, $all_terms);
  }
  else {
    $term = variable_get('uw_student_fee_estimator_current_term');
    $options[$term] = $term;
  }

  $form['group1']['term'] = [
    '#type' => 'select',
    '#title' => t('UW Term code'),
    '#description' => t('All used terms are listed here, select one.'),
    '#options' => $options,
  ];

  $form['group1']['career'] = [
    '#type' => 'radios',
    '#title' => t('Student career'),
    '#options' => _uw_student_fee_estimator_career(),
    '#description' => t('Select student career to be included in export. Due to a different templates it is not possible to export all at once.'),
    '#default_value' => 'UG',
  ];

  $form['group1']['citizenship'] = [
    '#type' => 'radios',
    '#title' => t('Tuition residency'),
    '#options' => ['all' => t('All')] + _uw_student_fee_estimator_citizenship(),
    '#description' => t('Select specific tuition residency level, or include all.'),
    '#default_value' => 'all',
  ];

  $form['group1']['delivery'] = [
    '#type' => 'checkbox',
    '#title' => t('Send results by email'),
    '#description' => t('If not selected download link will be provided, and by clicking on it you will be able to download requested file.'),
  ];

  $form['group1']['email'] = [
    '#type' => 'textfield',
    '#title' => t('Destination email'),
    '#default_value' => $user->mail,
    '#description' => t('Email address to send generated CSV file.'),
    '#size' => 30,
    '#states' => [
      'enabled' => [
        ':input[name="delivery"]' => ['checked' => TRUE],
      ],
      'required' => [
        ':input[name="delivery"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['group1']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  return $form;
}

/**
 * Submit handler for export form.
 *
 * @param array $form
 *   Export form.
 * @param array $form_state
 *   Preparing export based on data from the form.
 */
function uw_student_fee_estimator_admin_export_data_form_submit(array $form, array $form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];

  $career = $values['career'];
  $citizenship = $values['citizenship'];
  $term = $values['term'];
  $email_notification = $values['delivery'] ? $values['email'] : FALSE;

  $batch = _uw_student_fee_estimator_export_prepare($term, $career, $citizenship, $email_notification);
  batch_set($batch);
}

/**
 * Preparing export folders, batches.
 *
 * @param string $term
 *   UW term code.
 * @param string $career
 *   Student career.
 * @param string $citizenship
 *   Student citizenship (tuition residency).
 * @param string|bool $email_notification
 *   Destination email or FALSE.
 *
 * @return array|null
 *   Returning batch, using Batch API.
 */
function _uw_student_fee_estimator_export_prepare($term, $career, $citizenship, $email_notification = FALSE) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  // This needs to be low enough, each record consist of
  // two database records (program and tuition).
  $batch_size = 20;
  $tuition_ids = uw_student_fee_estimator_export_tuition($term, $career, $citizenship);
  $folder = variable_get('uw_student_fee_estimator_export_path', 'sfe_export');
  $output_folder = 'private://' . $folder . '/';
  $batches = 0;
  $batch = $file = NULL;
  $operations = $terms = [];

  if (!empty($tuition_ids)) {
    $batches = count($tuition_ids) / $batch_size;
  }

  if ($career === 'GRD') {
    $v = taxonomy_vocabulary_machine_name_load('uw_student_fee_estimator_grd_program_types');
    if ($v) {
      $terms_obj = taxonomy_get_tree($v->vid);

      foreach ($terms_obj as $item) {
        $terms[$item->tid] = $item->name;
      }
    }
  }

  if (file_prepare_directory($output_folder, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
    $output_file_name = uniqid('sfe_') . '_' . format_date(REQUEST_TIME, 'custom', 'Y-m-d') . '.csv';
    $file = fopen($output_folder . $output_file_name, 'w');

    if ($file) {
      $file_header = _uw_student_fee_estimator_export_header($career);
      fputcsv($file, $file_header);
      fclose($file);
    }

    for ($i = 0; $i <= $batches; $i++) {
      $slice = array_slice($tuition_ids, $i * $batch_size, $batch_size);

      $operations[] = [
        '_uw_student_fee_estimator_export_operation',
        [
          $output_folder . $output_file_name,
          $career,
          $slice,
          $terms,
        ],
      ];
    }

    if ($email_notification) {
      $operations[] = [
        '_uw_student_fee_estimator_export_operation_email_notification',
        [
          $email_notification,
          $output_folder . $output_file_name,
        ],
      ];

    }

    $batch = [
      'operations' => $operations,
      'finished' => '_uw_student_fee_estimator_export_operation_finished',
      'file' => drupal_get_path('module', 'uw_student_fee_estimator') . '/includes/uw_student_fee_estimator.export.inc',
      'title' => t('Exporting tuition and program records.'),
      'init_message' => t('Preparing export ...'),
      'progress_message' => t('Processing @current out of total @total'),
      'error_message' => t('There was an error while processing export.'),
    ];
  }

  return $batch;
}

/**
 * Email notification for export.
 *
 * @param string $email
 *   Destination email.
 * @param string $file_name
 *   Generated file name.
 * @param array $context
 *   Context provided by Batch API.
 */
function _uw_student_fee_estimator_export_operation_email_notification($email, $file_name, array &$context) {
  global $language;

  $attachment = [
    'filecontent' => drupal_realpath($file_name),
    'filename' => $file_name,
    'filemime' => 'application/csv',
  ];

  $params = [
    'key' => 'csv-export',
    'to' => $email,
    'subject' => t('Student Fee Estimator - Export - @date',
      ['@date' => format_date(REQUEST_TIME, 'custom', 'Y-m-d')]),
    'body' => t('Exported file attached.'),
    'attachment' => $attachment,
  ];

  $context['results']['email_sent'] = drupal_mail('uw_student_fee_estimator', 'pdf-estimate', $email, $language->language, $params);
}

/**
 * Row extract for tuition data.
 *
 * @param string $output
 *   File name for export.
 * @param string $career
 *   Student career.
 * @param array $tids
 *   All tuition record for this batch.
 * @param array $terms
 *   Vocabulary terms.
 * @param array $context
 *   Context provided by Batch API.
 *
 * @throws \Exception
 *    If there are no permission to write data row.
 */
function _uw_student_fee_estimator_export_operation($output, $career, array $tids, array $terms, array &$context) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $records = _uw_student_fee_estimator_load_record([
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION,
    'sort_key' => 'tid',
    'fields' => [
      'tid' => $tids,
    ],
    'status' => 1,
    'always_array' => TRUE,
  ]);

  $file = fopen($output, 'a+');

  foreach ($records as $tuition) {
    $row = [
      $career,
      _uw_student_fee_estimator_array_value($tuition, 'citizenship'),
      _uw_student_fee_estimator_array_value($tuition, 'increase', 0),
      _uw_student_fee_estimator_array_value($tuition, 'online', 0),
      _uw_student_fee_estimator_array_value($tuition, 'program_description'),
    ];

    if ($career === 'UG') {
      $row[] = _uw_student_fee_estimator_array_value($tuition, 'form_of_study');

      $term_tuition = [];
      $term_coop = [];
      $term_inc = [];
      for ($i = 1; $i <= 5; $i++) {
        $row[] = _uw_student_fee_estimator_array_value_amount($tuition, 'PC' . $i . 'A');
        $row[] = _uw_student_fee_estimator_array_value_amount($tuition, 'PC' . $i . 'B');

        $term_tuition[] = _uw_student_fee_estimator_array_value_amount($tuition, 'T' . $i . 'A');
        $term_tuition[] = _uw_student_fee_estimator_array_value_amount($tuition, 'T' . $i . 'B');

        $term_coop[] = _uw_student_fee_estimator_array_value_amount($tuition, 'WRM' . $i . 'A');
        $term_coop[] = _uw_student_fee_estimator_array_value_amount($tuition, 'WRM' . $i . 'B');

        $term_inc[] = _uw_student_fee_estimator_array_value_amount($tuition, 'IF' . $i . 'A');
        $term_inc[] = _uw_student_fee_estimator_array_value_amount($tuition, 'IF' . $i . 'B');
      }

      $row = array_merge($row, $term_tuition, $term_coop, $term_inc);
    }
    elseif ($career === 'GRD') {
      $row[] = _uw_student_fee_estimator_array_value($terms, $tuition['grd_type']);
      $row[] = _uw_student_fee_estimator_array_value($tuition, 'academic_load');
      $row[] = _uw_student_fee_estimator_array_value_amount($tuition, 'per_term');
      $row[] = _uw_student_fee_estimator_array_value_amount($tuition, 'per_course');
      $row[] = _uw_student_fee_estimator_array_value_amount($tuition, 'per_term_inc');
      $row[] = _uw_student_fee_estimator_array_value_amount($tuition, 'work_term_per_course');
      $row[] = _uw_student_fee_estimator_array_value_amount($tuition, 'work_term_inc');
    }

    if ($file) {
      fputcsv($file, $row);
    }
    else {
      throw new Exception(t('Permission issue most likely when trying to write tuition row to csv file.'));
    }
  }

  if ($file) {
    if (!isset($context['results']['generated_file'])) {
      $context['results']['generated_file'] = $output;
    }
    fclose($file);
  }
}

/**
 * Batch finish handler.
 *
 * @param string $success
 *   Success of operation.
 * @param array|bool $results
 *   Any results that previous steps saved.
 * @param string|array $operations
 *   Array of any additional data.
 */
function _uw_student_fee_estimator_export_operation_finished($success, $results, $operations) {
  if ($success) {
    $email_message = '';

    $date = format_date(REQUEST_TIME, 'custom', 'Y-m-d');
    $file = $results['generated_file'];
    $url = file_create_url($file);
    $download = l(t('CSV file'), $url, ['attributes' => ['download' => 'export_' . $date . '.csv']]);

    if (isset($results['email_sent']) && $results['email_sent']) {
      $email_message = t('Email has been sent successfully to @mail.', ['@mail' => $results['email_sent']['to']]);
    }

    drupal_set_message(t('Successfully exported tuition records. Download file !dlfile. @notif', ['!dlfile' => $download, '@notif' => $email_message]));
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array(
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
    )), 'error');
  }
}

/**
 * Export header.
 *
 * @param string $career
 *   Student career.
 *
 * @return array
 *   Header to CSV file.
 */
function _uw_student_fee_estimator_export_header($career) {
  global $user;
  $header = [];

  if ($career === 'UG') {
    $header = [
      'Career',
      'Tuition residency',
      'Assumed % increase',
      'Online',
      'Program summary',
      'Co-op or Regular',
      'Per course 1A', 'Per course 1B', 'Per course 2A', 'Per course 2B', 'Per course 3A', 'Per course 3B', 'Per course 4A', 'Per course 4B', 'Per course 5A', 'Per course 5B',
      'Tuition 1A', 'Tuition 1B', 'Tuition 2A', 'Tuition 2B', 'Tuition 3A', 'Tuition 3B', 'Tuition 4A', 'Tuition 4B', 'Tuition 5A', 'Tuition 5B',
      'Coop 1A', 'Coop 1B', 'Coop 2A', 'Coop 2B', 'Coop 3A', 'Coop 3B', 'Coop 4A', 'Coop 4B', 'Coop 5A', 'Coop 5B',
      'Inc 1A', 'Inc 1B', 'Inc 2A', 'Inc 2B', 'Inc 3A', 'Inc 3B', 'Inc 4A', 'Inc 4B', 'Inc 5A', 'Inc 5B',
    ];
  }
  elseif ($career === 'GRD') {
    $header = [
      'Career',
      'Tuition residency',
      'Assumed % increase',
      'Online',
      'Program summary',
      'Type of Grad Program',
      'Full time or Part time',
      'Per term',
      'Per course',
      'Inc per term',
      'Per course in work term',
      'Inc for course in work term',
    ];
  }

  // Add timestamp and user doing export to header, row fields will be blank.
  $header[] = format_date(REQUEST_TIME, 'short');
  $header[] = $user->name;

  return $header;
}
