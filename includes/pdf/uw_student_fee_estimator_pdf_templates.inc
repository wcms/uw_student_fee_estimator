<?php

/**
 * @file
 * Default domestic (canadian) PDF template.
 */

/**
 * Default PDF template for canadian students.
 */
function uw_student_fee_estimator_pdf_template_domestic() {
  return '<h3>Name: <strong>[uw-fee-estimator:student_name] [uw-fee-estimator:student_number]</strong></h3>

<p>Here is your fee estimate:</p>

<table summary="Your fee estimate">
	<tbody>
		<tr>
			<td>Tuition residency:</td>
			<td><strong>[uw-fee-estimator:legal_status]</strong></td>
		</tr>
		<tr>
			<td>Academic level:</td>
			<td><strong>[uw-fee-estimator:student_career]</strong></td>
		</tr>
		<tr>
			<td>Program:</td>
			<td><strong>[uw-fee-estimator:program_name]</strong></td>
		</tr>
		<tr>
			<td>Form of study:</td>
			<td><strong>[uw-fee-estimator:form_of_study]</strong></td>
		</tr>
		<tr>
			<td>Period of study:</td>
			<td><strong>[uw-fee-estimator:requested_interval]</strong></td>
		</tr>
		<tr>
			<td>Study start date:</td>
			<td><strong>[uw-fee-estimator:term_name]</strong></td>
		</tr>
		<tr>
			<td>Tuition and ancillary fees:</td>
			<td><strong>$[uw-fee-estimator:tuition_cost] CAD</strong></td>
		</tr>
		<tr>
			<td>Residence:</td>
			<td><strong>$[uw-fee-estimator:residence_cost] CAD</strong></td>
		</tr>
		<tr>
			<td>Meal Plan:</td>
			<td><strong>$[uw-fee-estimator:meal_plan_cost] CAD</strong></td>
		</tr>
		<tr>
			<td><b>Total</b>:</td>
			<td><strong>$[uw-fee-estimator:total_cost] CAD</strong></td>
		</tr>
	</tbody>
</table>

<p>[uw-fee-estimator:student_payment_options]</p>

<p>All payments sent to your student account will be retained on your student account to pay current and future academic fees. Do not send funds to University of Waterloo which are intended to cover personal living expenses, textbooks, or miscellaneous study expenses. We do not provide estimates of personal living expenses.</p>

<p><strong>To access a receipt for your payment: </strong></p>

<ul>
	<li>Allow 5-7 business days for your payment to arrive at the university.</li>
	<li><span>Log in to the student information system, <a href="https://quest.pecs.uwaterloo.ca/psp/SS/ACADEMIC/SA/?cmd=login&amp;languageCd=ENG">Quest</a>.</span></li>
	<li><span>Click on the &quot;Admissions&quot; tile, then click on &quot;My Account&quot;, then &quot;Account Inquiry&quot;, and then on the term. (If there is no term indicated, your payment has not yet arrived. Continue to check your account daily.)</span></li>
	<li><span>Scroll to the bottom of the Account Summary page and click on the yellow button labelled &quot;Email my bill&quot;.</span></li>
	<li><span>Type your email address in the box and click &quot;SEND&quot;. You will receive an email with a PDF&nbsp;receipt attached.</span></li>
</ul>
';
}
