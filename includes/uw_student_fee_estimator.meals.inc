<?php

/**
 * @file
 * Meal Plans file.
 */

/**
 * Meal plan form.
 *
 * @param arrat|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 * @param string|null $mid
 *   Meal plan id.
 *
 * @return mixed
 *   Form fields.
 */
function uw_student_fee_estimator_admin_meals_form($form, &$form_state, $mid = NULL) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $selected_residences = [];
  $ref_residences_input = ['fields' => ['meal_plan_included' => 0], 'reset' => FALSE];

  $residences = uw_student_fee_estimator_load_residence(NULL, $ref_residences_input);

  if ($mid && $residences) {
    $selected_residences = _uw_student_fee_estimator_resmeals('mid', $mid['mid']);
  }

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Meal plan'),
  ];

  $form['group1']['mid'] = [
    '#type' => 'hidden',
    '#value' => $mid ? $mid['mid'] : FALSE,
  ];

  $form['group1']['name'] = [
    '#type' => 'textfield',
    '#title' => t('Title / Name'),
    '#default_value' => $mid ? $mid['name'] : '',
    '#required' => TRUE,
  ];

  $price =  $mid ? ((int)$mid['price']) / 100.00 : '';

  $form['group1']['price'] = [
    '#type' => 'textfield',
    '#title' => t('Cost per term'),
    '#default_value' => $price,
    '#size' => 10,
    '#required' => TRUE,
    '#description' => t('Total price for meal plan will be calculated on request length, for GRD one year is 3 terms, etc.'),
  ];

  $form['group1']['status'] = [
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => $mid ? $mid['status'] : FALSE,
  ];

  $form['group1']['res'] = [
    '#type' => 'fieldset',
    '#title' => t('Available in residences'),
  ];

  $residences_options = [];

  foreach ($residences as $residence) {
    $residences_options[$residence['rid']] = $residence['name'];
  }

  $form['group1']['res']['residences'] = [
    '#type' => 'checkboxes',
    '#options' => $residences_options,
    '#default_value' => array_keys($selected_residences),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => $mid ? t('Update') : t('Save'),
  ];

  return $form;
}

/**
 * Form validation handler.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_meals_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $cost = $values['price'];

  if (!is_numeric($cost)) {
    form_set_error('price', t('Cost per year must be numeric value.'));
  }
}

/**
 * Form submit handler.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_meals_form_submit($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  form_state_values_clean($form_state);
  $values = $form_state['values'];

  // Storing values in cents.
  $values['price'] = (int)($values['price'] * 100);

  // Generate id if creating new record.
  if (!$values['mid']) {
    $values['mid'] = _uw_student_fee_estimator_id_generator($values['name'], 'mealplan');
    $values['created'] = TRUE;
  }

  if (_uw_student_fee_estimator_mealplan_merge($values)) {
    drupal_set_message(t('Meal plan successfully saved.'));
  }

  if ($values['mid']) {
    $form_state['redirect'] = 'admin/student-fee-estimator/meals';
  }
}

/**
 * Delete meal plan form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 * @param string $meal_plan
 *   Meal plan id.
 *
 * @return mixed
 *   Conformation form.
 */
function uw_student_fee_estimator_admin_meals_delete_form($form, &$form_state, $meal_plan) {
  if (!$meal_plan || !isset($meal_plan['mid'])) {
    return $form;
  }

  $form['mid'] = [
    '#type' => 'value',
    '#value' => $meal_plan['mid'],
  ];

  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  $count_residences = count(_uw_student_fee_estimator_resmeals('mid', $meal_plan['mid']));

  $form = confirm_form($form,
    t('Are you sure you want to delete meal plan - @meal?', ['@meal' => $meal_plan['name']]),
    '/admin/student-fee-estimator/meals',
    t('Meal plan - <b>@title</b> has been used @num time(s) in residences, are you sure you want to delete it? This action cannot be undone.', ['@title' => $meal_plan['name'], '@num' => $count_residences]),
    t('Delete'),
    t('Cancel')
  );

  return $form;
}

/**
 * Actual delete form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_meals_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  if (isset($values['mid'])) {
    $mid = $values['mid'];

    module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
    $result = uw_student_fee_estimator_meals_delete($mid);

    if ($result) {
      drupal_set_message(t('Meal plan was successfully deleted.'));
    }

    $form_state['redirect'] = '/admin/student-fee-estimator/meals';
  }
}
