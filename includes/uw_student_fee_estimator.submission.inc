<?php

/**
 * @file
 * Submission functions.
 */

/**
 * Submission admin view.
 *
 * @param string $sid
 *   Submission id.
 *
 * @return array
 *   Render array.
 */
function uw_student_fee_estimator_submission_view($form, &$form_state, $sid) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.tuition');
  $form = [];

  if (is_numeric($sid)) {
    $data = uw_student_fee_estimator_load_submission($sid);
    $pdf = file_create_url($data['pdf_file']);

    $form['group1'] = [
      '#type' => 'fieldset',
      '#title' => t('Actions'),
    ];

    $form['group1']['resend_email'] = [
      '#type' => 'checkbox',
      '#title' => t('Resend email notification.'),
      '#description' => t('Re-send student email notification, can be combined with PDF regenerate. Or simply repeating sending.'),
      '#states' => [
        'checked' => [
          [
            ':input[name="submission_update"]' => ['checked' => TRUE],
          ],
          [
            ':input[name="regenerate_pdf"]' => ['checked' => TRUE],
          ],
          [
            ':input[name="email_override"]' => ['filled' => TRUE],
          ],
        ],
        'disabled' => [
          [
            ':input[name="submission_update"]' => ['checked' => TRUE],
          ],
          [
            ':input[name="regenerate_pdf"]' => ['checked' => TRUE],
          ],
          [
            ':input[name="email_override"]' => ['filled' => TRUE],
          ],
        ],
      ],
    ];

    $form['group1']['email_override'] = [
      '#type' => 'textfield',
      '#title' => t('Email on submission: @sub', ['@sub' => $data['email']]),
      '#description' => t("Allows you to update submission email address, if initial has a typo, or student can't find it."),
      '#size' => 30,
    ];

    $form['group1']['regenerate_pdf'] = [
      '#type' => 'checkbox',
      '#title' => t('Regenerate PDF'),
      '#description' => t('If checked PDF file will be created again using current tuition data. New PDF file will be associated with submission. Can be used if generated PDF was not valid, or has incorrect values/labels.'),
      '#states' => [],
    ];

    $form['group1']['submission_update'] = [
      '#type' => 'checkbox',
      '#title' => t('Update submission with email value and/or PDF file'),
      '#description' => t('If checked it will update submission record with email override value.'),
      '#states' => [
        'enabled' => [
          [
            ':input[name="email_override"]' => ['filled' => TRUE],
          ],
        ],
        'checked' => [
          [
            ':input[name="regenerate_pdf"]' => ['checked' => TRUE],
          ],
          [
            ':input[name="regenerate_pdf"]' => ['checked' => TRUE],
            ':input[name="email_override"]' => ['filled' => TRUE],
          ],
        ],
        'unchecked' => [
          ':input[name="regenerate_pdf"]' => ['checked' => FALSE],
          ':input[name="email_override"]' => ['empty' => TRUE],
        ],
        'disabled' => [
          [
            ':input[name="regenerate_pdf"]' => ['checked' => TRUE],
          ],
          [
            ':input[name="email_override"]' => ['filled' => TRUE],
            ':input[name="regenerate_pdf"]' => ['checked' => TRUE],
          ],
        ],
      ],
    ];

    $form['group1']['sid'] = [
      '#type' => 'hidden',
      '#value' => $sid,
    ];

    $form['group1']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Update'),
    ];

    $data['tuition'] = $data['tid'];
    $data['mealplan'] = $data['mid'];
    $data['residence'] = $data['rid'];

    $processed = uw_student_fee_estimator_tuition_calculate($data);

    $form_state['processed'] = $processed;

    $tokens_system = token_get_info();
    $tokens = isset($tokens_system['tokens']['uw-fee-estimator']) ? $tokens_system['tokens']['uw-fee-estimator'] : [];
    $rows = [];

    foreach ($processed as $token => $value) {
      if ($value) {
        $rows[] = [$tokens[$token]['name'], $token, $value];
      }
    }

    $form['group2'] = [
      '#type' => 'fieldset',
      '#title' => t('Submission details'),
    ];

    $form['group2']['submission'] = [
      '#theme' => 'table',
      '#header' => [t('Details'), t('Value')],
      '#rows' => [
        [t('Generated PDF (not token)'), l($pdf, $pdf)],
        [t('Notification email'), $data['email']],
        [t('Notification sent'), date('Y-M-d g:i a', $data['notification'])],
      ],
    ];

    $form['group2']['tokens'] = [
      '#theme' => 'table',
      '#header' => [t('Token name'), t('Token code'), t('Value')],
      '#rows' => $rows,
      '#empty' => t('No data to display.'),
    ];
  }

  return $form;
}

/**
 * Validation function for submission view form.
 */
function uw_student_fee_estimator_submission_view_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (!$values['regenerate_pdf'] && !$values['resend_email'] && empty($values['email_override'])) {
    form_set_error('resend_email', t('No options has been selected.'));
  }

  if (!empty($values['email_override'])) {
    $email = $values['email_override'];

    $valid_email = filter_var($email, FILTER_VALIDATE_EMAIL);

    if (!$valid_email) {
      form_set_error('email_override', t('Please enter valid email address, use correct format.'));
    }
  }

  // Update dependent fields if not resending email notification.
  if (!$values['resend_email']) {
    $form_state['values']['regenerate_pdf'] = 0;

    if (!$values['email_override']) {
      $form_state['values']['submission_update'] = 0;
    }
  }
}

/**
 * Submit handler for submission view form.
 */
function uw_student_fee_estimator_submission_view_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];
  $sid = $values['sid'];
  $input = [];
  $resend_email = $values['resend_email'];
  $update_submission = FALSE;

  if (($values['submission_update'] || $resend_email)) {
    $update_submission = TRUE;

    $input = [
      'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_SUBMISSIONS,
      'fields' => [
        'updated' => REQUEST_TIME,
      ],
      'keys' => [
        'sid' => $sid,
      ],
    ];
  }

  if ($sid) {
    $submission = uw_student_fee_estimator_load_submission($sid);

    // Use defaults from submission first, override if selected.
    $pdf_file = $submission['pdf_file'];
    $email = $submission['email'];

    // Check if PDF needs to be regenerated first.
    if ($values['regenerate_pdf']) {
      $processed = $form_state['processed'];
      module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

      $pdf_file = uw_student_fee_estimator_pdf_create($processed, $sid);

      if ($pdf_file && $update_submission) {
        $input['fields']['pdf_file'] = $pdf_file;

        drupal_set_message(t('New PDF file has been created.'));
      }

      if (!$pdf_file) {
        drupal_set_message(t('There was an issue generating PDF file.'), 'error');
      }
    }

    if (!empty($values['email_override'])) {
      $email = $values['email_override'];

      if ($update_submission) {
        $input['fields']['email'] = $email;
      }
    }

    if ($resend_email && $pdf_file) {
      module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.email');

      $result = uw_student_fee_estimator_email_notification($email, $pdf_file, $sid);

      if (isset($result['result']) && $result['result']) {
        drupal_set_message(t('Email has been successfully sent to @email.', ['@email' => $email]));
      }
      else {
        drupal_set_message(t('There has been an issue when sending email.'), 'error');
      }

      if ($update_submission) {
        $input['fields']['notification'] = REQUEST_TIME;
      }
    }

    if ($update_submission) {
      module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

      $updated = _uw_student_fee_estimator_merge_record($input);

      if ($updated) {
        drupal_set_message(t('Submission with id: @sub has been updated', ['@sub' => $sid]));
      }
      else {
        drupal_set_message(t('There was an issue updating submission.'), 'error');
      }
    }
  }

  $form_state['redirect'] = 'admin/student-fee-estimator/submissions';
}
