<?php

/**
 * @file
 * Residences inc file that includes all functionality specific to residences.
 */

/**
 * Residences form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 * @param int|null $rid
 *   Residence id if provided.
 *
 * @return mixed
 *   Residence form.
 */
function uw_student_fee_estimator_admin_residences_form($form, &$form_state, $rid = NULL) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $meals = uw_student_fee_estimator_load_meal_plan(NULL, [], FALSE);

  $selected_meals = [];
  if ($rid) {
    $selected_meals = _uw_student_fee_estimator_resmeals('rid', $rid['rid']);
  }

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Residence'),
  ];

  $form['group1']['rid'] = [
    '#type' => 'hidden',
    '#value' => $rid ? $rid['rid'] : FALSE,
  ];

  $form['group1']['career'] = [
    '#type' => 'radios',
    '#title' => t('Career'),
    '#options' => _uw_student_fee_estimator_career(),
    '#default_value' => isset($rid['career']) ? $rid['career'] : '',
    '#required' => TRUE,
  ];

  $form['group1']['name'] = [
    '#type' => 'textfield',
    '#title' => t('Title / Name'),
    '#default_value' => $rid ? $rid['name'] : '',
    '#required' => TRUE,
  ];

  $price =  $rid ? ((int)$rid['price']) / 100.00 : '';

  $form['group1']['price'] = [
    '#type' => 'textfield',
    '#title' => t('Cost per term'),
    '#default_value' => $price,
    '#size' => 10,
    '#required' => TRUE,
    '#description' => t('Total price will be calculated based on term, where one year for GRD is 3 terms, etc.'),
  ];

  $form['group1']['meal_plan_included'] = [
    '#type' => 'checkbox',
    '#title' => t('Meal plan included'),
    '#default_value' => $rid ? $rid['meal_plan_included'] : FALSE,
    '#description' => t('Check if meal plan is included in the price. This will disable meal plan selection for this residence.'),
    '#states' => [
      'disabled' => [
        ':input[name="meal_plan_required"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['group1']['meal_plan_required'] = [
    '#type' => 'checkbox',
    '#title' => t('Meal plan required'),
    '#default_value' => $rid ? $rid['meal_plan_required'] : FALSE,
    '#description' => t('Check if meal plan is required in residence.'),
    '#states' => [
      'disabled' => [
        ':input[name="meal_plan_included"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['group1']['status'] = [
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => $rid ? $rid['status'] : FALSE,
  ];

  $form['group1']['meals'] = [
    '#type' => 'fieldset',
    '#title' => t('Meal plans'),
    '#states' => [
      'visible' => [
        ':input[name="meal_plan_included"]' => ['checked' => FALSE],
      ],
    ],
  ];

  $meal_plans = [];

  foreach ($meals as $meal) {
    $meal_plans[$meal['mid']] = $meal['name'];
  }

  $form['group1']['meals']['meal_plans'] = [
    '#type' => 'checkboxes',
    '#options' => $meal_plans,
    '#default_value' => array_keys($selected_meals),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => $rid ? t('Update') : t('Save'),
  ];

  return $form;
}

/**
 * Form validation handler.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_residences_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $cost = $values['price'];

  if (!is_numeric($cost)) {
    form_set_error('price', t('Cost per year must be numeric value.'));
  }
}

/**
 * Residence submit form hanlder.
 *
 * @param array|bool $form
 *   Form values.
 * @param array|bool $form_state
 *   Form values.
 * @param int|null $rid
 *   Residence id if provided.
 */
function uw_student_fee_estimator_admin_residences_form_submit($form, &$form_state, $rid = NULL) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  form_state_values_clean($form_state);
  $values = $form_state['values'];

  // Storing value to database in cents.
  $values['price'] = (int)($values['price'] * 100);

  // Generate rid (unique id for this residence).
  if (!$values['rid']) {
    $values['rid'] = _uw_student_fee_estimator_id_generator($values['name'], 'residence');
    $values['created'] = TRUE;
  }

  if (_uw_student_fee_estimator_residence_merge($values)) {
    drupal_set_message(t('Residence successfully saved.'));
  }

  if ($values['rid']) {
    $form_state['redirect'] = 'admin/student-fee-estimator/residences';
  }
}

/**
 * Residence delete form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 * @param int $residence
 *   Residence id.
 *
 * @return mixed
 *   Conformation delete form.
 */
function uw_student_fee_estimator_admin_residences_delete_form($form, &$form_state, $residence) {
  if (!$residence || !isset($residence['rid'])) {
    return $form;
  }

  $form['rid'] = [
    '#type' => 'value',
    '#value' => $residence['rid'],
  ];

  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  $count_meal_plans = count(_uw_student_fee_estimator_resmeals('rid', $residence['rid']));

  $form = confirm_form($form,
    t('Are you sure you want to delete residence - @res?', ['@res' => $residence['name']]),
    '/admin/student-fee-estimator/residences',
    t('Residence - <b>@title</b> has been used @num time(s) in meal plans, are you sure you want to delete it? This action cannot be undone.', ['@title' => $residence['name'], '@num' => $count_meal_plans]),
    t('Delete'),
    t('Cancel')
  );

  return $form;
}

/**
 * Actual delete form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_residences_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  if (isset($values['rid'])) {
    $rid = $values['rid'];

    module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
    $result = uw_student_fee_estimator_residences_delete($rid);

    if ($result) {
      drupal_set_message(t('Residence was successfully deleted.'));
    }

    $form_state['redirect'] = '/admin/student-fee-estimator/residences';
  }
}
