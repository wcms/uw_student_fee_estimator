<?php

/**
 * @file
 * Common elements that might be used across files. Include this file if needed.
 */

use Spipu\Html2Pdf\Exception\Html2PdfException;

/**
 * Returns citizenship status, or all available options.
 *
 * This should be be used when adding future new status, just adding it here.
 *
 * @param null|string $code
 *   Citizenship code, or NULL.
 *
 * @return array|mixed
 *   Label for provided code or array of all available codes.
 */
function _uw_student_fee_estimator_citizenship($code = NULL) {
  $options = [
    'ontarian' => t('Domestic Ontario'),
    'canadian' => t('Domestic Out of Province'),
    'international' => t('International'),
  ];

  return ($code && isset($options[$code])) ? $options[$code] : $options;
}

/**
 * Returns career options.
 *
 * @param string|null $code
 *   Career code.
 *
 * @return array|mixed
 *   Career label or all careers.
 */
function _uw_student_fee_estimator_career($code = NULL) {
  $options = [
    'UG' => t('Undergraduate'),
    'GRD' => t('Graduate'),
  ];

  return ($code && isset($options[$code])) ? $options[$code] : $options;
}

/**
 * Returns Academic load label or select field options.
 *
 * @param string|null $code
 *   Academic load code.
 *
 * @return array|mixed
 *   Academic label for provided code or all available codes as array.
 */
function _uw_student_fee_estimator_academic_load($code = NULL) {
  $result = '';

  $options = [
    'FT' => t('Full time'),
    'PT' => t('Part time'),
  ];

  if ($code !== '') {
    $result = ($code && isset($options[$code])) ? $options[$code] : $options;
  }

  return $result;
}

/**
 * Returns Form of study label or select field options.
 *
 * @param string|null $code
 *   Form of study code.
 *
 * @return array|mixed
 *   Form of study label or all values for form of study.
 */
function _uw_student_fee_estimator_form_of_study($code = NULL) {
  $options = [
    'REG' => t('Regular'),
    'CO-OP' => t('Co-op'),
  ];
  $result = $options;

  if (!empty($code)) {
    $result = !empty($options[$code]) ? $options[$code] : $code;
  }

  return $result;
}

/**
 * Generates list of terms codes (fields) with labels.
 *
 * @param int $year
 *   Academic year.
 * @param bool $header
 *   Should header be included when returning result array.
 * @param bool $include_sum_fields
 *   Should calculated fields be included when returning result array.
 *
 * @return array
 *   Array of terms codes.
 */
function _uw_student_fee_estimator_program_terms($year = 0, $header = FALSE, $include_sum_fields = FALSE) {
  $terms = [];

  $academic_year = range(1, 5);
  $academic_term = ['A', 'B'];

  if ($year !== 0 && is_numeric($year)) {
    $academic_year = [$year];
  }

  if ($include_sum_fields) {
    $academic_term = array_merge([''], $academic_term);
  }

  foreach ($academic_year as $term) {
    foreach ($academic_term as $code) {
      $terms['T' . $term . $code] = $term . $code;
      if ($header) {
        $terms['WRM' . $term . $code] = 'WRM' . $term . $code;
        $terms['IF' . $term . $code] = 'IF' . $term . $code;
        $terms['PC' . $term . $code] = 'PC' . $term . $code;
      }
    }
  }

  if ($include_sum_fields) {
    $terms['total'] = t('Total');
  }

  return $terms;
}

/**
 * Theme function for custom field - uw_student_fee_estimator_table.
 *
 * @param array $variables
 *   Tuition fields values.
 *
 * @return string
 *   Table using Form API.
 * @throws \Exception
 */
function theme_uw_student_fee_estimator_table(array $variables) {
  $element = $variables['element'];
  $fields = _uw_student_fee_estimator_program_terms();
  $rows = [];

  foreach (element_children($element) as $key) {
    $row = [];
    foreach ($element[$key] as $field) {
      if (isset($field['#type'])) {
        $row[] = ['data' => $field];
      }
    }

    $rows[] = $row;
  }

  $header_terms = array_values($fields);
  $header = array_merge([t('Group')], $header_terms);

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Function that invokes PDF generator.
 *
 * @param array $values
 *   Token values prepared for PDF.
 *    Example of array $values = Array (
 *    [academic_load] => ''
 *    [program_name] => 'AHS Regular Programs/Plans'
 *    [student_career] => 'Undergraduate (UG)'
 *    [form_of_study] => 'Regular (REG)'
 *    [legal_status] => 'Citizen / PR'
 *    [meal_plan_name] => ''
 *    [meal_plan_cost] => 0
 *    [number_of_courses] => 0
 *    [number_of_terms] => 0
 *    [residence_name] => ''
 *    [residence_cost] => 0
 *    [student_name] => 'John Doe'
 *    [student_email] => 'john@doe.com'
 *    [student_number] => '12345678'
 *    [term_code] => '1191'
 *    [term_name] => 'Winter 2019'
 *    [total_cost] => 32760.25
 *    [tuition_cost] => 32760.25
 *    ).
 * @param string|null $sid
 *   Submission id.
 * @param string|null $eid
 *   Template id (eid) to be used when generating PDF, if NULL use default one.
 *
 * @return string
 *   PDF file name (with path) generated.
 */
function uw_student_fee_estimator_pdf_create(array $values, $sid = NULL, $eid = NULL) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.pdf');

  // Null is not supported as value in database.
  $file_name = '';

  try {
    $file_name = _uw_student_fee_estimator_pdf_create($values, $sid, $eid);
  }
  catch (Html2PdfException $mpdf_exception) {
    drupal_set_message(t('There was an error when generating PDF.'), 'error');
    watchdog(
      'uw_student_fee_estimator',
      'PDF Exception: @error',
      ['@error' => $mpdf_exception->getMessage()],
      WATCHDOG_CRITICAL
    );
  }
  catch (Exception $exception) {
    drupal_set_message(t('There was an exception when generating PDF.'), 'error');
    watchdog(
      'uw_student_fee_estimator',
      'PDF General Exception: @error',
      ['@error' => $exception->getMessage()],
      WATCHDOG_CRITICAL
    );
  }

  return $file_name;
}

/**
 * Creates select options or flat array option (used for random generator).
 *
 * @param bool $code
 *   Code for requested interval.
 *
 * @return array
 *   Label for requested interval or array
 */
function uw_student_fee_estimator_request_interval($code = FALSE) {
  // Any value rename here, don't forget to also update
  // common.inc line 670 and form.inc line 435!
  $values = [
    'program' => t('Entire program (8 terms; 10 terms for double degree programs)'),
    'year' => t('One year (two four-month terms)'),
    'term' => t('One term (four months)'),
  ];

  if ($code) {
    if (isset($values[$code])) {
      $result = $values[$code];
    }
    else {
      $result = FALSE;
    }
  }
  else {
    $result = $values;
  }

  return $result;
}

/**
 * Flatten associative array.
 *
 * @param array $input
 *   Input multidimensional array.
 *
 * @return array
 *   Flatten array (1 dim).
 */
function flatten_array(array $input) {
  $flat_array = [];

  foreach ($input as $code => $element) {
    if (is_array($element)) {
      $flat_array = array_merge($flat_array, flatten_array($element));
    }
    else {
      $flat_array[$code] = $element;
    }
  }

  return $flat_array;
}

/**
 * Term validation.
 *
 * @param string $term_code
 *   Term code to be validated.
 *
 * @param bool $allow_past_terms
 *   If set to TRUE it will ignore if term is in the past.
 *
 * @return bool
 *   Result of validation, TRUE for valid UW term code.
 */
function _uw_student_fee_estimator_term_validate($term_code, $allow_past_terms = TRUE) {
  $term_code_valid = TRUE;

  if (is_numeric($term_code) && strlen($term_code) === 4) {
    $calendar_year = date('y');
    $calendar_months = [1, 5, 9];

    $term_year = substr($term_code, 1, 2);
    $term_month = substr($term_code, 3, 1);

    if (!in_array($term_month, $calendar_months)) {
      $term_code_valid = FALSE;
    }

    // Allow only future terms.
    if (!$allow_past_terms && $term_year - $calendar_year < 0) {
      $term_code_valid = FALSE;
    }
  }
  else {
    $term_code_valid = FALSE;
  }

  return $term_code_valid;
}

/**
 * Term decode.
 *
 * @param string $term_code
 *   Term code needing decoding.
 * @param bool $include_month
 *   Should result include month.
 *
 * @return string
 *   Decoded term.
 */
function _uw_student_fee_estimator_term_decode($term_code, $include_month = FALSE) {
  $term_name = '';

  if (_uw_student_fee_estimator_term_validate($term_code)) {
    $term_names = [
      1 => t('Winter'),
      5 => t('Spring'),
      9 => t('Fall'),
    ];

    $term_year = substr($term_code, 1, 2);
    $term_month = substr($term_code, 3, 1);

    $term_name = $term_names[$term_month] . ' 20' . $term_year;

    if ($include_month) {
      $month_names = [
        1 => t('January'),
        5 => t('May'),
        9 => t('September'),
      ];

      $term_name .= ' (' . $month_names[$term_month] . ' 1, 20' . $term_year . ')';
    }
  }

  return $term_name;
}

/**
 * Returns term name.
 *
 * @param bool $include_month
 *   Should return value (result) return month name.
 *
 * @return string
 *   Decoding term code to Date when terms starts.
 */
function uw_student_fee_estimator_term_name($include_month = FALSE) {
  $term_name = '';
  $term = variable_get('uw_student_fee_estimator_current_term');

  if ($term) {
    $term_name = _uw_student_fee_estimator_term_decode($term, $include_month);
  }

  return $term_name;
}

/**
 * Preparing rows for tuition form.
 *
 * @return array
 *   Array of rows that hold numeric values, same names as database fields.
 */
function uw_student_fee_estimator_fee_table_rows_prepare() {
  $terms = [];

  $academic_year = range(1, 5);
  $academic_term = ['', 'A', 'B'];

  foreach ($academic_year as $term) {
    foreach ($academic_term as $code) {
      $terms[0]['T' . $term . $code] = $term . $code;
      $terms[1]['WRM' . $term . $code] = 'WRM' . $term . $code;
      $terms[2]['IF' . $term . $code] = 'IF' . $term . $code;
    }
  }

  $terms[0]['T_total'] = t('Total');
  $terms[1]['WRM_total'] = t('Total');
  $terms[2]['IF_total'] = t('Total');

  return $terms;
}

/**
 * Helper function prepare amounts to 2 decimal places.
 *
 * @param string $value
 *   Values that needs to be converted to number.
 *
 * @return string
 *   Formatted number.
 */
function prepare_fee_amount($value) {
  return number_format(floatval($value), 2);
}

/**
 * Get all email templates.
 *
 * @param bool $as_select
 *   If TRUE create array that can be used as #options.
 * @param bool $status
 *   Include only published templates.
 * @param bool $term_code
 *   Term code if not current.
 *
 * @return array|mixed
 *   Array of all published templates.
 */
function _uw_student_fee_estimator_pdf_templates_all($as_select = FALSE, $status = TRUE, $term_code = FALSE) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_PDF_TEMPLATE,
    'sort_key' => 'eid',
    'reset' => FALSE,
  ];

  if ($status) {
    $input['fields']['status'] = 1;
  }

  $result = _uw_student_fee_estimator_load_record($input);

  if ($as_select) {
    $options = [];

    foreach ($result as $item) {
      $options[$item['eid']] = $item['name'];
    }

    $result = $options;
  }

  return $result;
}

/**
 * Generates select option list of PDF templates.
 *
 * @param bool $id_only
 *   If TRUE returns ID only.
 *
 * @return array|mixed
 *   Array of all published PDF templates.
 */
function _uw_student_fee_estimator_pdf_template_default($id_only = FALSE) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $input = [
    'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_PDF_TEMPLATE,
    'sort_key' => 'eid',
    'fields' => [
      'status' => 1,
      'default_template' => 1,
    ],
  ];

  $result = _uw_student_fee_estimator_load_record($input);
  if (is_array($result) && empty($result)) {
    $result = 0;
  }
  // We may have multiple default templates, use latest one.
  elseif (is_array($result) && count($result) > 1 && isset($result[1]['eid'])) {
    $result = end($result);
  }

  if ($id_only && isset($result['eid'])) {
    $result = $result['eid'];
  }

  return $result;
}

/**
 * Creates options for form's select residence field.
 *
 * @param null $career
 *   If provided load residences with matching career level.
 *
 * @return array
 *   Residences as array for #options.
 */
function _uw_student_fee_estimator_form_residences_select($career = NULL) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $arguments = [
    'reset' => FALSE,
  ];

  if ($career && in_array(strtoupper($career), ['GRD', 'UG'])) {
    $arguments['fields']['career'] = strtoupper($career);
  }

  $residences = uw_student_fee_estimator_load_residence(NULL, $arguments);

  $options = [];

  foreach ($residences as $residence) {
    $options[$residence['rid']] = $residence['name'] . ' - $' . ((int) $residence['price'] / 100);
  }

  return $options;
}

/**
 * Based on selected residence available meals plans are loaded.
 *
 * @param string $residence_id
 *   Residence id.
 *
 * @return array
 *   Based on residence, create meal plan list.
 */
function uw_student_fee_estimator_form_meals_select($residence_id) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  $options = [];

  if ($residence_id) {
    $available_meals = _uw_student_fee_estimator_resmeals('rid', $residence_id);

    if ($available_meals) {
      $meals = uw_student_fee_estimator_load_meal_plan($available_meals, [], FALSE);
    }
  }

  if (!empty($meals)) {
    foreach ($meals as $meal) {
      $options[$meal['mid']] = $meal['name'] . ' - $' . ((int) $meal['price'] / 100);
    }
  }

  return $options;
}

/**
 * Calculates values for all tokens.
 *
 * @param array $submission
 *   Submission data as array
 *    (
 *      [name] => John Doe
 *      [email] => john@doe.ca
 *      [student_number] => 12345678
 *      [citizenship] => canadian | international | ontarian
 *      [student_career] => UG | GRD
 *      [requested_interval] => program | year | term
 *      [tuition] => 1  -- tid (tuition id) database record
 *      [residence] => 0  -- rid (residence id) database record id
 *      [mealplan] => 0   -- mid (meal plan id) database record id
 *      [form_of_study] => REG | CO-OP
 *      [academic_load] => FT | PT
 *      [number_of_courses] => 1..12
 *      [number_of_terms] => 1..12
 *    ).
 * @param array $tokens
 *   Array of token names, if not provided all tokens will be returned.
 *
 * @return array
 *   Tokens values calculated, and returned as array.
 */
function uw_student_fee_estimator_tokens_values(array $submission, array $tokens = NULL) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  $results = [];

  if (empty($submission)) {
    return $results;
  }

  $tokens_valid = $tokens;

  // Get all available tokens and do intersect against provided token list,
  // just to be sure no invalid tokes are used. If no such list is provided
  // use available token list.
  $tokens_system = token_get_info();
  $tokens_all = isset($tokens_system['tokens']['uw-fee-estimator']) ? $tokens_system['tokens']['uw-fee-estimator'] : [];
  // Loads all tokens from cache, and then use only this module tokens.
  if (!$tokens_valid) {
    $tokens_valid = $tokens_all;
  }
  else {
    $tokens_valid = array_intersect(array_values($tokens_all), array_values($tokens_valid));
  }

  $tuition_record = uw_student_fee_estimator_load_tuition($submission['tuition']);

  $records = [
    'tuition' => $tuition_record,
    'mealplan' => 0,
    'residence' => 0,
  ];

  if (!empty($submission['mealplan'])) {
    $records['meal_plan'] = uw_student_fee_estimator_load_meal_plan($submission['mealplan']);
  }

  if (!empty($submission['residence'])) {
    $records['residence'] = uw_student_fee_estimator_load_residence($submission['residence']);
  }

  $form_of_study = _uw_student_fee_estimator_array_value($submission, 'form_of_study', 'Not applicable for this program');
  $academic_load = _uw_student_fee_estimator_array_value($submission, 'academic_load');
  $career = _uw_student_fee_estimator_array_value($submission, 'student_career');
  $citizenship = _uw_student_fee_estimator_array_value($submission, 'citizenship');
  $requested_interval = _uw_student_fee_estimator_array_value($submission, 'requested_interval');
  $student_number = _uw_student_fee_estimator_array_value($submission, 'student_number');
  $student_first_name = trim(_uw_student_fee_estimator_array_value($submission, 'first_name'));
  $student_last_name = trim(_uw_student_fee_estimator_array_value($submission, 'last_name'));
  $student_full_name = $student_first_name . ' ' . $student_last_name;

  $simple_values = [
    'student_career' => $career ? _uw_student_fee_estimator_career($career) : '',
    'legal_status' => $citizenship ? _uw_student_fee_estimator_citizenship($citizenship) : '',
    'student_name' => $student_full_name,
    'first_name' => $student_first_name,
    'last_name' => $student_last_name,
    'student_email' => _uw_student_fee_estimator_array_value($submission, 'email'),
    'student_number' => _uw_student_fee_estimator_tokens_student_number($student_number),
    'term_code' => variable_get('uw_student_fee_estimator_current_term'),
    'term_name' => _uw_student_fee_estimator_term_decode(variable_get('uw_student_fee_estimator_current_term')),
    'number_of_courses' => _uw_student_fee_estimator_array_value($submission, 'number_of_courses'),
    'number_of_terms' => _uw_student_fee_estimator_array_value($submission, 'number_of_terms'),
    'form_of_study' => $form_of_study ? _uw_student_fee_estimator_form_of_study($form_of_study) : '',
    'academic_load' => $academic_load ? _uw_student_fee_estimator_academic_load($academic_load) : '',
    'requested_interval' => $requested_interval ? uw_student_fee_estimator_request_interval($requested_interval) : '',
  ];

  $fields = array_fill_keys(array_keys($tokens_valid), '');

  foreach ($fields as $token => $value) {
    // Check if value is not just plain submission value (simple value).
    if (isset($simple_values[$token])) {
      $results[$token] = $simple_values[$token];

      // Special case for GRD, where academic year is 3 terms. This is quick
      // fix, not spending any additional time to refactor.
      if ($career === 'GRD' && $token === 'requested_interval' && $simple_values[$token] === 'One year (two four-month terms)') {
        $results[$token] = 'One year (three four-month terms)';
      }
    }
    // If not, check if there is a function we can invoke.
    else {
      $function_name = '_uw_student_fee_estimator_tokens_' . $token;

      if (function_exists($function_name)) {
        $results[$token] = call_user_func($function_name, $records, $submission);
      }
      else {
        $results[$token] = '';
      }
    }
  }

  // Some aggregated fields don't have function, add placeholder.
  if (isset($results['total_cost'])) {
    $results['total_cost'] = $results['meal_plan_cost'] + $results['residence_cost'] + $results['tuition_cost'];

    if ($round_up = variable_get('uw_student_fee_estimator_total_rounding')) {
      $results['meal_plan_cost'] = ceil($results['meal_plan_cost'] / $round_up) * $round_up;
      $results['residence_cost'] = ceil($results['residence_cost'] / $round_up) * $round_up;
      $results['tuition_cost'] = ceil($results['tuition_cost'] / $round_up) * $round_up;
    }

    $results['total_cost'] = floatval($results['meal_plan_cost']) + floatval($results['residence_cost']) + floatval($results['tuition_cost']);

    if ($round_up) {
      $results['total_cost'] = ceil($results['total_cost'] / $round_up) * $round_up;
    }

    // Prepare numbers for display.
    $results['total_cost'] = number_format($results['total_cost'], 0);
    $results['tuition_cost'] = number_format($results['tuition_cost'], 0);
    $results['meal_plan_cost'] = number_format($results['meal_plan_cost'], 0);
    $results['residence_cost'] = number_format($results['residence_cost'], 0);
  }

  return $results;
}

/**
 * Getting value from array.
 *
 * @param array $input
 *   Input array with values.
 * @param string $field
 *   Field name.
 * @param string $default
 *   Default value to be used if not found.
 *
 * @return mixed|string
 *   Value for a field or default value.
 */
function _uw_student_fee_estimator_array_value(array $input, $field, $default = '') {
  $result = $default;

  if (!empty($input) && isset($input[$field]) && $input[$field]) {
    $result = $input[$field];
  }

  return $result;
}

/**
 * Getting amount value.
 *
 * @param array $input
 *   Input array with values.
 * @param $field
 *   Field name to get value for.
 * @param bool c2d
 *   Convert to dollar value if TRUE. All amounts stored in cents.
 *
 * @return int
 *   Value for amount field or default.
 */
function _uw_student_fee_estimator_array_value_amount(array $input, $field, $c2d = TRUE) {
  $result = 0;

  if (!empty($input) && !empty($input[$field])) {
    $result = (int)$input[$field];

    if ($c2d) {
      $result /= 100;
    }
  }

  return $result;
}

/**
 * Program name from tuition record.
 *
 * @param array $records
 *   Tuition record as array.
 * @param array $submission
 *   Submission array.
 *
 * @return string
 *   Program name.
 */
function _uw_student_fee_estimator_tokens_program_name(array $records, array $submission) {
  $program_name = '';

  if ($records && isset($records['tuition']['program_description'])) {
    $program_name = $records['tuition']['program_description'];
  }

  return $program_name;
}

/**
 * Calculating tuition cost for submission.
 *
 * @param array $records
 *   Array with all data for tuition, residence, meal plan.
 * @param array $submission
 *   Submission data.
 *
 * @return float|int
 *   Calculated sum for requested career, program, interval.
 */
function _uw_student_fee_estimator_tokens_tuition_cost(array $records, array $submission) {
  $tuition = isset($records['tuition']) ? $records['tuition'] : NULL;
  $requested_interval = isset($submission['requested_interval']) ? $submission['requested_interval'] : 0;
  $career = isset($submission['student_career']) ? $submission['student_career'] : NULL;
  $program_cost = 0.0;

  if ($tuition && $requested_interval) {
    if ($career === 'UG') {
      if ($requested_interval === 'program') {
        $program_cost = _uw_student_fee_estimator_tokens_tuition_cost_ug_program($tuition);
      }
      elseif ($requested_interval === 'year') {
        $program_cost = (int) $tuition['T1'] + (int) $tuition['IF1'] + (int) $tuition['WRM1'];
      }
      elseif ($requested_interval === 'term') {
        $program_cost = (int) $tuition['T1A'] + (int) $tuition['IF1A'] + (int) $tuition['WRM1A'];
      }
    }
    elseif ($career === 'GRD') {
      if ($requested_interval === 'program') {
        $program_cost = _uw_student_fee_estimator_tokens_tuition_cost_grd(
          isset($submission['number_of_terms']) ? $submission['number_of_terms'] : 0,
          isset($submission['number_of_courses']) ? $submission['number_of_courses'] : 0,
          $tuition['increase'],
          $tuition['per_term'],
          $tuition['per_course'],
          $tuition['per_term_inc']
        );
      }
      elseif ($requested_interval === 'year') {
        if ($tuition['per_term'] > 0) {
          $increase = 1.0 + (float) $tuition['increase'] / 100;
          $per_term = (int) $tuition['per_term'];
          $program_cost = (2 * $per_term) + ($increase * $per_term) + (3 * (int) $tuition['per_term_inc']);
        }
        elseif ($tuition['per_course'] > 0) {
          // GRD year has 3 terms, using that to multiply per term fee.
          $program_cost = 3 * ((int) $tuition['per_course'] * $submission['number_of_courses']) + 3 * (int) $tuition['per_term_inc'];
        }
      }
      elseif ($requested_interval === 'term') {
        if ($tuition['per_term'] > 0) {
          $program_cost = (int) $tuition['per_term'] + (int) $tuition['per_term_inc'];
        }
        elseif ($tuition['per_course'] > 0) {
          $program_cost = (int) $tuition['per_course'] * $submission['number_of_courses'] + (int) $tuition['per_term_inc'];
        }
      }
    }
  }

  return ((float)$program_cost / 100);
}

/**
 * Calculates fee for GRD tuition records.
 *
 * @param int $terms
 *   Number of terms.
 * @param int $courses_per_term
 *   Courses per term.
 * @param int $increase
 *   Yearly increase in %, eg, 10, 15.
 * @param int $per_term_fee
 *   Fee per term.
 * @param int $per_course_fee
 *   Fee per course.
 * @param int $incidental_per_term
 *   Incidental fee per term.
 *
 * @return float|int
 *   Sum of all years/terms/courses.
 */
function _uw_student_fee_estimator_tokens_tuition_cost_grd($terms = 0, $courses_per_term = 0, $increase = 0, $per_term_fee = 0, $per_course_fee = 0, $incidental_per_term = 0) {
  $program_increase = 1.0 + (float) $increase / 100;
  $years = floor($terms / 3);
  $year_sums = [];

  if ($per_term_fee > 0) {
    for ($i = 1; $i <= $years; $i++) {
      $year_sums[$i] = (3 * $per_term_fee * ($program_increase ** ($i - 1))) + (3 * $incidental_per_term);
    }

    if ($rem = $terms % 3) {
      $year_sums[$years + 1] = $rem * (($program_increase ** $years) * $per_term_fee + $incidental_per_term);
    }
  }
  elseif ($per_course_fee > 0) {
    for ($i = 1; $i <= $years; $i++) {
      $year_sums[$i] = (3 * $courses_per_term * $per_course_fee * ($program_increase ** ($i - 1))) + (3 * $incidental_per_term);
    }

    if ($rem = $terms % 3) {
      $year_sums[$years + 1] = ($rem * $courses_per_term * $per_course_fee * (($program_increase ** $years)) + ($rem * $incidental_per_term));
    }
  }

  $program_cost = array_sum($year_sums);

  return $program_cost;
}

/**
 * Calculates tuition values for UG students, with term by term increase.
 *
 * @param array $tuition
 *   Tuition record as array.
 *
 * @return float|int
 *   Cost for UG program with increase compounded.
 */
function _uw_student_fee_estimator_tokens_tuition_cost_ug_program(array $tuition) {
  // E.g. for 10% this will be 1.1 value.
  $program_increase = 1.0 + (double) $tuition['increase'] / 100.00;
  $years = [];

  for ($i = 1; $i <= 5; $i++) {
    $year_increase = 1;

    if ($i > 1) {
      $year_increase = $program_increase;
    }

    // Set sum to zero in case there isn't year 5.
    $years[$i] = 0;

    if ($tuition['calc_formula'] === 'default' || empty($tuition['calc_formula'])) {
      //Check if there is data for current year.
      if ($tuition['T' . $i]) {
        $years[$i] = (($year_increase ** ($i - 1)) * (int) $tuition['T1']) + (int) $tuition['IF' . $i] + (int) $tuition['WRM' . $i];
      }
    }
    elseif ($tuition['calc_formula'] === 'accounting' && $tuition['T' . $i]) {
      if ($i === 1) {
        $years[$i] = (($year_increase ** ($i - 1)) * (int) $tuition['T1']) + (int) $tuition['IF' . $i] + (int) $tuition['WRM' . $i];
      }
      else {
        $years[$i] = (($year_increase ** ($i - 1)) * (int) $tuition['T2']) + (int) $tuition['IF' . $i] + (int) $tuition['WRM' . $i];
      }
    }
  }

  return array_sum($years);
}

/**
 * Returns meal plan name.
 *
 * @param array $records
 *   All relevant records in array.
 * @param array $submission
 *   Submission data.
 *
 * @return string
 *   Meal plan name.
 */
function _uw_student_fee_estimator_tokens_meal_plan_name(array $records, array $submission) {
  $meal_plan_name = '';

  if ($records && isset($records['meal_plan']['name'])) {
    $meal_plan_name = $records['meal_plan']['name'];
  }

  return $meal_plan_name;
}

/**
 * Calculates meal plan cost, for one term only.
 *
 * @param array $records
 *   All relevant records as array.
 * @param array $submission
 *   Submission data.
 *
 * @return float|int
 *   Meal plan cost.
 */
function _uw_student_fee_estimator_tokens_meal_plan_cost(array $records, array $submission) {
  $meal_plan = isset($records['meal_plan']) ? $records['meal_plan'] : 0;
  $costs = 0;

  if ($meal_plan) {
    $costs_per_term = isset($meal_plan['price']) ? ($meal_plan['price'] / 100) : 0;

    if ($submission['student_career'] === 'GRD') {
      if ($submission['requested_interval'] === 'year') {
        $costs = 3 * $costs_per_term;
      }
      else {
        $costs = $costs_per_term;
      }
    }
    // Student career is UG.
    else {
      if ($submission['requested_interval'] === 'term') {
        $costs = $costs_per_term;
      }
      else {
        // In case year or program for UG, use two terms.
        $costs = 2 * $costs_per_term;
      }
    }
  }

  return $costs;
}

/**
 * Returns residence name is residence is available.
 *
 * @param array $records
 *   All relevant records as array.
 * @param array $submission
 *   Submission data as array.
 *
 * @return string
 *   Residence name.
 */
function _uw_student_fee_estimator_tokens_residence_name(array $records, array $submission) {
  $residence_name = '';

  if ($records && isset($records['residence']['name'])) {
    $residence_name = $records['residence']['name'];
  }

  return $residence_name;
}

/**
 * Calculates residence costs. Calculates only one term.
 *
 * @param array $records
 *   Array of database records associated with submission.
 * @param array $submission
 *   Submission data.
 *
 * @return float|int
 *   Residence cost, zero otherwise.
 */
function _uw_student_fee_estimator_tokens_residence_cost(array $records, array $submission) {
  $residence = isset($records['residence']) ? $records['residence'] : FALSE;
  $costs = 0;

  if ($residence) {
    $costs_per_term = isset($residence['price']) ? ($residence['price'] / 100) : 0;

    if ($submission['student_career'] === 'GRD') {
      if ($submission['requested_interval'] === 'year') {
        $costs = 3 * $costs_per_term;
      }
      else {
        $costs = $costs_per_term;
      }
    }
    else {
      if ($submission['requested_interval'] === 'term') {
        $costs = $costs_per_term;
      }
      // If UG requested term is year/program, use two terms.
      else {
        $costs = 2 * $costs_per_term;
      }
    }
  }

  return $costs;
}

/**
 * Field definition for term code.
 *
 * @param bool $term_code
 *   Provided term code, when editing for example.
 *
 * @return array
 *   Field definition using Form API.
 */
function _uw_student_fee_estimator_current_term_field($term_code = NULL) {
  return [
    '#type' => 'textfield',
    '#title' => t('Term code'),
    '#default_value' => $term_code ?: variable_get('uw_student_fee_estimator_current_term', ''),
    '#disabled' => TRUE,
    '#size' => 5,
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div><br/>',
  ];
}

/**
 * Payment option snippet.
 *
 * @param array $records
 *   Array of database records associated with submission.
 * @param array $submission
 *   Submission data.
 *
 * @return string
 *   Citizenship dependent payment description.
 */
function _uw_student_fee_estimator_tokens_student_payment_options(array $records, array $submission) {
  $citizenship = _uw_student_fee_estimator_array_value($submission, 'citizenship');
  $description = '';

  if ($citizenship) {
    $token_variable = variable_get('uw_student_fee_estimator_payment_option_' . $citizenship);
    if ($token_variable) {
      $payment_option = check_markup($token_variable['value'], 'uw_tf_basic');
      $description = $payment_option;
    }
  }

  return $description;
}

/**
 * Formats UW student number for output.
 *
 * @param string $student_number
 *   Student number.
 *
 * @return string
 *   Formatted student number.
 */
function _uw_student_fee_estimator_tokens_student_number($student_number) {
  $label = '';
  $student_number_pattern = '/^\d{8}$/';
  $valid = preg_match($student_number_pattern, $student_number);

  if (!empty($student_number) && $valid) {
    $label = 'UW #' . trim($student_number);
  }

  return $label;
}

/**
 * For provided value creates unique database id.
 *
 * @param $value
 *   Source name you want id generated for
 * @param $type
 *   Type of destination, could be residence or mealplan.
 *
 * @return string
 *   Unique string that can be used as database key.
 */
function _uw_student_fee_estimator_id_generator($value, $type){
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $candidate = trim(strtolower($value));
  // Replace blank spaces with dash.
  $result = preg_replace('/\s+/', '-', $candidate);
  // Remove all non a-z and 0-9 and dashes.
  $result = preg_replace('/[^a-z0-9\-_]+/', '', $result);
  // Remove multiple dashes
  $result = preg_replace('/\-+/', '-', $result);

  // Truncate if length is over 50 characters.
  if (strlen($result) > 50) {
    $result = substr($result, 0, 49);
  }

  $base_name = $result;

  if ($type === 'residence') {
    $query_result = uw_student_fee_estimator_load_residence($result);
    $counter = 0;
    while (!empty($query_result)) {
      $counter++;
      $result = $base_name  . '-' . $counter;
      $query_result = uw_student_fee_estimator_load_residence($result);
    }
  }
  elseif ($type === 'mealplan') {
    $query_result = uw_student_fee_estimator_load_meal_plan($result);
    $counter = 0;
    while (!empty($query_result)) {
      $counter++;
      $result = $base_name . '-' . $counter;
      $query_result = uw_student_fee_estimator_load_meal_plan($result);
    }
  }

  return $result;
}
