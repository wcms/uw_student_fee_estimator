<?php

/**
 * @file
 * Development features.
 *
 * Depends on development mode being enabled.
 */

/**
 * Admin notification form.
 */
function uw_student_fee_estimator_admin_notification_form($form, &$form_state) {
  global $user;
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $form['group0'] = [
    '#type' => 'fieldset',
    '#title' => t('PDF Estimate - details'),
  ];

  $form['group0']['generate_pdf'] = [
    '#type' => 'checkbox',
    '#title' => t('<strong>Generate PDF</strong>'),
    '#description' => t('PDF file with test_ prefix will be generated. Link to it will be displayed.'),
  ];

  $form['group0']['template'] = [
    '#type' => 'select',
    '#title' => t('<strong>Template type</strong>'),
    '#options' => _uw_student_fee_estimator_pdf_templates_all(TRUE, FALSE),
    '#states' => [
      'enabled' => [
        ':input[name="generate_pdf"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['group0']['notification'] = [
    '#type' => 'checkbox',
    '#title' => t('<strong>Send email notification</strong>'),
    '#description' => t('Email override applies, if it is set up.'),
    '#states' => [
      'enabled' => [
        ':input[name="generate_pdf"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['group0']['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Student info'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['group0']['group1']['name'] = [
    '#type' => 'textfield',
    '#title' => t('<strong>Full name</strong>'),
    '#description' => t('Enter your name.'),
    '#required' => TRUE,
    '#default_value' => 'Jane Doe',
    '#size' => 30,
  ];

  $form['group0']['group1']['email'] = [
    '#type' => 'textfield',
    '#title' => t('<strong>Email</strong>'),
    '#description' => t('Generated document (estimate) will be send to this email address.'),
    '#required' => TRUE,
    '#default_value' => $user->mail,
    '#size' => 30,
  ];

  $form['group0']['group1']['student_number'] = [
    '#type' => 'textfield',
    '#title' => t('<strong>Student number</strong>'),
    '#description' => t('For current/future students that have student number assigned. This is optional field.'),
    '#size' => 10,
    '#default_value' => '12345678',
  ];

  $form['group0']['group2'] = [
    '#type' => 'fieldset',
    '#title' => t('Program info'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $intervals = uw_student_fee_estimator_request_interval();

  $form['group0']['group2']['requested_interval'] = [
    '#type' => 'select',
    '#title' => t('<strong>Program/Year/Term</strong>'),
    '#options' => $intervals,
    '#description' => t('Estimates are starting from this academic year'),
  ];

  $tuition_records = uw_student_fee_estimator_load_tuition();
  $tuition_programs = [];
  if ($tuition_records) {
    foreach ($tuition_records as $tid => $program) {
      $tuition_programs[$tid] = t('@term_code - @name - @career - @citizenship (@aload@formos)',
        [
          '@term_code' => $program['term_code'],
          '@name' => $program['program_description'],
          '@career' => $program['career'],
          '@citizenship' => $program['citizenship'],
          '@aload' => $program['academic_load'],
          '@formos' => $program['form_of_study'],
        ]);
    }
  }

  // Sort program by their name, easier to navigate.
  asort($tuition_programs);

  $form['group0']['group2']['tuition'] = [
    '#type' => 'select',
    '#title' => t('<strong>Program</strong>'),
    '#options' => $tuition_programs,
  ];

  $form['group0']['group2']['number_of_courses'] = [
    '#type' => 'select',
    '#title' => t('<strong>Number of courses per term</strong>'),
    '#options' => drupal_map_assoc(range(0, 10)),
    '#default_value' => 0,
  ];

  $form['group0']['group2']['number_of_terms'] = [
    '#type' => 'select',
    '#title' => t('<strong>How many terms to complete your program</strong>'),
    '#options' => drupal_map_assoc(range(0, 10)),
    '#default_value' => 0,
  ];

  $form['group0']['group3'] = [
    '#type' => 'fieldset',
    '#title' => t('Residence and meal plan info'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $residences = _uw_student_fee_estimator_form_residences_select();
  $residences = [0 => t('None')] + $residences;

  $form['group0']['group3']['residence'] = [
    '#type' => 'select',
    '#title' => t('<strong>Available residences</strong>'),
    '#options' => $residences,
    '#default_value' => 0,
  ];

  $meals_records = uw_student_fee_estimator_load_meal_plan();
  $meals = [
    0 => t('None'),
  ];

  foreach ($meals_records as $mid => $one_meal) {
    $meals[$mid] = t('@name - @price',
      [
        '@name' => $one_meal['name'],
        '@price' => $one_meal['price'],
      ]);
  }

  $form['group0']['group3']['mealplan'] = [
    '#type' => 'select',
    '#title' => t('<strong>Meal plans</strong>'),
    '#description' => t('Select place of residence first and then meal plan. Based on residence this is updated.'),
    '#options' => $meals,
    '#default_value' => isset($form_state['values']['mealplan']) ? $form_state['values']['mealplan'] : 0,
  ];

  $form['refresh'] = [
    '#type' => 'submit',
    '#value' => t('Refresh'),
    '#ajax' => [
      'callback' => '_uw_student_fee_estimator_development_preview_ajax',
      'wrapper' => 'tuition-calculations',
      'progress' => [],
      'event' => 'click',
    ],
    '#submit' => ['_uw_student_fee_estimator_development_preview'],
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Generate'),
    '#states' => [
      'enabled' => [
        ':input[name="generate_pdf"]' => ['checked' => TRUE],
      ],
    ],
  ];

  // Use ajax if configured.
  if (variable_get('uw_student_fee_estimator_mode_ajax')) {
    $form['submit']['#ajax'] = [
      'callback' => '_uw_student_fee_estimator_development_preview_ajax',
      'wrapper' => 'tuition-calculations',
      'progress' => [],
      'event' => 'click',
    ];

    $form['submit']['#submit'] = ['uw_student_fee_estimator_admin_notification_form_submit'];
  }

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Tuition values preview'),
    '#prefix' => '<div id="tuition-calculations">',
    '#suffix' => '</div>',
  ];

  $rows = isset($form_state['values']['rows']) ? $form_state['values']['rows'] : '';

  if ($rows) {
    $form['group1']['table'] = [
      '#theme' => 'table',
      '#header' => [t('Token'), t('Value')],
      '#rows' => $rows,
      '#empty' => t('There is not enough data to render table.'),
      '#attributes' => [
        'class' => ['tuition-preview'],
      ],
    ];
  }

  return $form;
}

/**
 * Generates random submission using existing database data.
 */
function uw_student_fee_estimator_admin_notification_form_submit($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.tuition');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.email');

  form_state_values_clean($form_state);
  $values = $form_state['values'];

  $combined = _uw_student_fee_estimator_development_prepare_data($values);

  if (!empty($combined['submission'])) {
    $generate_pdf = $values['generate_pdf'];
    $template = $values['template'];
    $send_notification = $values['notification'];
    $calculated = $combined['calculated'];

    // Set flag so file gets prefixed with test_.
    $calculated['test'] = TRUE;

    if ($generate_pdf) {
      $pdf_file = uw_student_fee_estimator_pdf_create($calculated, '1', $template);

      if ($pdf_file) {
        if ($send_notification) {
          if (uw_student_fee_estimator_email_notification($values['email'], $pdf_file, '1')) {
            drupal_set_message(t('Email notification has been sent successfully.'));
          }
        }

        drupal_set_message(t('Successfully generated PDF, <a href="!link">click to open file</a>.', ['!link' => file_create_url($pdf_file)]));
      }
    }
  }
}

/**
 * Ajax callback for development form results fieldset.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 *
 * @return mixed
 *   Section of form that needs to be rebuild.
 */
function _uw_student_fee_estimator_development_preview_ajax($form, &$form_state) {
  return $form['group1'];
}

/**
 * Recalculates tuition values.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function _uw_student_fee_estimator_development_preview($form, &$form_state) {
  $calculated = _uw_student_fee_estimator_development_prepare_data($form_state['values']);

  if ($calculated) {
    $form_state['values']['rows'] = _uw_student_fee_estimator_development_prepare_data_rows($calculated['calculated']);
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Parse and prepare data to mock submission.
 *
 * @param mixed $values
 *   Form values.
 *
 * @return array
 *   List of calculated values.
 */
function _uw_student_fee_estimator_development_prepare_data($values) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.tuition');

  $submission = $calculated = [];
  $tuition = uw_student_fee_estimator_load_tuition($values['tuition']);

  if ($tuition) {
    $submission = [
      'name' => $values['name'],
      'email' => $values['email'],
      'student_number' => $values['student_number'],
      'citizenship' => $tuition['citizenship'],
      'student_career' => $tuition['career'],
      'requested_interval' => $values['requested_interval'],
      'form_of_study' => $tuition['form_of_study'],
      'academic_load' => $tuition['academic_load'],
      'number_of_courses' => $values['number_of_courses'],
      'number_of_terms' => $values['number_of_terms'],
      'residence' => $values['residence'],
      'mealplan' => $values['mealplan'],
      'tuition' => $values['tuition'],
    ];

    $calculated = uw_student_fee_estimator_tuition_calculate($submission);
  }

  return [
    'submission' => $submission,
    'calculated' => $calculated,
  ];
}

/**
 * Prepares data for table display.
 *
 * @param array $data
 *   Calculated token fields.
 *
 * @return array
 *   Table rows with token name and value for each row.
 */
function _uw_student_fee_estimator_development_prepare_data_rows(array $data) {
  $rows = [];

  foreach ($data as $token => $value) {
    $rows[] = [
      ['data' => $token, 'class' => 'tuition-preview-token-label'],
      $value,
    ];
  }

  return $rows;
}

/**
 * Admin delete form.
 */
function uw_student_fee_estimator_admin_delete_form($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Select records to delete'),
  ];

  $options = [
    'mealplan' => t('Meal plans'),
    'residence' => t('Residences'),
    'program_ug' => t('Programs UG'),
    'program_grd' => t('Programs GRD'),
    'submission' => t('Submissions'),
  ];

  $options = _uw_student_fee_estimator_record_count($options);

  foreach ($options as $key => $label) {
    $form['group1']['delete_' . $key] = [
      '#type' => 'checkbox',
      '#title' => t('Delete: @label', ['@label' => $label]),
    ];

    if ($key === 'submission') {
      $form['group1']['delete_' . $key]['#states']['checked'] = [
        [':input[name="delete_mealplan"]' => ['checked' => TRUE]],
        [':input[name="delete_residence"]' => ['checked' => TRUE]],
        [':input[name="delete_program_ug"]' => ['checked' => TRUE]],
        [':input[name="delete_program_grd"]' => ['checked' => TRUE]],
      ];

      $form['group1']['delete_' . $key]['#states']['disabled'] = $form['group1']['delete_' . $key]['#states']['checked'];
    }
  }

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Delete selected'),
  ];

  return $form;
}

/**
 * Records count from database.
 *
 * @param mixed $options
 *   Array of tables to perform count on.
 *
 * @return mixed
 *   Input array with counts.
 */
function _uw_student_fee_estimator_record_count($options) {
  $with_count = $options;

  foreach ($options as $code => $value) {
    $table = 'uw_student_fee_estimator_' . $code;
    $count = 0;

    if (db_table_exists($table)) {
      $count = db_select($table, 't')
        ->fields('t')
        ->condition('status', 1)
        ->countQuery()
        ->execute()
        ->fetchField();
    }
    else {
      $query = db_select(UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION, 't')
        ->condition('status', 1);

      if ($code === 'program_ug') {
        $query->condition('career', 'UG');
      }
      elseif ($code === 'program_grd') {
        $query->condition('career', 'GRD');
      }

      $count = $query->countQuery()->execute()->fetchField();
    }

    $with_count[$code] = $value . ' (' . $count . ')';
  }

  return (count($with_count) === count($options)) ? $with_count : $options;
}

/**
 * Submit handler for delete form.
 */
function uw_student_fee_estimator_admin_delete_form_submit($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  form_state_values_clean($form_state);
  $values = $form_state['values'];

  if (!empty(array_filter($values))) {
    $deleted = 0;
    $pdf_deleted = 0;
    $delete_resmeal = FALSE;

    if ($values['delete_mealplan'] && $values['delete_residence'] && $values['delete_program_ug'] && $values['delete_program_grd']) {
      $deleted = _uw_student_fee_estimator_delete_all_records();
    }
    else {
      if ($values['delete_residence']) {
        $deleted += uw_student_fee_estimator_delete_table(UW_STUDENT_FEE_ESTIMATOR_TABLE_RESIDENCES);
        $delete_resmeal = TRUE;
      }

      if ($values['delete_mealplan']) {
        $deleted += uw_student_fee_estimator_delete_table(UW_STUDENT_FEE_ESTIMATOR_TABLE_MEALS);
        $delete_resmeal = TRUE;
      }

      if ($values['delete_program_ug']) {
        $deleted += uw_student_fee_estimator_delete_table(UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION, 'UG');
      }

      if ($values['delete_program_grd']) {
        $deleted += uw_student_fee_estimator_delete_table(UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION, 'GRD');
      }

      if ((isset($values['delete_submission']) && $values['delete_submission']) || $deleted) {
        // Always delete submissions. Load all submission and iterate in
        // a loop to delete generated files.
        $all_submissions = uw_student_fee_estimator_load_submission(NULL, ['reset' => FALSE]);

        foreach ($all_submissions as $submission) {
          if (isset($submission['pdf_file'])) {
            if (file_unmanaged_delete($submission['pdf_file'])) {
              $pdf_deleted++;
            }
          }
        }

        $deleted += uw_student_fee_estimator_delete_table(UW_STUDENT_FEE_ESTIMATOR_TABLE_SUBMISSIONS);
      }

      if ($delete_resmeal) {
        // Truncate mapping table for meals and residences.
        uw_student_fee_estimator_delete_table(UW_STUDENT_FEE_ESTIMATOR_TABLE_RESMEAL);
      }
    }

    if ($deleted) {
      // Unpublish form to prevent new submission.
      variable_set('uw_student_fee_estimator_form_published', FALSE);

      $delete_message = format_plural($deleted, '1 record', '@count records');
      drupal_set_message(t('Successfully deleted @message.', ['@message' => $delete_message]));
    }
    else {
      drupal_set_message(t('No data has been deleted.'), 'warning');
    }
  }
}

/**
 * Migration form used to move all data from one site to another.
 */
function uw_student_fee_estimator_admin_migrate_form($form, &$form_state) {
  $form['group0'] = [
    '#type' => 'fieldset',
    '#title' => t('Migration task'),
  ];

  $form['group0']['task'] = [
    '#type' => 'radios',
    '#description' => t('Select action you want to take, export or import.'),
    '#options' => [
      'export' => t('Export'),
      'import' => t('Import'),
    ],
    '#required' => TRUE,
    '#default_value' => 'import',
  ];

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Data migration - Export'),
    '#states' => [
      'visible' => [
        ':input[name="task"]' => ['value' => 'export']
      ],
    ],
  ];

  $form['group1']['selection'] = [
    '#type' => 'radios',
    '#title' => t('Select what to export'),
    '#options' => [
      'all' => t('All'),
      'selective' => t('Some'),
    ],
    '#description' => t('Selected data types will be serialized and exported to a file, this may includes meal plans, residences, programs, taxonomy etc.'),
  ];

  $form['group1']['specific'] = [
    '#type' => 'checkboxes',
    '#title' => t('Select at least one data type to export'),
    '#description' => t('Selected data types will be serialized and exported.'),
    '#options' => [
      'mealplan' => t('Meal plans'),
      'residence' => t('Residences'),
      'program' => t('Programs'),
      'taxonomy' => t('Taxonomy'),
    ],
    '#states' => [
      'visible' => [
        ':input[name="selection"]' => ['value' => 'selective'],
      ],
    ],
  ];

  $form['group2'] = [
    '#type' => 'fieldset',
    '#title' => t('Data migration - Import'),
    '#states' => [
      'visible' => [
        ':input[name="task"]' => ['value' => 'import']
      ],
    ],
  ];

  $form['group2']['input_file'] = [
    '#type' => 'file',
    '#title' => t('Input file'),
    '#description' => t('File has to be result of data export generated by this module.'),
  ];

  $form['group2']['input_delete'] = [
    '#type' => 'checkbox',
    '#title' => t('Delete before import'),
    '#description' => t('If enabled this will delete data before import only for data types that are being imported. Example, if you import meal plans only, and select this feature, it will delete all meal plans first and then run import.')
  ];

  $form['group2']['import'] = [
    '#type' => 'checkboxes',
    '#title' => t('Import only selected data types'),
    '#options' => [
      'mealplan' => t('Meal plans'),
      'residence' => t('Residences'),
      'program' => t('Programs'),
      'taxonomy' => t('Taxonomy'),
    ],
    '#description' => t('If nothing is selected, everything from file will be imported.'),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Complete'),
  ];

  return $form;
}

/**
 * Validation for migration form.
 *
 * @param $form
 * @param $form_state
 */
function uw_student_fee_estimator_admin_migrate_form_validate($form, &$form_state) {
  $task = $form_state['values']['task'];

  if ($task === 'import') {
    $upload = file_save_upload('input_file', [
      'file_validate_extensions' => ['bin'],
    ]);

    if ($upload && $moved = file_move($upload, 'private://')) {
      $form_state['storage']['input_file'] = $moved;
    }
    else {
      form_set_error('input_file', t('No file has been selected.'));
    }
  }

}

/**
 * Submission handler for migration form.
 *
 * @param $form
 * @param $form_state
 */
function uw_student_fee_estimator_admin_migrate_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);

  $values = $form_state['values'];
  $task = $values['task'];

  if ($task === 'export') {
    $filename = uw_student_fee_estimator_admin_migrate_form_submit_export($values);
    $date = format_date(REQUEST_TIME, 'custom', 'Y-m-d');

    if (!empty($filename)) {
      $download = l(t('BIN file'), $filename, ['attributes' => ['download' => 'sfe_raw_' . $date . '.bin']]);

      drupal_set_message(t('Successfully created export of records, download export !export.', ['!export' => $download]));
    }
    else {
      drupal_set_message(t('There was an issue with creating export file.'), 'error');
    }
  }
  else {
    uw_student_fee_estimator_admin_migrate_form_submit_import($values, $form_state['storage']);
  }
}

/**
 * Migration - exporting data.
 *
 * @param array $values
 *
 * @return string
 *   Filename as a result.
 */
function uw_student_fee_estimator_admin_migrate_form_submit_export(array $values) {
  $result = [];
  $sections = ['mealplan', 'residence', 'program', 'taxonomy'];
  $selection = $values['selection'];

  if ($selection === 'all') {
    $specific = array_combine($sections, $sections);
  }
  else {
    $specific = $values['specific'];
  }

  foreach ($specific as $name => $section) {
    if (!empty($section)) {
      $fname = 'uw_student_fee_estimator_admin_migrate_export_' . $section;

      if (!empty($specific[$section]) && function_exists($fname)) {
        $result[$section] = call_user_func($fname);
      }
    }
  }

  // Resmeal will be triggered only when residence and meal plan are selected.
  if ($specific['residence'] && $specific['mealplan']) {
    $result['resmeal'] = uw_student_fee_estimator_admin_migrate_export_resmeal();
  }

  $outcome = '';
  if (!empty($result)) {
    $folder = variable_get('uw_student_fee_estimator_export_path', 'sfe_export');
    $output_folder = 'private://' . $folder . '/';

    if (file_prepare_directory($output_folder, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      $output_file_name = uniqid('sfe_raw_') . '_' . format_date(REQUEST_TIME, 'custom', 'Y-m-d') . '.bin';
      $file_path = $output_folder . $output_file_name;

      if (file_put_contents($file_path, serialize($result))) {
        $outcome = file_create_url($file_path);
      }
    }
  }

  return $outcome;
}

/**
 * Migration - importing data.
 *
 * @param array $values
 * @param $storage
 *
 * @return bool
 */
function uw_student_fee_estimator_admin_migrate_form_submit_import(array $values, $storage) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  $result = TRUE;
  $sections = ['mealplan', 'residence', 'program', 'taxonomy'];
  $specific = array_filter($values['import']);

  if (empty($specific)) {
    // Run for all
    $specific = array_combine($sections, $sections);
  }

  // There needs to be a file uploaded.
  if (!empty($storage['input_file']) && $file = $storage['input_file']) {
    $transaction = db_transaction();
    if ($values['input_delete'] && _uw_student_fee_estimator_delete_all_records()) {
        drupal_set_message(t('All student fee estimator data deleted.'));
    }

    try {
      $file_contents = file_get_contents($file->uri);

      if ($file_contents) {
        $data = unserialize($file_contents);

        if (!empty($specific['taxonomy']) && !empty($data['taxonomy'])) {
          uw_student_fee_estimator_admin_migrate_import_taxonomy($data['taxonomy']);
        }

        if (!empty($specific['mealplan']) && !empty($data['mealplan'])) {
          uw_student_fee_estimator_admin_migrate_import_mealplan($data['mealplan']);
        }

        if (!empty($specific['residence']) && !empty($data['residence'])) {
          uw_student_fee_estimator_admin_migrate_import_residence($data['residence']);
        }

        if (!empty($specific['program']) && !empty($data['program'])) {
          uw_student_fee_estimator_admin_migrate_import_program($data['program']);
        }

        if ((!empty($specific['mealplan']) && !empty($specific['residence'])) && !empty($data['resmeal'])) {
          uw_student_fee_estimator_admin_migrate_import_resmeal($data['resmeal']);
        }
      }
    }
    catch (Exception $ex) {
      $transaction->rollback();
      drupal_set_message(t('There was an error in transaction process. Error message: @mess', ['@mess' => $ex->getMessage()]), 'error');
      $result = FALSE;
    }

    // Clean up.
    file_delete($file);
  }

  if ($result) {
    drupal_set_message(t('Import process completed.'));
  }

  return $result;
}

/**
 * Exports all meal plans and return result as assoc array.
 *
 * @return mixed | array
 *   All records as associative array.
 */
function uw_student_fee_estimator_admin_migrate_export_mealplan() {
  return db_select(UW_STUDENT_FEE_ESTIMATOR_TABLE_MEALS, 'm')
    ->fields('m')
    ->execute()
    ->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * Exports all residence records as associative array.
 *
 * @return mixed
 *   All residences.
 */
function uw_student_fee_estimator_admin_migrate_export_residence() {
  return db_select(UW_STUDENT_FEE_ESTIMATOR_TABLE_RESIDENCES, 'r')
    ->fields('r')
    ->execute()
    ->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * Export all programs records.
 *
 * @return mixed
 *   All programs (tuitions).
 */
function uw_student_fee_estimator_admin_migrate_export_program() {
  // Load all programs, keep in mind that grd_type is referencing local taxonomy
  // term id needs to be replaced with name.
  $programs = db_select(UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION, 't')
    ->fields('t')
    ->execute()
    ->fetchAll(PDO::FETCH_ASSOC);

  $group = [];
  $vocab = taxonomy_vocabulary_machine_name_load(UW_STUDENT_FEE_ESTIMATOR_GRD_PROGRAM_TYPES);

  if ($vocab) {
    $terms = taxonomy_get_tree($vocab->vid);
    foreach ($terms as $object) {
      $group[$object->tid] = (array) $object;
    }
  }

  foreach ($programs as $index => $program) {
    // Update grd_type which is a number with name from term. Apply on to GRD.
    if ($program['career'] === 'GRD') {
      $grd_type = $program['grd_type'];
      if (!empty($group[$grd_type]['name'])) {
        $programs[$index]['grd_type'] = $group[$grd_type]['name'];
      }
    }
  }

  return $programs;
}

/**
 * Export taxonomy as array.
 *
 * @return array
 *   All taxonomy data.
 */
function uw_student_fee_estimator_admin_migrate_export_taxonomy() {
  $result = [];

  $vocab = taxonomy_vocabulary_machine_name_load(UW_STUDENT_FEE_ESTIMATOR_GRD_PROGRAM_TYPES);

  if ($vocab) {
    $objects = taxonomy_get_tree($vocab->vid);
    foreach ($objects as $object) {
      $result[] = (array) $object;
    }
  }

  return $result;
}

/**
 * Exports meal plan and residence data table.
 *
 * @return mixed
 *   Resmeal data. Since keys are now based on name, we can migrate.
 */
function uw_student_fee_estimator_admin_migrate_export_resmeal() {
  return db_select(UW_STUDENT_FEE_ESTIMATOR_TABLE_RESMEAL, 'rs')
    ->fields('rs')
    ->execute()
    ->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * Imports taxonomy data and creates vocabulary if needed.
 *
 * @param $values
 *   All terms as associative array.
 *
 * @return int
 *   Number of records created.
 */
function uw_student_fee_estimator_admin_migrate_import_taxonomy($values) {
  $new_terrms_count = 0;
  $vocab = taxonomy_vocabulary_machine_name_load(UW_STUDENT_FEE_ESTIMATOR_GRD_PROGRAM_TYPES);

  if (!$vocab) {
    $vocab = new stdClass();
    $vocab->name = 'UW Student Fee Estimator GRD Program Types';
    $vocab->description = 'Vocabulary to hold all graduate program types/classes for UW Student Fee Estimator module.';
    $vocab->machine_name = 'uw_student_fee_estimator_grd_program_types';
    taxonomy_vocabulary_save($vocab);
    drupal_set_message(t('Taxonomy vocabulary @name missing. It was created.', ['@name' => UW_STUDENT_FEE_ESTIMATOR_GRD_PROGRAM_TYPES]));
  }

  // Iterate all taxonomy values from file and create term is not found.
  foreach ($values as $value) {
    if (!taxonomy_get_term_by_name($value['name'], UW_STUDENT_FEE_ESTIMATOR_GRD_PROGRAM_TYPES)) {
      // Create new taxonomy term and add it to existing vocabulary.
      $new_term = new stdClass();
      $new_term->name = check_plain($value['name']);
      $new_term->vid = $vocab->vid;

      taxonomy_term_save($new_term);
      $new_terrms_count++;
    }
  }

  return $new_terrms_count;
}

/**
 * Imports meal plan data.
 *
 * @param $values
 *   Meal plans as assoc array.
 */
function uw_student_fee_estimator_admin_migrate_import_mealplan($values) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  foreach ($values as $value) {
    _uw_student_fee_estimator_mealplan_merge($value);
  }
}

/**
 * Imports residence data.
 *
 * @param $values
 *   Residence data as assoc array.
 */
function uw_student_fee_estimator_admin_migrate_import_residence($values) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  foreach ($values as $value) {
    _uw_student_fee_estimator_residence_merge($value);
  }
}

/**
 * Imports tuition (program) data.
 *
 * @param $values
 *   Program (tuition) data.
 */
function uw_student_fee_estimator_admin_migrate_import_program($values) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  foreach ($values as $value) {
    // Remove tid (tuition id) from value, this will trigger new record to be created.
    unset($value['tid']);
    // Check taxonomy for grd_type for GRD only records.
    if ($value['career'] === 'GRD') {
      $grd_type = $value['grd_type'];
      $term = taxonomy_get_term_by_name($grd_type, UW_STUDENT_FEE_ESTIMATOR_GRD_PROGRAM_TYPES);

      if ($term) {
        $term = reset($term);
        $value['grd_type'] = $term->tid;
      }
    }

    _uw_student_fee_estimator_tuition_merge($value);
  }
}

/**
 * Imports connection between meal plan and residence.
 *
 * @param $values
 */
function uw_student_fee_estimator_admin_migrate_import_resmeal($values) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  foreach ($values as $value) {
    $input = [
      'table' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESMEAL,
      'fields' => $value,
    ];

    _uw_student_fee_estimator_merge_record($input);
  }
}
