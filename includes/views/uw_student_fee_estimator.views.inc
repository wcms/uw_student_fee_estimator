<?php

/**
 * @file
 * Views schema definition for UW Student Fee Estimator.
 */

/**
 * Implements hook_views_data().
 */
function uw_student_fee_estimator_views_data() {
  $custom_handlers = [
    'views_handler_uw_sfe_citizenship',
    'views_handler_uw_sfe_academic_load',
    'views_handler_uw_sfe_form_of_study',
    'views_handler_uw_sfe_amount',
  ];

  foreach ($custom_handlers as $custom_handler) {
    if (!class_exists($custom_handler)) {
      module_load_include('inc', 'uw_student_fee_estimator', 'includes/views/handlers/' . $custom_handler);
    }
  }

  $field_numeric = [
    'field' => [
      'handler' => 'views_handler_uw_sfe_amount',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  $field_string = [
    'field' => [
      'handler' => 'views_handler_field',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $field_date = [
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
  ];

  $field_boolean = [
    'field' => [
      'handler' => 'views_handler_field_boolean',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  $data = [];

  $table = UW_STUDENT_FEE_ESTIMATOR_TABLE_MEALS;

  $data[$table]['table']['group'] = t('UW Student Fee Estimator - Meal plans');
  $data[$table]['table']['base'] = [
    'title' => t('UW Student Fee Estimator - Meal plans'),
    'help' => t('Add/remove/edit available meal plans.'),
  ];

  $data[$table]['mid'] = [
    'title' => t('Meal plan Id'),
    'help' => t('Unique identifier for a meal plan.'),
  ] + $field_string;

  $data[$table]['name'] = [
    'title' => t('Meal plan name'),
    'help' => t('Name/Title for meal plan'),
  ] + $field_string;

  $data[$table]['price'] = [
    'title' => t('Price for one term'),
    'help' => t('Per term cost of meal plan'),
  ] + $field_numeric;

  $data[$table]['created'] = [
    'title' => t('Created'),
    'help' => t('Timestamp when meal plan was created.'),
  ] + $field_date;

  $data[$table]['updated'] = [
    'title' => t('Updated'),
    'help' => t('Timestamp when meal plan was updated.'),
  ] + $field_date;

  $data[$table]['status'] = [
    'title' => t('Status'),
    'help' => t('Submission status, as number or label.'),
  ] + $field_boolean;

  $table = UW_STUDENT_FEE_ESTIMATOR_TABLE_RESIDENCES;

  $data[$table]['table']['group'] = t('UW Student Fee Estimator - Residences');
  $data[$table]['table']['base'] = [
    'title' => t('UW Student Fee Estimator - Residences'),
    'help' => t('Add/remove/edit available residences.'),
  ];

  $data[$table]['rid'] = [
    'title' => t('Residence Id'),
    'help' => t('Unique identifier for a residence.'),
  ] + $field_string;

  $data[$table]['career'] = [
      'title' => t('Career'),
      'help' => t('Academic career could be UG, GR or all.'),
    ] + $field_string;

  $data[$table]['name'] = [
    'title' => t('Residence name'),
    'help' => t('Name/Title for residence.'),
  ] + $field_string;

  $data[$table]['price'] = [
    'title' => t('Price for one term'),
    'help' => t('Per term cost for residence.'),
  ] + $field_numeric;

  $data[$table]['created'] = [
    'title' => t('Created'),
    'help' => t('Timestamp when residence record was created.'),
  ] + $field_date;

  $data[$table]['updated'] = [
    'title' => t('Updated'),
    'help' => t('Timestamp when residence record was updated.'),
  ] + $field_date;

  $data[$table]['status'] = [
    'title' => t('Status'),
    'help' => t('Submission status, as number or label.'),
  ] + $field_boolean;

  $data[$table]['meal_plan_included'] = [
    'title' => t('Meal plan included'),
    'help' => t('If meal plan is included in price of residence.'),
  ] + $field_boolean;

  $data[$table]['meal_plan_required'] = [
    'title' => t('Meal plan required'),
    'help' => t('If meal plan is required in residence.'),
  ] + $field_boolean;

  $table = UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION;
  $data[$table]['table']['group'] = t('UW Student Fee Estimator - Tuition');

  $data[$table]['table']['base'] = [
    'title' => t('UW Student Fee Estimator - Tuition'),
    'help' => t('Add/remove/edit available tuition.'),
  ];

  $data[$table]['online'] = [
    'title' => t('Online'),
    'help' => t('If course is only online.'),
  ] + $field_boolean;

  $data[$table]['pdf_template'] = [
    'title' => t('PDF Template'),
    'relationship' => [
      'base' => 'uw_student_fee_estimator_pdf_template',
      'base field' => 'eid',
      'handler' => 'views_handler_relationship',
      'label' => t('PDF Template'),
      'help' => t('Template override for this tuition record.'),
    ],
  ];

  $data[$table]['tid'] = [
    'title' => t('Tuition Id'),
    'help' => t('Unique identifier for tuition record.'),
  ] + $field_string;

  $data[$table]['term_code'] = [
    'title' => t('Term code'),
    'help' => t('UW term code'),
  ] + $field_string;

  $data[$table]['career'] = [
    'title' => t('Career'),
    'help' => t('Academic career'),
  ] + $field_string;

  $data[$table]['program_description'] = [
    'title' => t('Program description'),
    'help' => t('Short description for program.'),
  ] + $field_string;

  $data[$table]['increase'] = [
    'title' => t('Assumed increase'),
    'help' => t('Percentage used in tuition calculations per year increase.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  $data[$table]['academic_load'] = [
    'title' => t('Academic load'),
    'help' => t('Academic load: full time or part time.'),
    'field' => [
      'handler' => 'views_handler_uw_sfe_academic_load',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$table]['form_of_study'] = [
    'title' => t('Form of study'),
    'help' => t('Form of study: regular or co-op.'),
    'field' => [
      'handler' => 'views_handler_uw_sfe_form_of_study',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$table]['grd_type'] = [
    'title' => t('GRD Program type'),
    'relationship' => [
      'base' => 'taxonomy_term_data',
      'base field' => 'tid',
      'handler' => 'views_handler_relationship',
      'label' => t('GDR Program type'),
      'help' => t('When added to relation details about taxonomy term can displayed.'),
    ],
  ];

  $data[$table]['citizenship'] = [
    'title' => t('Tuition residency'),
    'help' => t('Legal status in Canada.'),
    'field' => [
      'handler' => 'views_handler_uw_sfe_citizenship',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$table]['per_term'] = [
    'title' => t('Tuition per term'),
    'help' => t('Estimated tuition amount per term for GRD students.'),
  ] + $field_numeric;

  $data[$table]['per_course'] = [
    'title' => t('Tuition per course'),
    'help' => t('Estimated tuition amount per course for GRD students.'),
  ] + $field_numeric;

  $data[$table]['per_term_inc'] = [
    'title' => t('Tuition per term incidental'),
    'help' => t('Estimated incidental fee amount per term for GRD students.'),
  ] + $field_numeric;

  $data[$table]['T_total'] = [
    'title' => t('Tuition total'),
    'help' => t('Estimate tuition cost for whole program'),
  ] + $field_numeric;

  $data[$table]['IF_total'] = [
    'title' => t('Incidental fees total'),
    'help' => t('Estimate incidental cost for whole program'),
  ] + $field_numeric;

  $data[$table]['WRM_total'] = [
    'title' => t('WRM fees total'),
    'help' => t('Estimate WRM cost for whole program'),
  ] + $field_numeric;

  $data[$table]['created'] = [
    'title' => t('Created'),
    'help' => t('Timestamp when tuition record was created.'),
  ] + $field_date;

  $data[$table]['updated'] = [
    'title' => t('Updated'),
    'help' => t('Timestamp when residence record was updated.'),
  ] + $field_date;

  $data[$table]['status'] = [
    'title' => t('Status'),
    'help' => t('Submission status, as number or label.'),
  ] + $field_boolean;

  foreach (range(1, 5) as $term) {
    foreach (['', 'A', 'B'] as $code) {
      // Tuition costs fields format.
      $field = $term . $code;
      $id = 'T' . $field;

      $data[$table][$id] = [
        'title' => $field,
        'help' => t('Estimated cost for term @tcode.', ['@tcode' => $field]),
      ] + $field_numeric;

      // Incidental fees fields.
      $inc_field = 'IF' . $field;

      $data[$table][$inc_field] = [
        'title' => $inc_field,
        'help' => t('Estimated cost of incidental fees for term @tcode.', ['@tcode' => $field]),
      ] + $field_numeric;

      // Co-op values fields.
      $inc_field = 'WRM' . $field;

      $data[$table][$inc_field] = [
        'title' => $inc_field,
        'help' => t('Estimated cost of WRM fees for term @tcode.', ['@tcode' => $field]),
      ] + $field_numeric;
    }
  }

  $table = UW_STUDENT_FEE_ESTIMATOR_TABLE_SUBMISSIONS;

  $data[$table]['table']['group'] = t('UW Student Fee Estimator - Submissions');

  $data[$table]['table']['base'] = [
    'title' => t('UW Student Fee Estimator - Submissions'),
    'help' => t('Add/remove/edit submissions.'),
  ];

  $data[$table]['sid'] = [
    'title' => t('Submission Id'),
    'help' => t('Unique identifier for a submission.'),
  ] + $field_string;

  $data[$table]['term_code'] = [
    'title' => t('Term code'),
    'help' => t('UW term code'),
  ] + $field_string;

  $data[$table]['name'] = [
    'title' => t('Full name'),
    'help' => t('Name of person requesting estimate.'),
  ] + $field_string;

  $data[$table]['first_name'] = [
      'title' => t('First name'),
      'help' => t('First name of person requesting estimate.'),
    ] + $field_string;

  $data[$table]['last_name'] = [
      'title' => t('Last name'),
      'help' => t('Last name of person requesting estimate.'),
    ] + $field_string;

  $data[$table]['email'] = [
    'title' => t('Email'),
    'help' => t('Email of person requesting estimate.'),
  ] + $field_string;

  $data[$table]['student_number'] = [
    'title' => t('Student number'),
    'help' => t('Student number of person requesting estimate.'),
  ] + $field_string;

  $data[$table]['student_career'] = [
    'title' => t('Academic career'),
    'help' => t('Academic career for estimate.'),
  ] + $field_string;

  $data[$table]['rid'] = [
    'title' => t('Residence'),
    'relationship' => [
      'base' => UW_STUDENT_FEE_ESTIMATOR_TABLE_RESIDENCES,
      'base field' => 'rid',
      'handler' => 'views_handler_relationship',
      'label' => t('Residence'),
      'help' => t('Linked residence'),
    ],
  ];

  $data[$table]['mid'] = [
    'title' => t('Meal plan'),
    'relationship' => [
      'base' => UW_STUDENT_FEE_ESTIMATOR_TABLE_MEALS,
      'base field' => 'mid',
      'handler' => 'views_handler_relationship',
      'label' => t('Meal plan'),
      'help' => t('Linked meal plan'),
    ],
  ];

  $data[$table]['citizenship'] = [
    'title' => t('Tuition residency'),
    'help' => t('Legal status in Canada.'),
    'field' => [
      'handler' => 'views_handler_uw_sfe_citizenship',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$table]['tid'] = [
    'title' => t('Tuition'),
    'relationship' => [
      'base' => UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION,
      'base field' => 'tid',
      'handler' => 'views_handler_relationship',
      'label' => t('Tuition'),
      'help' => t('Linked tuition'),
    ],
  ];

  $data[$table]['requested_interval'] = [
    'title' => t('Interval'),
    'help' => t('Interval for estimate'),
  ] + $field_string;

  $data[$table]['pdf_file'] = [
    'title' => t('PDF File'),
    'help' => t('Generated PDF file.'),
    'field' => [
      'handler' => 'views_handler_field_file_uri',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$table]['form_of_study'] = [
    'title' => t('Form of study'),
    'help' => t('For UG students, available options are regular (REG) and co-op (CO-OP) studies.'),
  ] + $field_string;

  $data[$table]['academic_load'] = [
    'title' => t('Academic load'),
    'help' => t('For GRD students, available options are full time (FT) and part time (PT).'),
  ] + $field_string;

  $data[$table]['number_of_courses'] = [
    'title' => t('Number of courses'),
    'help' => t('Some requested intervals require student to provide number of courses in order to calculate estimate.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  $data[$table]['number_of_terms'] = [
    'title' => t('Number of terms'),
    'help' => t('Some requested intervals require student to provide number of terms for selected program.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  $data[$table]['notification'] = [
    'title' => t('Notification'),
    'help' => t('Timestamp when notification was sent.'),
  ] + $field_date;

  $data[$table]['created'] = [
    'title' => t('Created'),
    'help' => t('Timestamp when a submission record was created.'),
  ] + $field_date;

  $data[$table]['updated'] = [
    'title' => t('Updated'),
    'help' => t('Timestamp when a submission record was updated.'),
  ] + $field_date;

  $data[$table]['status'] = [
    'title' => t('Status'),
    'help' => t('Submission status, as number or label.'),
  ] + $field_boolean;

  $table = UW_STUDENT_FEE_ESTIMATOR_TABLE_PDF_TEMPLATE;

  $data[$table]['table']['group'] = t('UW Student Fee Estimator - PDF Template');

  $data[$table]['table']['base'] = [
    'title' => t('UW Student Fee Estimator - PDF Template'),
    'help' => t('Add/remove/edit PDF templates.'),
  ];

  $data[$table]['eid'] = [
    'title' => t('PDF Template Id'),
    'help' => t('Unique identifier for a PDF template record.'),
  ] + $field_string;

  $data[$table]['name'] = [
    'title' => t('PDF Template name'),
    'help' => t('Human readable name that can be used as select box label.'),
  ] + $field_string;

  $data[$table]['created'] = [
    'title' => t('Created'),
    'help' => t('Timestamp when a PDF template record was created.'),
  ] + $field_date;

  $data[$table]['updated'] = [
    'title' => t('Updated'),
    'help' => t('Timestamp when a PDF template record was updated.'),
  ] + $field_date;

  $data[$table]['status'] = [
    'title' => t('Status'),
    'help' => t('Submission status, as number or label.'),
  ] + $field_boolean;

  $data[$table]['default_template'] = [
    'title' => t('Default'),
    'help' => t('Is template default template.'),
  ] + $field_boolean;

  return $data;
}
