<?php
/**
 * @file
 *
 * Custom handler for cent/dollar amount.
 */


/**
 * Class views_handler_uw_sfe_amount
 */
class views_handler_uw_sfe_amount extends views_handler_field {

  /**
   * Field available options.
   *
   * @return array
   *   List of options.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['style'] = ['default' => 'dollars'];
    $options['use_separator'] = ['default' => TRUE];
    $options['display_cents'] = ['default' => TRUE];

    return $options;
  }

  /**
   * Field option form.
   *
   * @param $form
   *   Form displayed on view field.
   * @param $form_state
   *   Form state values.
   */
  public function options_form(&$form, &$form_state) {
    $form['style'] = [
      '#type' => 'select',
      '#title' => t('Display amount in'),
      '#options' => [
        'cents' => t('Cents'),
        'dollars' => t('Dollars')
      ],
      '#default_value' => $this->options['style'],
      '#description' => t('Values are stored as cents in database, use this to transform.'),
    ];

    $form['use_separator'] = [
      '#type' => 'checkbox',
      '#title' => t('Use thousand separator'),
      '#default_value' => $this->options['use_separator'],
      '#description' => t('Use thousand separator when displaying amount.'),
    ];

    $form['display_cents'] = [
      '#type' => 'checkbox',
      '#title' => t('Display cents'),
      '#default_value' => $this->options['display_cents'],
      '#description' => t('Use two decimal points to display 1/100.'),
      '#states' => [
        'visible' => [
          'select[name="options[style]"]' => ['value' => 'dollars']
        ],
      ],
    ];

    parent::options_form($form, $form_state);
  }


  /**
   * Displays dollar value from stored cents values.
   *
   * @param object $values
   *   Row values.
   * @return string
   *   Modified output.
   */
  public function render($values) {
    $result = $value = $this->get_value($values);

    if ($value) {
      $int_value = (int)$value;
      $style = $this->options['style'];
      $display_cents = $this->options['display_cents'] ? 2 : 0;
      $separator = $this->options['use_separator'] && $style === 'dollars' ? ',' : '';

      if ($style === 'dollars') {
        $result = $int_value / 100;
      }

      if ($separator || $display_cents) {
        $result = number_format($result, $display_cents, '.', $separator);
      }
    }

    return $result;
  }

}
