<?php

/**
 * @file
 * Citizenship custom handler.
 *
 * Used in uw_student_fee_estimator_tuition view.
 */

/**
 * Class views_handler_uw_sfe_citizenship.
 *
 * @ingroup views_field_handlers
 */
class views_handler_uw_sfe_citizenship extends views_handler_field {

  /**
   * Field available options.
   *
   * @return array
   *   Options returned as array
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['style'] = ['default' => 'label'];

    return $options;
  }

  /**
   * Options form.
   *
   * @param array|bool $form
   *   Form displayed on view.
   * @param array|bool $form_state
   *   Form state values.
   */
  public function options_form(&$form, &$form_state) {
    $form['style'] = [
      '#type' => 'select',
      '#title' => t('Display'),
      '#options' => [
        'label' => t('Label'),
      ],
      '#default_value' => $this->options['style'],
      '#description' => t('Displays label for citizenship field.'),
    ];

    parent::options_form($form, $form_state);
  }

  /**
   * Render method used for display.
   *
   * @param array|bool $values
   *   Row values.
   *
   * @return string
   *   Modified display.
   */
  public function render($values) {
    $value = $this->get_value($values);
    module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
    $status = _uw_student_fee_estimator_citizenship($value);

    return is_string($status) ? $status : 'Unknown';
  }

}
