<?php

/**
 * @file
 * Custom handler for program type.
 */

/**
 * Class views_handler_uw_sfe_program_type.
 */
class views_handler_uw_sfe_academic_load extends views_handler_field {

  /**
   * Render method used to display field.
   *
   * @param object $values
   *   Row values.
   *
   * @return string
   *   Modified output.
   */
  public function render($values) {
    module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
    $value = $values->{$this->field_alias};

    return _uw_student_fee_estimator_academic_load(strtoupper($value));
  }

}
