<?php

/**
 * @file
 * Custom handler for program type.
 */

/**
 * Class views_handler_uw_sfe_program_type.
 */
class views_handler_uw_sfe_form_of_study extends views_handler_field {

  /**
   * Render method for display.
   *
   * @param array|bool $values
   *   Row values.
   *
   * @return string
   *   Modified value used for display.
   */
  public function render($values) {
    $value = $values->{$this->field_alias};

    if ($value) {
      $value = _uw_student_fee_estimator_form_of_study(strtoupper($value));
    }

    return $value;
  }

}
