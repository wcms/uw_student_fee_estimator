<?php

/**
 * @file
 * UW Student Fee Estimator Admin file.
 */

/**
 * Module's admin form.
 */
function uw_student_fee_estimator_admin_form($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $tuition_count = _uw_student_fee_estimator_database_record_count([UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION]);

  // Disable form if there is no tuition data.
  if (!$tuition_count) {
    variable_set_value('uw_student_fee_estimator_form_published', FALSE);
  }

  $form['group0'] = [
    '#type' => 'fieldset',
    '#title' => t('Global settings'),
  ];

  $form['group0']['uw_student_fee_estimator_form_published'] = [
    '#type' => 'checkbox',
    '#title' => t('<strong>Form enabled (published)</strong>'),
    '#description' => t('Control switch to make form available (open) to submissions or not. Make sure there is tuition data before you publish form.'),
    '#default_value' => variable_get('uw_student_fee_estimator_form_published', FALSE),
    '#disabled' => !$tuition_count[UW_STUDENT_FEE_ESTIMATOR_TABLE_TUITION],
  ];

  $form['group0']['uw_student_fee_estimator_maintenance_mode_message'] = [
    '#type' => 'textarea',
    '#title' => t('Maintenance mode message'),
    '#default_value' => variable_get('uw_student_fee_estimator_maintenance_mode_message', t('Form is closed for submissions.')),
    '#states' => [
      'visible' => [
        ':input[name="uw_student_fee_estimator_form_published"]' => ['checked' => FALSE],
      ],
      'required' => [
        ':input[name="uw_student_fee_estimator_form_published"]' => ['checked' => FALSE],
      ],
    ],
  ];

  $form['group0']['uw_student_fee_estimator_email_override_status'] = [
    '#type' => 'checkbox',
    '#title' => t('Email override'),
    '#default_value' => variable_get('uw_student_fee_estimator_email_override_status'),
    '#description' => t('If enabled, all email will be rerouted to provided email address. Useful when testing.'),
  ];

  $form['group0']['uw_student_fee_estimator_email_override_email'] = [
    '#type' => 'textfield',
    '#title' => t('Override email address'),
    '#description' => t('This email address will override student email address and all estimates will be sent to it.'),
    '#default_value' => variable_get('uw_student_fee_estimator_email_override_email', ''),
    '#size' => 30,
    '#states' => [
      'visible' => [
        ':input[name="uw_student_fee_estimator_email_override_status"]' => ['checked' => TRUE],
      ],
      'required' => [
        ':input[name="uw_student_fee_estimator_email_override_status"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  ];

  $form['group1']['uw_student_fee_estimator_current_term'] = [
    '#type' => 'textfield',
    '#title' => t('Current term code'),
    '#description' => t('Make sure to use current and correct term code, UW uses. Examples: 1189, 1191, 1195 etc. Generated PDF files will be grouped by this code.'),
    '#default_value' => variable_get('uw_student_fee_estimator_current_term'),
    '#size' => 10,
    '#required' => TRUE,
  ];

  $form['group1']['uw_student_fee_estimator_total_rounding'] = [
    '#type' => 'select',
    '#title' => t('Round up total value to value of'),
    '#options' => [
      0 => t('No rounding'),
      10 => t('10'),
      50 => t('50'),
      100 => t('100'),
      500 => t('500'),
      1000 => t('1000'),
    ],
    '#description' => t('After all has been calculated, this logic will round up number to closest number selected.'),
    '#default_value' => variable_get('uw_student_fee_estimator_total_rounding', 100),
  ];

  $form['group1']['uw_student_fee_estimator_thank_you_url'] = [
    '#type' => 'textfield',
    '#title' => t('Thank you page url'),
    '#description' => t('Internal URL of thank you page that may be any page on site. Do not start with slash. Leaving empty will default to redirect to form.'),
    '#default_value' => variable_get('uw_student_fee_estimator_thank_you_url', 'student-fee-estimator-thank-you'),
  ];

  $form['group1']['uw_student_fee_estimator_pdf_path'] = [
    '#type' => 'textfield',
    '#title' => t('Generated PDF files destination path'),
    '#description' => t("By default these files are stored on Drupal's private folder. You can specify sub-folder, all generated files will be stored in term code and created month. Default value: <em>sfe_pdf</em>."),
    '#default_value' => variable_get('uw_student_fee_estimator_pdf_path', 'sfe_pdf'),
    '#size' => 20,
    '#field_prefix' => 'private://',
    '#required' => TRUE,
  ];

  $form['group1']['uw_student_fee_estimator_export_path'] = [
    '#type' => 'textfield',
    '#title' => t('Export files generated path'),
    '#description' => t('Content is not considered protected and export files will be generated on public folder path, you can specify subfolder here. Default value: <em>sfe_export</em>.'),
    '#default_value' => variable_get('uw_student_fee_estimator_export_path', 'sfe_export'),
    '#size' => 20,
    '#field_prefix' => 'private://',
    '#required' => TRUE,
  ];

  $form['group2'] = [
    '#type' => 'fieldset',
    '#title' => t('Development / Testing'),
  ];

  $form['group2']['uw_student_fee_estimator_mode_ajax'] = [
    '#type' => 'checkbox',
    '#title' => t('Use Ajax for PDF Generator'),
    '#description' => t('With this enabled you there is no whole page reload, can improve testing.'),
    '#default_value' => variable_get('uw_student_fee_estimator_mode_ajax'),
  ];

  // Adding submit handler manually since we use system_setting_form
  // which overrides default handler.
  $form['#submit'][] = 'uw_student_fee_estimator_admin_form_submit';

  return system_settings_form($form);
}

/**
 * Validate handler for admin form.
 */
function uw_student_fee_estimator_admin_form_validate($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $values = $form_state['values'];
  $term = $values['uw_student_fee_estimator_current_term'];

  if (!_uw_student_fee_estimator_term_validate($term)) {
    form_set_error('uw_student_fee_estimator_current_term', t('Provided term code (@code) does not looks valid. Please update.', ['@code' => $term]));
  }

  $thank_you_page_url = $values['uw_student_fee_estimator_thank_you_url'];

  if (!empty($thank_you_page_url)) {
    $internal = drupal_lookup_path('source', $thank_you_page_url);

    if (!$internal) {
      form_set_error('uw_student_fee_estimator_thank_you_url', t('Not valid internal URI, do not start with slash.'));
    }
  }

}

/**
 * Submit handler triggers menu rebuild only when mode is toggled.
 */
function uw_student_fee_estimator_admin_form_submit($form, &$form_state) {
  $term = $form_state['values']['uw_student_fee_estimator_current_term'];
  $current_term_setting = variable_get('uw_student_fee_estimator_current_term');

  if ($term && $term !== $current_term_setting) {
    $all_terms = variable_get('uw_student_fee_estimator_terms');
    $all_terms[] = $term;
    $all_terms = array_unique($all_terms);
    sort($all_terms);
    variable_set('uw_student_fee_estimator_terms', $all_terms);
  }
}
