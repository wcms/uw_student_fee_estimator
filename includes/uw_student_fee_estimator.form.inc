<?php

/**
 * @file
 * Multi-step wizard form.
 */

/**
 * Page callback from menu.
 */
function uw_student_fee_estimator_page_callback() {
  $form_published = variable_get('uw_student_fee_estimator_form_published');

  if (!$form_published && !user_access('administer student fee estimator')) {
    drupal_set_title(t('Form closed'));
    $message = variable_get('uw_student_fee_estimator_maintenance_mode_message', t('Form is closed for submissions.'));
    $output = check_markup($message, 'uw_tf_standard_wide');
  }
  else {
    $output = drupal_get_form('uw_student_fee_estimator_form_wizard');
  }

  return $output;
}

/**
 * Wizard steps.
 *
 * @return array
 *   Wizard steps.
 */
function _uw_student_fee_estimator_form_wizard_steps() {
  $steps = [];

  $messages = [
    1 => t('Student information'),
    2 => t('Academic details'),
    3 => t('Program'),
    4 => t('Program details'),
    5 => t('Residence'),
    6 => t('Meal plan'),
    7 => t('Review the options you selected for your estimate'),
  ];

  for ($i = 1; $i <= count($messages); $i++) {
    $steps[$i] = [
      'form' => 'uw_student_fee_estimator_form_wizard_step_' . $i,
      'message' => $messages[$i],
    ];
  }

  return $steps;
}

/**
 * Wizard main form.
 *
 * @param array|bool $form
 *   Form definition.
 * @param array|bool $form_state
 *   Form values.
 *
 * @return mixed
 *   Wizard form.
 */
function uw_student_fee_estimator_form_wizard($form, &$form_state) {
  // Display maintenance message if form is not published.
  $form_published = variable_get('uw_student_fee_estimator_form_published');

  // Initialize a description of the steps for the wizard.
  if (empty($form_state['step'])) {
    $form_state['step'] = 1;
    $form_state['prev_step'] = 1;

    // This array contains the function to be called at each step to get the
    // relevant form elements. It will also store state information for each
    // step.
    $form_state['step_information'] = _uw_student_fee_estimator_form_wizard_steps();
  }

  $step = &$form_state['step'];
  $form_title = t('@message', ['@message' => $form_state['step_information'][$step]['message']]);

  // Call the function named in $form_state['step_information'] to get the
  // form elements to display for this step.
  $form = $form_state['step_information'][$step]['form']($form, $form_state);

  if (!$form_published) {
    $form['#attributes']['class'] = ['form-unpublished-admin'];
    $form_title .= ' ' . t('(Unpublished)');
  }

  drupal_set_title($form_title);

  if ($step > 1) {
    $form['prev'] = [
      '#type' => 'submit',
      '#value' => t('Previous'),
      '#name' => 'prev',
      '#submit' => ['uw_student_fee_estimator_form_wizard_previous_submit'],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['estimator-submit-prev'],
      ],
    ];
  }

  // Show the Next button only if there are more steps defined.
  if ($step < count($form_state['step_information'])) {
    // The Next button should be included on every step.
    $form['next'] = [
      '#type' => 'submit',
      '#value' => t('Next'),
      '#name' => 'next',
      '#attributes' => [
        'class' => ['estimator-submit-next'],
      ],
      '#submit' => ['uw_student_fee_estimator_form_wizard_next_submit'],
    ];
  }
  else {
    // Just in case there are no more steps, we use the default submit handler
    // of the form wizard. Call this button Finish, Submit, or whatever you
    // want to show. When this button is clicked, the
    // uw_student_fee_estimator_form_wizard_submit handler will be called.
    $form['finish'] = [
      '#type' => 'submit',
      '#value' => t('e-mail my estimate'),
      '#attributes' => [
        'class' => ['estimator-submit-next'],
      ],
    ];
  }

  // Include each validation function defined for the different steps.
  if (function_exists($form_state['step_information'][$step]['form'] . '_validate')) {
    $form['next']['#validate'] = [$form_state['step_information'][$step]['form'] . '_validate'];
  }

  return $form;
}

/**
 * Submit handler for the "previous" button.
 *
 * This function:
 * - Stores away $form_state['values']
 * - Decrements the step counter
 * - Replaces $form_state['values'] with the values from the previous state.
 * - Forces form rebuild.
 * You are not required to change this function.
 *
 * @ingroup form_example
 */
function uw_student_fee_estimator_form_wizard_previous_submit($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['input'];
  $initial_step = $current_step;

  if ($current_step > 1) {
    $tuition_id = 0;
    // Calculate special case that enables to skip 4th step.
    if (!empty($form_state['step_information'][3]['stored_values']['tuition'])) {
      $tuition_id = $form_state['step_information'][3]['stored_values']['tuition'];
    }
    $tuition = uw_student_fee_estimator_load_tuition($tuition_id);

    if ($current_step === 5) {
      $student_career = $form_state['step_information'][1]['stored_values']['student_career'];
      $requested_interval = $form_state['step_information'][2]['stored_values']['requested_interval'];

      if (_uw_student_fee_estimator_form_wizard_skip_step_4($student_career, $requested_interval, $tuition)) {
        $current_step--;
      }
    }
    elseif ($current_step === 7) {
      $residence = isset($form_state['step_information'][5]['stored_values']['residence']) ? $form_state['step_information'][5]['stored_values']['residence'] : 0;
      $academic_load = isset($form_state['step_information'][2]['stored_values']['academic_load']) ? $form_state['step_information'][2]['stored_values']['academic_load'] : '';
      $student_career = $form_state['step_information'][1]['stored_values']['student_career'];

      if (!$residence) {
        $current_step--;
      }

      if (($tuition && $tuition['online']) || ($student_career === 'GRD' && $academic_load === 'PT')) {
        $current_step -= 2;
      }
    }

    $current_step--;
    $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
  }

  // Remove stored values for step.
  unset($form_state['step_information'][$initial_step]['stored_values']);

  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the 'next' button.
 *
 * This function:
 * - Saves away $form_state['values']
 * - Increments the step count.
 * - Replace $form_state['values'] from the last time we were at this page
 *   or with array() if we haven't been here before.
 * - Force form rebuild.
 *
 * You are not required to change this function.
 *
 * @ingroup form_example
 */
function uw_student_fee_estimator_form_wizard_next_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];

  if ($current_step < count($form_state['step_information'])) {

    // Calculate special case that enables to skip 4th step (program details).
    if ($current_step === 3) {
      module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

      $student_career = $form_state['step_information'][1]['stored_values']['student_career'];
      $requested_interval = $form_state['step_information'][2]['stored_values']['requested_interval'];
      $tuition_id = $form_state['step_information'][3]['stored_values']['tuition'];
      $tuition = uw_student_fee_estimator_load_tuition($tuition_id);
      $academic_load = isset($form_state['step_information'][2]['stored_values']['academic_load']) ? $form_state['step_information'][2]['stored_values']['academic_load'] : '';

      if (_uw_student_fee_estimator_form_wizard_skip_step_4($student_career, $requested_interval, $tuition)) {
        $current_step++;

        // If course is only only skip residence and meal plan pages.
        if ($tuition['online'] || ($student_career === 'GRD' && $academic_load === 'PT')) {
          $current_step +=2;
        }
      }
    }
    elseif ($current_step === 4) {
      module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

      $tuition_id = $form_state['step_information'][3]['stored_values']['tuition'];
      $tuition = uw_student_fee_estimator_load_tuition($tuition_id);

      // If course is only only skip residence and meal plan pages.
      if ($tuition['online']) {
        $current_step +=2;
      }
    }
    elseif ($current_step === 5) {
      $residence = $form_state['step_information'][5]['stored_values']['residence'];

      if (!$residence) {
        $current_step++;
      }
    }
    $current_step++;

    if (!empty($form_state['step_information'][$current_step]['stored_values'])) {
      $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
    }
    else {
      $form_state['values'] = [];
    }
    // Force rebuild with next step.
    $form_state['rebuild'] = TRUE;
    return;
  }
}

/**
 * Wizard form submit handler.
 *
 * This function:
 * - Process all the form values.
 * - Saves values from $form_state['values']
 */
function uw_student_fee_estimator_form_wizard_submit($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.email');

  $result = NULL;
  $calculated = $form_state['calculated'];
  $submission_data = $form_state['combined'];

  $type = 'error';
  $message = t('There was an issue with your submission. Please try again or contact sfs@uwaterloo.ca for assistance.');

  // Maybe store submission, and it that is successful do PDF and notification.
  $submission = _uw_student_fee_estimator_submission_merge($submission_data);

  if ($submission) {
    // Need to create PDF and send notification to requester,
    // using sid to calculate hash, so we can recreate the file.
    $pdf_file = uw_student_fee_estimator_pdf_create($calculated, $submission);

    // Only when PDF file has been generated.
    // Otherwise inform admin that something went wrong.
    if ($pdf_file) {
      $submission_data['sid'] = $submission;
      $new_public_pdf = uw_student_fee_estimator_public_pdf($pdf_file, $submission_data);

      if ($new_public_pdf) {
        $result = uw_student_fee_estimator_email_notification($submission_data['email'], $new_public_pdf, $submission);
      }

      $message = t('Thank you for using the Fee Estimator. Your estimate request ID is @sid. ',
        ['@sid' => $submission]
      );

      if (isset($result['result']) && $result['result']) {
        $message .= t('Please allow several minutes for your estimate letter to arrive at the e-mail address you provided.');
        $type = 'status';
      }
      else {
        $message .= t('There was an issue trying to send email.');
        $type = 'warning';
      }

      $input = [
        'pdf_file' => $pdf_file,
        'notification' => REQUEST_TIME,
      ];

      // Updating submission with PDF uri.
      _uw_student_fee_estimator_submission_update($submission, $input);

      // Removing public PDF file, leaving only private version.
      file_unmanaged_delete($new_public_pdf);
    }
  }

  $thank_you_page = variable_get('uw_student_fee_estimator_thank_you_url');

  // If everything went well, and there is thank you page set, redirect to it.
  if ($type === 'status' && !empty($thank_you_page)) {
    $form_state['redirect'] = $thank_you_page;
  }
  else {
    drupal_set_message($message, $type);
  }
}

/**
 * Wizard step 1.
 *
 * @param array|bool $form
 *   Form fields for step 1.
 * @param array|bool $form_state
 *   Form values.
 *
 * @return mixed
 *   Form fields for step 1.
 */
function uw_student_fee_estimator_form_wizard_step_1($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $form['first_name'] = [
    '#type' => 'textfield',
    '#title' => t('First name(s)'),
    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['first_name']) ? $form_state['values']['first_name'] : '',
  ];

  $form['last_name'] = [
    '#type' => 'textfield',
    '#title' => t('Last name(s)'),
    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['last_name']) ? $form_state['values']['last_name'] : '',
  ];

  $form['email'] = [
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#description' => t('Your fee estimate will be sent to this e-mail address.'),
    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['email']) ? $form_state['values']['email'] : '',
  ];

  $form['student_number'] = [
    '#type' => 'textfield',
    '#title' => t('Student number'),
    '#description' => t('If you have been assigned a UWaterloo student ID number, you can enter it here.'),
    '#size' => 10,
    '#default_value' => isset($form_state['values']['student_number']) ? $form_state['values']['student_number'] : '',
  ];

  $form['citizenship'] = [
    '#type' => 'radios',
    '#title' => t('Tuition residency'),
    '#options' => _uw_student_fee_estimator_citizenship(),
    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['citizenship']) ? $form_state['values']['citizenship'] : 'ontarian',
  ];

  $form['student_career'] = [
    '#type' => 'radios',
    '#title' => t('Academic level'),
    '#options' => _uw_student_fee_estimator_career(),
    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['student_career']) ? $form_state['values']['student_career'] : 'UG',
  ];

  return $form;
}

/**
 * Validate handler for wizard step 1.
 */
function uw_student_fee_estimator_form_wizard_step_1_validate($form, &$form_state) {
  $values = $form_state['values'];
  $format = "/^\d{8}$/";

  $student_number = $values['student_number'];

  if ($student_number && !preg_match($format, $student_number)) {
    form_set_error('student_number', t('Provided student number format is not valid (must be 8 digits long or empty).'));
  }
}

/**
 * Wizard step 2 - estimate length, system of study, academic load.
 */
function uw_student_fee_estimator_form_wizard_step_2($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  $intervals = uw_student_fee_estimator_request_interval();
  $student_career = isset($form_state['step_information'][1]['stored_values']['student_career']) ? $form_state['step_information'][1]['stored_values']['student_career'] : NULL;

  // Removing program options for both UG and GRD.
  unset($intervals['program']);
  if ($student_career === 'GRD') {
    $intervals['year'] = 'One year (three four-month terms)';
  }

  $form['requested_interval'] = [
    '#type' => 'select',
    '#title' => t('Estimate length'),
    '#options' => $intervals,
    '#description' => t('Select the period of study you want the estimate to reflect.'),
    '#default_value' => isset($form_state['values']['requested_interval']) ? $form_state['values']['requested_interval'] : '',
  ];

  if ($student_career === 'GRD') {
    $form['requested_interval']['#description'] .= t('<br/><br/>Because graduate programs can vary in length based on the individual student, an estimate of the entire program cost is not available.');
  }

  if ($student_career === 'UG') {
    $form['form_of_study'] = [
      '#type' => 'radios',
      '#title' => t('System of study'),
      '#options' => _uw_student_fee_estimator_form_of_study(),
      '#default_value' => isset($form_state['values']['form_of_study']) ? $form_state['values']['form_of_study'] : 'REG',
    ];
  }

  if ($student_career === 'GRD') {
    $form['academic_load'] = [
      '#type' => 'radios',
      '#title' => t('Academic load'),
      '#options' => _uw_student_fee_estimator_academic_load(),
      '#default_value' => isset($form_state['values']['academic_load']) ? $form_state['values']['academic_load'] : 'FT',
    ];
  }

  return $form;
}

/**
 * Wizard step 3.
 */
function uw_student_fee_estimator_form_wizard_step_3($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $student_career = $form_state['step_information'][1]['stored_values']['student_career'];
  $citizenship = $form_state['step_information'][1]['stored_values']['citizenship'];

  $form_of_study = isset($form_state['step_information'][2]['stored_values']['form_of_study']) ? $form_state['step_information'][2]['stored_values']['form_of_study'] : NULL;
  $academic_load = isset($form_state['step_information'][2]['stored_values']['academic_load']) ? $form_state['step_information'][2]['stored_values']['academic_load'] : NULL;

  // Loading only published tuition records.
  $options = [
    'status' => 1,
  ];

  if ($student_career === 'UG' && $form_of_study) {
    $options['form_of_study'] = $form_of_study;
  }

  if ($student_career === 'GRD' && $academic_load) {
    $options['academic_load'] = $academic_load;
  }

  // We are querying tuition records and return tids
  // but using program's description.
  $tuition_programs = _uw_student_fee_estimator_program_select($student_career, $citizenship, $options);

  $form['tuition'] = [
    '#type' => 'select',
    '#title' => t('Select your program'),
    '#options' => $tuition_programs,
    '#default_value' => isset($form_state['values']['tuition']) ? $form_state['values']['tuition'] : '',
  ];

  $term_code = variable_get('uw_student_fee_estimator_current_term');
  $term_name = _uw_student_fee_estimator_term_decode($term_code);
  $term_year = (int)('20' . substr($term_code, 1, 2));
  $term_year_next = $term_year + 1;

  if ($citizenship === 'international') {
    if ($student_career === 'UG') {
      $form['tuition']['#description'] = t('Note that the Fee Estimator information is based on @term_year/@term_year_next tuition rates and assumes you are beginning your degree program in the @term_name term. If you are required to complete a preliminary English language training program (BASE, ELAS, EFAS) before entering your program, please be aware that
      <ul>
        <li>this estimate will not include the cost of the English language training program</li>
        <li>the actual cost of your program will be based on @term_year/@term_year_next tuition rates and will be higher than the estimate you are generating now</li></ul>', ['@term_name' => $term_name, '@term_year' => $term_year, '@term_year_next' => $term_year_next]);
    }
    elseif ($student_career === 'GRD') {
      $year = date('Y') . '/' . (date('Y') + 1);
      $term = _uw_student_fee_estimator_term_decode(variable_get('uw_student_fee_estimator_current_term'), False);

      $message = 'The Tuition Fee Estimator is based on ' . $year . ' Board of Governors’ approved tuition rates and assumes you will begin your program in the ' . $term . ' term.';
      $form['tuition']['#description'] = t($message);
    }
  }

  return $form;
}

/**
 * Wizard step 4 - Program details.
 */
function uw_student_fee_estimator_form_wizard_step_4($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $requested_interval = $form_state['step_information'][2]['stored_values']['requested_interval'];
  $tuition_id = $form_state['step_information'][3]['stored_values']['tuition'];

  if ($tuition_id) {
    $tuition = uw_student_fee_estimator_load_tuition($tuition_id);

    if ($tuition) {
      // Don't display this for term, display for program and year.
      if ($requested_interval === 'program') {
        $form['number_of_terms'] = [
          '#type' => 'select',
          '#title' => t('Program length (terms)'),
          '#options' => drupal_map_assoc(range(1, 10)),
          '#description' => t('Select the length of time you want the estimate to cover.'),
          '#default_value' => isset($form_state['values']['number_of_terms']) ? $form_state['values']['number_of_terms'] : 0,
        ];
      }

      if (isset($tuition['per_course']) && $tuition['per_course'] != 0) {
        $form['number_of_courses'] = [
          '#type' => 'select',
          '#title' => t('Number of .5-credit courses per term'),
          '#options' => drupal_map_assoc(range(1, 5)),
          '#description' => t('Select the number of courses you intend to take each term.'),
          '#default_value' => isset($form_state['values']['number_of_courses']) ? $form_state['values']['number_of_courses'] : 0,
        ];
      }

      // Limit or extend dropdown options for specific scenarios.
      if ($requested_interval === 'program') {
        if (isset($form['number_of_courses'])) {
          // Can't choose more than 10 courses for a program.
          $form['number_of_courses']['#options'] = drupal_map_assoc(range(1, 10));
        }
      }
      elseif ($requested_interval === 'year') {
        if (isset($form['number_of_terms'])) {
          // Can't choose more than 3 terms for a year estimate.
          $form['number_of_terms']['#options'] = drupal_map_assoc(range(1, 3));
        }

      }
    }
    else {
      drupal_set_message(t('There was an issue with loading tuition/program data.'), 'error');
    }
  }

  return $form;
}

/**
 * Wizard step 5.
 */
function uw_student_fee_estimator_form_wizard_step_5($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  $student_career = $form_state['step_information'][1]['stored_values']['student_career'];

  $residences = _uw_student_fee_estimator_form_residences_select($student_career);
  $residences = [0 => t('None (skip)')] + $residences;

  $form['residence'] = [
    '#type' => 'select',
    '#title' => t('Available residences'),
    '#description' => t('If you plan to live in residence, select your residence.'),
    '#options' => $residences,
    '#default_value' => isset($form_state['values']['residence']) ? $form_state['values']['residence'] : 0,
  ];

  return $form;
}

/**
 * Wizard step 6.
 */
function uw_student_fee_estimator_form_wizard_step_6($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $residence_id = isset($form_state['step_information'][5]['stored_values']['residence']) ? $form_state['step_information'][5]['stored_values']['residence'] : NULL;
  $meals = [0 => t('None (skipping meal plan)')];

  if ($residence_id) {
    $meals = uw_student_fee_estimator_form_meals_select($residence_id);
    $residence = uw_student_fee_estimator_load_residence($residence_id);

    if ($residence) {
      if ($residence['meal_plan_included']) {
        $meals = [
          0 => t('Included in residence fee'),
        ];
      }
      elseif (!$residence['meal_plan_required']) {
        $meals = [
          0 => t('None (skipping meal plan)'),
        ] + $meals;
      }
    }
  }

  $form['mealplan'] = [
    '#type' => 'select',
    '#title' => t('Meal plans'),
    '#description' => t('Select your meal plan.'),
    '#options' => $meals,
    '#default_value' => isset($form_state['values']['mealplan']) ? $form_state['values']['mealplan'] : 0,
  ];

  return $form;
}

/**
 * Wizard step 7.
 */
function uw_student_fee_estimator_form_wizard_step_7($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.tuition');

  $combined = uw_student_fee_estimator_form_wizard_step_7_clean_up($form_state['step_information']);
  $processed = uw_student_fee_estimator_tuition_calculate($combined);
  $form_state['calculated'] = $processed;
  $form_state['combined'] = $combined;

  // Remove tuition cost, leave total cost that is rounded number.
  $hide_tokens = [
    'tuition_cost', 'term_code', 'term_name', 'student_payment_options',
  ];
  foreach ($hide_tokens as $token) {
    unset($processed[$token]);
  }

  // Providing order for overview to display.
  $display_order = [
    'first_name' => t('First name'),
    'last_name' => t('Last name'),
    'student_email' => t('Email'),
    'student_number' => t('Student number'),
    'legal_status' => t('Tuition residency'),
    'student_career' => t('Academic career'),
    'requested_interval' => t('Estimate length'),
    'academic_load' => t('Academic load'),
    'program_name' => t('Program'),
    'residence_name' => t('Residence'),
    'residence_cost' => t('Residence cost'),
    'meal_plan_name' => t('Meal plan'),
    'meal_plan_cost' => t('Meal plan cost'),
    'total_cost' => '<strong>' . t('Total cost') . '</strong>',
  ];

  foreach ($display_order as $token => $value) {
    if (!empty($processed[$token])) {
      $form[$token] = [
        '#type' => 'textfield',
        '#title' => $value,
        '#default_value' => $processed[$token],
        '#disabled' => TRUE,
      ];

      switch ($token) {
        case 'student_email':
          $form[$token]['#description'] = t('This is where we will send the estimate letter.');
          break;

        case 'term_code':
          $form[$token]['#description'] = t('Estimated total cost for tuition, residence and meal plan selection.');
          break;

        case 'total_cost':
          $form[$token]['#prefix'] = '<div class="total-cost-container">';
          $form[$token]['#suffix'] = '</div>';
          $form[$token]['#description'] = t('This is an estimate only. It includes only fees that will be charged to the student\'s account at University of Waterloo.');
          $form[$token]['#default_value'] = '$' . $processed[$token] . ' CAD';
          $form[$token]['#attributes']['class'] = ['total-cost-field'];
      }
    }
  }

  return $form;
}

/**
 * Final step clean up before PDF generation.
 *
 * @param array|bool $steps
 *   All steps data.
 *
 * @return array
 *   Cleaned data/fields.
 */
function uw_student_fee_estimator_form_wizard_step_7_clean_up($steps) {
  $data = [];

  foreach ($steps as $step_information) {
    if (isset($step_information['stored_values'])) {
      unset(
        $step_information['stored_values']['form_id'],
        $step_information['stored_values']['form_token'],
        $step_information['stored_values']['form_build_id'],
        $step_information['stored_values']['op'],
        $step_information['stored_values']['next'],
        $step_information['stored_values']['prev']
      );

      $data += $step_information['stored_values'];
    }
  }

  // This can happen only when we skipped meal plan step,
  // when residence is not selected.
  if (!isset($data['mealplan'])) {
    $data['mealplan'] = '0';
  }

  return $data;
}

/**
 * Copy file from private to public temporary (deletion afterwards).
 *
 * This is done because sending file (attachment) from private subsystem is
 * not allowed for anonymous users. Permission exists that would allow
 * anonymous users to receive files from non-public folders, but that represent
 * security issue, granting users non-public file access. To overcome that,
 * PDF is copied from private to public folder, sending an email with file
 * attached (from public). Then deleting that public file, leaving
 * private intact.
 *
 * @param string $pdf_file
 *   Private PDF file uri.
 * @param array $submission_data
 *   Submission information, to recreate hash.
 *
 * @return bool
 *   On success url if public file, otherwise FALSE.
 */
function uw_student_fee_estimator_public_pdf($pdf_file, array $submission_data) {
  // Since user can be anonymous, and can not access generated PDF,
  // PDF file needs to be copied to public:// structure which deletes pddf
  // after email has been sent.
  $result = FALSE;

  $hash = substr(hash('md5', $submission_data['email'] . $submission_data['sid']), 0, 10);
  $filename = $hash . '.pdf';
  $file_path = 'public://sfe_temp/';

  if (file_prepare_directory($file_path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
    if (file_exists($pdf_file)) {
      $new_path = $file_path . $filename;
      $public_pdf_file = file_unmanaged_copy($pdf_file, $new_path);

      if ($public_pdf_file) {
        $result = $public_pdf_file;
      }
    }
  }

  return $result;
}

/**
 * Check if wizard should skip step 4 (program details).
 *
 * @param string $career
 *   Student career level, UG or GRD.
 * @param string $requested_interval
 *   Requested interval for estimate.
 * @param array $tuition
 *   Tuition database id record.
 *
 * @return bool
 *   True if program is coursed based, FALSE otherwise.
 */
function _uw_student_fee_estimator_form_wizard_skip_step_4($career, $requested_interval, $tuition) {
  $skip_step = FALSE;

  if ($career === 'GRD' && ($requested_interval === 'term' || $requested_interval === 'year')) {
    if ($tuition) {
      if (isset($tuition['per_course']) && (int) $tuition['per_course'] === 0) {
        $skip_step = TRUE;
      }
    }
  }
  elseif ($career === 'UG') {
    $skip_step = TRUE;
  }

  return $skip_step;
}
