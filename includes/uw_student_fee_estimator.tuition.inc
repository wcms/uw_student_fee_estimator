<?php

/**
 * @file
 * Tuition form and logic.
 */

/**
 * Tuition form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 * @param int|null $tuition
 *   Tuition record id if provided.
 *
 * @return mixed
 *   Tuition form.
 */
function uw_student_fee_estimator_admin_tuition_form($form, &$form_state, $tuition = NULL) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Tuition add/edit form'),
  ];

  $fields = _uw_student_fee_estimator_tuition_editable_fields($tuition);

  if ($tuition && isset($tuition['tid'])) {
    $form['group1']['tid'] = [
      '#type' => 'hidden',
      '#default_value' => isset($tuition['tid']) ? $tuition['tid'] : '',
    ];
  }

  $term_code = $tuition ? $tuition['term_code'] : NULL;

  $form['group1']['term_code'] = _uw_student_fee_estimator_current_term_field($term_code);

  $form['group1']['citizenship'] = [
    '#type' => 'radios',
    '#title' => t('Tuition residency'),
    '#options' => _uw_student_fee_estimator_citizenship(),
    '#default_value' => isset($tuition['citizenship']) ? $tuition['citizenship'] : 'ontarian',
    '#required' => TRUE,
  ];

  $form['group1']['career'] = [
    '#type' => 'radios',
    '#title' => t('Career'),
    '#options' => _uw_student_fee_estimator_career(),
    '#default_value' => isset($tuition['career']) ? $tuition['career'] : 'UG',
    '#required' => TRUE,
  ];

  $form['group1']['container1'] = [
    '#type' => 'container',
    '#prefix' => '<div id="program-details">',
    '#suffix' => '</div>',
  ];

  $form['group1']['container1']['program_description'] = [
    '#type' => 'textfield',
    '#title' => t('Program'),
    '#size' => 80,
    '#description' => t('Program description/label. Allows duplicates. Plain text only (no html).'),
    '#default_value' => isset($tuition['program_description']) ? $tuition['program_description'] : '',
    '#required' => TRUE,
  ];

  $form['group1']['container1']['online'] = [
    '#type' => 'checkbox',
    '#title' => t('Online course'),
    '#description' => t('If program is online only. Selecting this will remove residence and meal plan selection on estimator form.'),
    '#default_value' => $tuition ? $tuition['online'] : FALSE,
  ];

  $form['group1']['container1']['calc_formula'] = [
    '#type' => 'select',
    '#title' => t('Fee calculation formula'),
    '#options' => [
      'default' => t('Default'),
      'accounting' => t('Accounting'),
    ],
    '#description' => t('UG Only! Default formula calculates first year fee over the years. While Legacy uses each year data.'),
    '#default_value' => $tuition ? $tuition['calc_formula'] : 'default',
  ];

  $form['group1']['container1']['container-inline-1'] = [
    '#type' => 'container',
    '#attributes' => ['class' => ['container-inline']],
  ];

  $form['group1']['container1']['academic_load'] = [
    '#type' => 'radios',
    '#title' => t('Academic load'),
    '#options' => _uw_student_fee_estimator_academic_load(),
    '#default_value' => $tuition ? $tuition['academic_load'] : '',
    '#states' => [
      'visible' => [
        ':input[name="career"]' => array('value' => 'GRD'),
      ],
      'required' => [
        ':input[name="career"]' => array('value' => 'GRD'),
      ],
    ],
  ];

  $form['group1']['container1']['form_of_study'] = [
    '#type' => 'radios',
    '#title' => t('Form of study'),
    '#options' => _uw_student_fee_estimator_form_of_study(),
    '#default_value' => $tuition ? $tuition['form_of_study'] : '',
    '#states' => [
      'visible' => [
        ':input[name="career"]' => array('value' => 'UG'),
      ],
      'required' => [
        ':input[name="career"]' => array('value' => 'UG'),
      ],
    ],
  ];

  $vocab = taxonomy_vocabulary_machine_name_load(UW_STUDENT_FEE_ESTIMATOR_GRD_PROGRAM_TYPES);
  $terms = taxonomy_get_tree($vocab->vid);
  $term_options = [];

  foreach ($terms as $term) {
    $term_options[$term->tid] = $term->name;
  }

  $form['group1']['container1']['grd_type'] = [
    '#type' => 'select',
    '#title' => t('Type / Class'),
    '#options' => $term_options,
    '#states' => [
      'visible' => [
        ':input[name="career"]' => array('value' => 'GRD'),
      ],
    ],
    '#default_value' => $tuition ? $tuition['grd_type'] : '',
    '#disabled' => empty($term_options),
  ];

  $form['group1']['container1']['increase'] = [
    '#type' => 'textfield',
    '#title' => t('Increase'),
    '#default_value' => $tuition ? $tuition['increase'] : '',
    '#size' => 3,
    '#field_suffix' => '%',
    '#required' => TRUE,
  ];

  $form['group1']['container1']['pdf_template'] = [
    '#type' => 'select',
    '#title' => t('PDF Template'),
    '#options' => _uw_student_fee_estimator_pdf_templates_all(TRUE, TRUE, $term_code),
    '#default_value' => isset($tuition['pdf_template']) ? $tuition['pdf_template'] : _uw_student_fee_estimator_pdf_template_default(TRUE),
    '#description' => t('Select which PDF template to be used for sending notifications. If not selected default template will be used, check PDF templates configuration page for details on default template.'),
  ];

  $form['group1']['container1']['subgroup1'] = [
    '#type' => 'fieldset',
    '#title' => t('Tuition fee structure - UG'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#states' => [
      'visible' => [
        ':input[name="career"]' => ['value' => 'UG'],
      ],
    ],
  ];

  $form['group1']['container1']['subgroup1']['tuition_values'] = [
    '#tree' => TRUE,
    '#theme' => 'uw_student_fee_estimator_table',
  ];

  // Adding headers for first column named Group.
  $form['group1']['container1']['subgroup1']['tuition_values'][0]['R1'] = [
    '#type' => 'markup',
    '#markup' => 'Per course',
  ];

  $form['group1']['container1']['subgroup1']['tuition_values'][1]['R2'] = [
    '#type' => 'markup',
    '#markup' => 'Tuition values',
  ];

  $form['group1']['container1']['subgroup1']['tuition_values'][2]['R3'] = [
    '#type' => 'markup',
    '#markup' => 'Co-op values',
  ];

  $form['group1']['container1']['subgroup1']['tuition_values'][3]['R4'] = [
    '#type' => 'markup',
    '#markup' => 'Incidental fees values',
  ];

  foreach ($fields as $field_code => $field_value) {
    if (strpos($field_code, "IF") === 0) {
      $form['group1']['container1']['subgroup1']['tuition_values'][3][$field_code] = _uw_student_fee_estimator_tuition_field_generator($field_code, $field_value);
    }
    elseif (strpos($field_code, "WRM") === 0) {
      $form['group1']['container1']['subgroup1']['tuition_values'][2][$field_code] = _uw_student_fee_estimator_tuition_field_generator($field_code, $field_value);
    }
    elseif (strpos($field_code, "PC") === 0) {
      $form['group1']['container1']['subgroup1']['tuition_values'][0][$field_code] = _uw_student_fee_estimator_tuition_field_generator($field_code, $field_value);
    }
    else {
      $form['group1']['container1']['subgroup1']['tuition_values'][1][$field_code] = _uw_student_fee_estimator_tuition_field_generator($field_code, $field_value);
    }
  }

  $form['group1']['container1']['group2'] = [
    '#type' => 'fieldset',
    '#title' => t('Tuition calculated fees preview'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => [
      'visible' => [
        ':input[name="career"]' => ['value' => 'UG'],
      ],
    ],
  ];

  $form['group1']['container1']['group2']['refresh'] = [
    '#type' => 'submit',
    '#value' => t('Refresh table'),
    '#ajax' => [
      'callback' => '_uw_student_fee_estimator_tuition_preview_table_ajax',
      'wrapper' => 'tuition-calculations',
      'progress' => [],
      'event' => 'click',
    ],
    '#submit' => ['_uw_student_fee_estimator_tuition_preview_table'],
  ];

  if (isset($form_state['values']['tuition_rows'])) {
    $rows = $form_state['values']['tuition_rows'];
  }
  elseif ($tuition) {
    $calculated = uw_student_fee_estimator_tuition_calculate_total_cost($tuition);
    $rows = _uw_student_fee_estimator_tuition_prepare_table_rows($calculated);
  }
  else {
    $rows = [];
  }

  $header_terms = _uw_student_fee_estimator_program_terms(0, FALSE, TRUE);

  $form['group1']['container1']['group2']['table'] = [
    '#theme' => 'table',
    '#header' => ['Group'] + $header_terms,
    '#rows' => $rows,
    '#empty' => t('There is not enough data to render table.'),
    '#prefix' => '<div id="tuition-calculations">',
    '#suffix' => '</div>',
  ];

  $form['group1']['container1']['subgroup2'] = [
    '#type' => 'fieldset',
    '#title' => t('Tuition fee structure - GRD'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#states' => [
      'visible' => [
        ':input[name="career"]' => ['value' => 'GRD'],
      ],
    ],
  ];

  $form['group1']['container1']['subgroup2']['container-inline-2'] = [
    '#type' => 'container',
    '#attributes' => ['class' => ['container-inline']],
  ];

  $form['group1']['container1']['subgroup2']['container-inline-2']['per_term'] = [
    '#type' => 'textfield',
    '#title' => t('Per term'),
    '#default_value' => $tuition['per_term'] / 100,
    '#size' => 5,
  ];

  $form['group1']['container1']['subgroup2']['container-inline-2']['per_course'] = [
    '#type' => 'textfield',
    '#title' => t('Per course'),
    '#default_value' => $tuition['per_course'] / 100,
    '#size' => 5,
  ];

  $form['group1']['container1']['subgroup2']['container-inline-2']['per_term_inc'] = [
    '#type' => 'textfield',
    '#title' => t('Per term incidental'),
    '#default_value' => $tuition['per_term_inc'] / 100,
    '#size' => 5,
  ];

  $form['group1']['container1']['subgroup2']['container-inline-2']['work_term_per_course'] = [
    '#type' => 'textfield',
    '#title' => t('Work term per course fee'),
    '#default_value' => $tuition['work_term_per_course'] / 100,
    '#size' => 5,
  ];

  $form['group1']['container1']['subgroup2']['container-inline-2']['work_term_inc'] = [
    '#type' => 'textfield',
    '#title' => t('Work term incidental fees'),
    '#default_value' => $tuition['work_term_inc'] / 100,
    '#size' => 5,
  ];

  $form['group1']['container1']['status'] = [
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => isset($tuition['status']) ? $tuition['status'] : FALSE,
    '#description' => t('Unpublished tuition records will not be visible to students as option.'),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
  ];

  return $form;
}

/**
 * Form validate handler.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_tuition_form_validate($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  $values = $form_state['values'];

  $fields = [
    'per_term', 'per_course', 'per_term_inc',
    'work_term_per_course', 'work_term_inc',
  ];

  foreach ($fields as $field) {
    if (empty($values[$field])) {
      $form_state['values'][$field] = 0;
    }
  }

  if (!is_numeric($values['increase'])) {
    form_set_error('increase', t('Increase must be numeric value.'));
  }

  $career = $values['career'];

  if ($career === 'UG') {
    $form_state['values']['grd_type'] = 0;
    $form_state['values']['academic_load'] = '';
  }
  elseif ($career === 'GRD') {
    $form_state['values']['form_of_study'] = '';
  }

}

/**
 * Tuition admin form submit handler.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_tuition_form_submit($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  form_state_values_clean($form_state);
  $values = $form_state['values'];
  $message = t('There was an issue with saving tuition record');
  $message_type = 'error';

  foreach ($values['tuition_values'] as $row => $group) {
    foreach ($group as $key => $value) {
      if (is_numeric($value)) {
        $values['tuition_values'][$row][$key] = (int)($value * 100);
      }
    }
  }

  // Convert values to cents before storing them.
  foreach (['per_term', 'per_course', 'per_term_inc', 'work_term_per_course', 'work_term_inc'] as $field) {
    if (!empty($values[$field])) {
      $values[$field] = (int)($values[$field] * 100);
    }
  }

  $values = uw_student_fee_estimator_tuition_calculate_total_cost($values);

  // When updating an existing tuition record,
  // check for program connection and update it.
  if (isset($values['tid'])) {
    $stored_record = uw_student_fee_estimator_load_tuition($values['tid']);

    // Update an existing tuition records.
    if ($stored_record && _uw_student_fee_estimator_tuition_merge($values)) {
      $message = t('Tuition record updated successfully.');
      $message_type = 'status';
    }
  }
  else {
    if ($new_tid = _uw_student_fee_estimator_tuition_merge($values)) {
      $message = t('Tuition successfully saved.');
      $message_type = 'status';
    }
  }

  drupal_set_message($message, $message_type);
  $form_state['redirect'] = 'admin/student-fee-estimator/tuition';
}

/**
 * Delete form for tuition record.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 * @param int $tuition
 *   Tuition record id.
 *
 * @return mixed
 *   Conformation form for delete.
 */
function uw_student_fee_estimator_admin_tuition_delete_form($form, &$form_state, $tuition) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  if (!$tuition || !isset($tuition['tid']) || !ctype_alnum($tuition['tid'])) {
    return $form;
  }

  $form['tid'] = [
    '#type' => 'value',
    '#value' => $tuition['tid'],
  ];

  $citizenship = _uw_student_fee_estimator_citizenship($tuition['citizenship']);

  $form = confirm_form($form,
    t('Are you sure you want to delete tuition with id: @id?', ['@id' => $tuition['tid']]),
    '/admin/student-fee-estimator/tuition',
    t('You are deleting tuition record for<strong>@citizenship</strong> students. This action cannot be undone.', ['@citizenship' => $citizenship]),
    t('Delete'),
    t('Cancel')
  );

  return $form;
}

/**
 * Actual delete form form tuition.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_tuition_delete_form_submit($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  $values = $form_state['values'];
  $tid = isset($values['tid']) ? $values['tid'] : NULL;

  if ($tid && is_numeric($tid)) {
    $result = uw_student_fee_estimator_tuition_delete($tid);

    if ($result) {
      drupal_set_message(t('Tuition record was successfully deleted.'));
    }

    $form_state['redirect'] = '/admin/student-fee-estimator/tuition';
  }
}

/**
 * Generates list of fields that are editable by admin/user.
 *
 * @param int|null $tuition
 *   Tuition record id.
 *
 * @return array
 *   List of editable fields for tuition as array.
 */
function _uw_student_fee_estimator_tuition_editable_fields($tuition = NULL) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $fields = _uw_student_fee_estimator_program_terms(0, TRUE);

  if ($tuition && is_array($tuition)) {
    $fields = array_intersect_key(array_merge($fields, $tuition), $fields);
  }
  else {
    $fields = array_fill_keys(array_keys($fields), '');
  }

  return $fields;
}

/**
 * Prepared form fields to be used in a table.
 *
 * @param string $field_name
 *   Field name for generator.
 * @param string $field_value
 *   Field value for generator.
 *
 * @return array
 *   Form API field definition.
 */
function _uw_student_fee_estimator_tuition_field_generator($field_name, $field_value) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $table_field = [
    '#default_value' => $field_value / 100,
  ];

  switch ($field_name) {
    case 'tid':
      $table_field['#type'] = 'hidden';
      break;

    case 'status':
      $table_field['#type'] = 'checkbox';
      break;

    case 'citizenship':
      $table_field['#type'] = 'select';
      $table_field['#options'] = _uw_student_fee_estimator_citizenship();
      break;

    default:
      $table_field['#type'] = 'textfield';
      $table_field['#size'] = 5;
  }

  return $table_field;
}

/**
 * Calculates sums for each academic year as well as total.
 *
 * Used when displaying preview table on tuition form.
 *
 * @param array $values
 *   Fields that needs to be summed.
 *
 * @return array
 *   Resulting sum.
 */
function uw_student_fee_estimator_tuition_calculate_total_cost(array $values) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $calculated = $values;
  $academic_year = range(1, 5);

  // Check if tuition record is an argument,
  // or tuition table preview is invoking this function.
  if (isset($calculated['tuition_values'])) {
    $fees = array_map('floatval', flatten_array($calculated['tuition_values']));
  }
  else {
    $fees = array_map('floatval', flatten_array($calculated));
  }

  foreach (['T', 'WRM', 'IF'] as $prefix) {
    $grand_total = 0;
    foreach ($academic_year as $year) {
      // Use prefix, append year and check array values.
      $current = $prefix . $year;
      $year_total = (isset($fees[$current . 'A']) ? $fees[$current . 'A'] : 0) + (isset($fees[$current . 'B']) ? $fees[$current . 'B'] : 0);
      $calculated[$current] = $year_total;
      $grand_total += $year_total;
    }

    $calculated[$prefix . '_total'] = $grand_total;
  }

  // Merge fees array to calculated array.
  $calculated += $fees;

  if (isset($calculated['tuition_values'])) {
    unset($calculated['tuition_values']);
  }

  return $calculated;
}

/**
 * Ajax callback for the tuition form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 *
 * @return mixed
 *   Section of the form that needs to be rebuild.
 */
function _uw_student_fee_estimator_tuition_preview_table_ajax($form, &$form_state) {
  return $form['group1']['container1']['group2']['table'];
}

/**
 * Calculates year totals, and grand totals based on provided values.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function _uw_student_fee_estimator_tuition_preview_table($form, &$form_state) {
  $values = $form_state['values'];
  $calculated = uw_student_fee_estimator_tuition_calculate_total_cost($values);
  $rows = _uw_student_fee_estimator_tuition_prepare_table_rows($calculated, TRUE);

  $form_state['values']['tuition_rows'] = $rows;
  $form_state['rebuild'] = TRUE;
}

/**
 * Table preview rows refresh.
 *
 * @param array $values
 *   Tuition fee values as array.
 * @param bool $in_dollars
 *   Values already in dollars (eg. When pulling from form).
 *
 * @return array
 *   Formatted output.
 */
function _uw_student_fee_estimator_tuition_prepare_table_rows(array $values, $in_dollars = FALSE) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $rows = [];
  $rows_placeholders = uw_student_fee_estimator_fee_table_rows_prepare();

  foreach ($rows_placeholders as $index => $single_row) {
    $row_in_cents = array_intersect_key(array_merge($single_row, $values), $single_row);

    if ($in_dollars) {
      $row_in_dollars = $row_in_cents;
    }
    else {
      $row_in_dollars = _uw_student_fee_estimator_convert_to_dollar_amount($row_in_cents);
    }

    $rows[$index] = $row_in_dollars;
  }

  // Calculating column sums.
  $terms = _uw_student_fee_estimator_program_terms(0, FALSE, TRUE);

  $column_total = [];

  foreach ($terms as $term) {
    if ($term !== 'Total') {
      $sum = $rows[0]['T' . $term] + $rows[1]['WRM' . $term] + $rows[2]['IF' . $term];
      $column_total['CT' . $term] = $sum;
    }
  }

  $column_total['total'] = $rows[0]['T_total'] + $rows[1]['WRM_total'] + $rows[2]['IF_total'];
  $rows[] = $column_total;

  $formatted_rows = [];
  foreach ($rows as $index => $row) {
    switch ($index) {
      case 0:
        $label = t('Tuition values');
        break;

      case 1:
        $label = t('Co-op values');
        break;

      case 2:
        $label = t('Incidental values');
        break;

      case 3:
        $label = strtoupper(t('Column total'));
        break;

      default:
        $label = t('Unknown');
    }

    $formatted_rows[$index] = ['H' . $index => $label] + array_map('prepare_fee_amount', $row);
  }

  return $formatted_rows;
}

/**
 * Convert cents values to dollar values.
 *
 * @param array $values
 *   Preview table row in cent values.
 *
 * @return array
 *   Array in dollar values.
 */
function _uw_student_fee_estimator_convert_to_dollar_amount(array $values) {
  $dollar_values = [];

  foreach ($values as $key => $value) {
    $dollar_values[$key] = (int)$value / 100;
  }

  return $dollar_values;
}

/**
 * Central function for tuition calculation.
 *
 * @param array $submission
 *   Submission values array with following structure (example):
 *    $submission = [
 *    [name] => Jane Doe
 *    [email] => jane@doe.ca
 *    [student_number] => 12345678
 *    [citizenship] => international | canadian | ontarian
 *    [student_career] => UG | GRD
 *    [requested_interval] => program | year | term
 *    [form_of_study] => REG | CO-OP
 *    [academic_load] => PT | FT
 *    [tuition] => 46
 *    [number_of_courses] => 4
 *    [number_of_terms] => 5
 *    [residence] => 2
 *    [mealplan] => 3
 *    ].
 *
 * @return array
 *   Calculated fees for tuition, residence and meal plan as an array.
 */
function uw_student_fee_estimator_tuition_calculate(array $submission) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $token_values = uw_student_fee_estimator_tokens_values($submission);

  return $token_values;
}
