<?php

/**
 * @file
 * CSV import logic.
 */

/**
 * Import form.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 *
 * @return mixed
 *   Import form array.
 */
function uw_student_fee_estimator_admin_import_data_form($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Import programs & tuition data'),
  ];

  $form['group1']['csv_file_res'] = [
    '#type' => 'file',
    '#title' => t('Residence'),
    '#description' => t('Import Residence CSV template.'),
  ];

  $form['group1']['csv_file_ug'] = [
    '#type' => 'file',
    '#title' => t('Undergraduates'),
    '#description' => t('Import UG CSV template.'),
  ];

  $form['group1']['csv_file_grd'] = [
    '#type' => 'file',
    '#title' => t('Graduates'),
    '#description' => t('Import GRD CSV template.'),
  ];

  $form['group1']['header_exists'] = [
    '#type' => 'checkbox',
    '#title' => t('Files have header?'),
    '#description' => t('Check if files contains header, it will skip first row. Otherwise first line will be considered data as well.'),
    '#default_value' => TRUE,
  ];

  $form['group1']['delete_all'] = [
    '#type' => 'checkbox',
    '#title' => t('Delete all existing data prior import'),
    '#description' => t('When selected this will drop all data form all tables prior to import. This is recommended for new term data.'),
    '#default_value' => FALSE,
  ];

  $form['group1']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Upload and process'),
  ];

  return $form;
}

/**
 * Validate form handler.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_import_data_form_validate($form, &$form_state) {
  $all_files = [
    'csv_file_res' => 'Residence / Meal plan',
    'csv_file_ug' => 'Undergraduate',
    'csv_file_grd' => 'Graduate',
  ];

  foreach ($all_files as $file => $label) {
    $upload = file_save_upload($file, [
      // Validates file's extension.
      'file_validate_extensions' => ['csv'],
    ]);

    if ($upload && $moved = file_move($upload, 'private://')) {
      $form_state['storage'][$file] = $moved;
    }
//    else {
//      form_set_error($file, t("Failed to write the uploaded @file file to the site's file folder.", ['@file' => $label]));
//    }
  }
}

/**
 * Form submit handler.
 *
 * @param array|bool $form
 *   Form fields.
 * @param array|bool $form_state
 *   Form values.
 */
function uw_student_fee_estimator_admin_import_data_form_submit($form, &$form_state) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');

  $skip_header = $form_state['values']['header_exists'];
  $delete_all = $form_state['values']['delete_all'];
  $count = 0;

  // Creating transaction, once variable goes out of scope it gets
  // automatically committed.
  $transaction = db_transaction();

  if ($delete_all) {
    _uw_student_fee_estimator_delete_all_records();
    drupal_set_message(t('All records have been deleted'));
  }

  $all_files = [
    'csv_file_res' => '_uw_student_fee_estimator_import_resmeal_records',
    'csv_file_ug' => '_uw_student_fee_estimator_import_tuition_records',
    'csv_file_grd' => '_uw_student_fee_estimator_import_tuition_records',
  ];

  foreach ($all_files as $file_key => $file_process_function) {
    if (!empty($form_state['storage'][$file_key]) && $file = $form_state['storage'][$file_key]) {
      unset($form_state['storage'][$file_key]);

      try {
        $count += call_user_func($file_process_function, $file, $skip_header);
      }catch (Exception $ex) {
        // In case of an exception roll back transaction.
        $transaction->rollback();
      }

      drupal_set_message(t('Successfully created @count new records.', ['@count' => $count]));

      // Clean up.
      file_delete($file);
    }
  }

  $form_state['redirect'] = 'admin/student-fee-estimator/tuition/import';
}


function _uw_student_fee_estimator_import_tuition_records($file, $skip_header) {
  $tuition_count = 0;
  $handler = fopen($file->uri, 'r');
  // Used to keep track when there is an exception.
  // Error message will have this number displayed.
  $row_number = 0;

  $default_template = _uw_student_fee_estimator_pdf_template_default(TRUE);
  $current_term = variable_get('uw_student_fee_estimator_current_term');

  if ($handler) {
    while (($data = fgetcsv($handler, 1000)) !== FALSE) {
      $row_number++;

      // Skip header only once.
      if ($skip_header) {
        $skip_header = FALSE;
        continue;
      }

      try {
        $tuition = _uw_student_fee_estimator_import_parse_tuition($data, $current_term, $default_template);

        if ($tuition && is_array($tuition)) {
          if ($tid = _uw_student_fee_estimator_tuition_merge($tuition)) {
            $tuition_count++;
          }
        }
      }
      catch (Exception $ex) {
        // Do some logging and skip record, move to next one.
        $row_error = array_slice($data, 0, 4);
        drupal_set_message(
          t('There was an error while parsing @row_num row of tuition/program data. Skipping record. Data: @data.',
            ['@row_num' => $row_number, '@data' => implode(' ', $row_error)]),
          'error'
        );
      }

      if (!$tuition_count === 0) {
        throw new Exception('No data imported. Stopping import.');
      }
    }
  }

  return $tuition_count;
}

function _uw_student_fee_estimator_import_resmeal_records($file, $skip_header) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.database');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.tuition');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');
  $residence_count = 0;
  $meal_plan_count = 0;
  $row_number = 0;
  $rooms_end_pos = 9;
  $meal_plan_start_pos = 11;
  $meal_plan_end_pos = 15;

  $handler = fopen($file->uri, 'r');
  $header = NULL;

  if ($handler) {
    while (($data = fgetcsv($handler, 1000)) !== FALSE) {
      $row_number++;

      if (!$header) {
        $header = $data;

        $separator = array_search('Meal Plan Required?', $header);
        if (!$separator) {
          $separator = array_search('Meal Plan Required', $header);
        }

        if ($separator) {
          $rooms_end_pos = $separator - 1;
          $meal_plan_start_pos = $separator + 1;
          $meal_plan_end_pos = count($header) - 1;
        }

        continue;
      }

      try {
        $residence = [
          'status' => 1,
          'res_name' => check_plain($data[0]),
          'meal_plan_included' => 0,
          'meal_plan_required' => 0,
        ];

        for ($i = 1; $i <= $rooms_end_pos; $i++) {
          if ($data[$i]) {
            $residence['name'] = $residence['res_name'] . ' - ' . check_plain($header[$i]);
            $residence['price'] = _uw_student_fee_estimator_parse_decimal($data, $i);
            $meal_plan_required = strtoupper(check_plain($data[10])) === 'YES' ? 1 : 0;
            $residence['meal_plan_required'] = $meal_plan_required;
            $residence['meal_plans'] = [];

            // Get meal plans, try to load meal plan based on name first.
            for ($y = $meal_plan_start_pos; $y <= $meal_plan_end_pos; $y++) {
              $meal_plan_price = _uw_student_fee_estimator_parse_decimal($data, $y);
              $meal_plan_name = check_plain($header[$y]);

              if ($meal_plan_price > 0) {
                $existing_meal_plan = uw_student_fee_estimator_load_meal_plan(NULL, ['fields' => ['name' => $meal_plan_name]]);

                if (!$existing_meal_plan) {
                  $existing_meal_plan['mid'] = _uw_student_fee_estimator_mealplan_merge(
                    [
                      'name' => $meal_plan_name,
                      'price' => $meal_plan_price,
                      'status' => 1,
                    ]
                  );
                  $meal_plan_count++;
                }
                else {
                  // Compare prices and update meal plan if meal plan exists.
                  if ($existing_meal_plan['price'] != $meal_plan_price) {
                    $existing_meal_plan['price'] = $meal_plan_price;
                    _uw_student_fee_estimator_mealplan_merge($existing_meal_plan);
                  }
                }

                if (isset($existing_meal_plan['mid'])) {
                  $residence['meal_plans'][] = $existing_meal_plan['mid'];
                }
              }
            }
          }
          else {
            continue;
          }

          _uw_student_fee_estimator_residence_merge($residence);
          $residence_count++;
        }
      }
      catch (Exception $ex) {
        $row_error = array_slice($data, 0, 4);
        drupal_set_message(t('There was an error with @row row number. Data: @data',
          ['@row' => $row_number, '@data' => implode(' ', $row_error)]));
      }
    }
  }

  if (!$residence_count || !$meal_plan_count) {
    throw new Exception('No residence data imported. Stopping import.');
  }

  return $residence_count + $meal_plan_count;
}

/**
 * Parsing CSV row.
 *
 * @param array $data
 *   Row data form file.
 *
 * @param string $current_term
 *   Current term.
 * @param $default_template
 *   Default pdf template.
 * @return array
 *   Parsed data.
 *
 * @throws \Exception If validation fails.
 */
function _uw_student_fee_estimator_import_parse_tuition(array $data, $current_term, $default_template) {
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.tuition');
  module_load_include('inc', 'uw_student_fee_estimator', 'includes/uw_student_fee_estimator.common');

  $row_career = _uw_student_fee_estimator_import_career($data[0]);

  $tuition = [
    'status' => 1,
    'term' => $current_term,
    'career' => $row_career,
    'citizenship' => _uw_student_fee_estimator_import_citizenship($data[1]),
    'increase' => _uw_student_fee_estimator_parse_decimal($data, 2, 1),
    'online' => $data[3],
    'program_description' => $data[4],
  ];

  if ($row_career === 'UG') {
    $tuition['form_of_study'] = _uw_student_fee_estimator_import_form_of_study($data[5]);

    if (!$tuition['form_of_study']) {
      throw new Exception(t('Missing required part: Form of study. Skipping data row.'));
    }

    for ($i = 1; $i <= 5; $i++) {
      $tuition['PC' . $i . 'A'] = _uw_student_fee_estimator_parse_decimal($data, (4 + (2 * $i)));
      $tuition['PC' . $i . 'B'] = _uw_student_fee_estimator_parse_decimal($data, (5 + (2 * $i)));

      $tuition['T' . $i . 'A'] = _uw_student_fee_estimator_parse_decimal($data, (14 + (2 * $i)));
      $tuition['T' . $i . 'B'] = _uw_student_fee_estimator_parse_decimal($data, (15 + (2 * $i)));

      $tuition['WRM' . $i . 'A'] = _uw_student_fee_estimator_parse_decimal($data, (24 + (2 * $i)));
      $tuition['WRM' . $i . 'B'] = _uw_student_fee_estimator_parse_decimal($data, (25 + (2 * $i)));

      $tuition['IF' . $i . 'A'] = _uw_student_fee_estimator_parse_decimal($data, (34 + (2 * $i)));
      $tuition['IF' . $i . 'B'] = _uw_student_fee_estimator_parse_decimal($data, (35 + (2 * $i)));
    }
  }
  elseif ($row_career === 'GRD') {
    $tuition['grd_type'] = _uw_student_fee_estimator_import_grd_type($data[5]);
    $tuition['academic_load'] = _uw_student_fee_estimator_import_academic_load($data[6]);
    $tuition['per_term'] = _uw_student_fee_estimator_parse_decimal($data, 7);
    $tuition['per_course'] = _uw_student_fee_estimator_parse_decimal($data, 8);
    $tuition['per_term_inc'] = _uw_student_fee_estimator_parse_decimal($data, 9);
    $tuition['work_term_per_course'] = _uw_student_fee_estimator_parse_decimal($data, 10);
    $tuition['work_term_inc'] = _uw_student_fee_estimator_parse_decimal($data, 11);

    if (!$tuition['academic_load']) {
      throw new Exception(t('Missing required part: Academic load. Skipping data row.'));
    }

    if ($tuition['per_term'] === 0 && $tuition['per_course'] === 0) {
      throw new Exception(t('Missing fees for GRD tuition record.'));
    }
  }

  $tuition = uw_student_fee_estimator_tuition_calculate_total_cost($tuition);
  $tuition['pdf_template'] = $default_template;

  return $tuition;
}

/**
 * Importing GRD program type.
 *
 * @param string $value
 *   Name of grd type.
 *
 * @return int|null
 *   Vocabulary term id if found.
 */
function _uw_student_fee_estimator_import_grd_type($value) {
  $term_id = NULL;

  if (!empty($value)) {
    $term = taxonomy_get_term_by_name($value, UW_STUDENT_FEE_ESTIMATOR_GRD_PROGRAM_TYPES);

    if ($term) {
      $term = reset($term);
      $term_id = $term->tid;
    }
    else {
      // Create new taxonomy term and add it to existing vocabulary.
      $vocab = taxonomy_vocabulary_machine_name_load(UW_STUDENT_FEE_ESTIMATOR_GRD_PROGRAM_TYPES);

      if ($vocab) {
        $new_term = new stdClass();
        $new_term->name = check_plain($value);
        $new_term->vid = $vocab->vid;

        taxonomy_term_save($new_term);

        $term_id = $new_term->tid;
      }
    }
  }

  return $term_id;
}

/**
 * Parsing career input field.
 *
 * @param string $code_raw
 *   Value from file.
 *
 * @return mixed|null
 *   If paring successful parse value, NULL otherwise.
 * @throws \Exception
 */
function _uw_student_fee_estimator_import_career($code_raw) {
  $code = strtoupper($code_raw);
  $synonyms = [
    'UG' => 'UG',
    'GRD' => 'GRD',
    'U' => 'UG',
    'G' => 'GRD',
    'GRAD' => 'GRD',
    'UGRAD' => 'UG',
  ];

  if (!isset($synonyms[$code])) {
    throw new Exception('Career level not recognized.');
  }

  return $synonyms[$code];
}

/**
 * Parsing citizenship input field.
 *
 * @param string $code_raw
 *   Value from file.
 *
 * @return mixed|null
 *   If paring successful parse value, NULL otherwise.
 * @throws \Exception
 */
function _uw_student_fee_estimator_import_citizenship($code_raw) {
  $code = strtoupper($code_raw);

  if ($code[0] === 'C') {
    $result = 'canadian';
  }
  elseif ($code[0] === 'I') {
    $result = 'international';
  }
  elseif ($code[0] === 'O') {
    $result = 'ontarian';
  }
  else {
    throw new Exception('Tuition residency not recognized.');
  }

  return $result;
}

/**
 * Parsing form of study input field.
 *
 * @param string $code_raw
 *   Value from file.
 *
 * @return mixed|null
 *   If paring successful parse value, NULL otherwise.
 * @throws \Exception
 */
function _uw_student_fee_estimator_import_form_of_study($code_raw) {
  $code = strtoupper($code_raw);

  if ($code[0] === 'R') {
    $result = 'REG';
  }
  elseif ($code[0] === 'C') {
    $result = 'CO-OP';
  }
  else {
    throw new Exception('Form of study field not recognized.');
  }

  return $result;
}

/**
 * Parsing academic load input field.
 *
 * @param string $code_raw
 *   Value from file.
 *
 * @return mixed|null
 *   If paring successful parse value, NULL otherwise.
 * @throws \Exception
 */
function _uw_student_fee_estimator_import_academic_load($code_raw) {
  $code = strtoupper($code_raw);

  if ($code[0] === 'F') {
    $result = 'FT';
  }
  elseif ($code[0] === 'P') {
    $result = 'PT';
  }
  else {
    throw new Exception('Academic field not recognized.');
  }

  return $result;
}

/**
 * Parsing numeric field.
 *
 * @param array|bool $row
 *   Row data from file.
 * @param int $position
 *   Position of numeric field.
 * @param int $factor
 *   Factor that is used to multiply value. Default is 100 (cents).
 *
 * @return float|int
 *   Parsed value.
 */
function _uw_student_fee_estimator_parse_decimal($row, $position, $factor = 100) {
  $value = 0;

  if (isset($row[$position]) && is_numeric($row[$position])) {
    // Transforming to cents from dollars.
    $value = intval($row[$position] * $factor);
  }

  return $value;
}
