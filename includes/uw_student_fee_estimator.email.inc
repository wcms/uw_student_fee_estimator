<?php

/**
 * @file
 * Email admin tab form.
 */

/**
 * Admin email form.
 *
 * @param array $form
 *   Admin configuration form.
 * @param array $form_state
 *   Form values and other controls.
 *
 * @return mixed
 *   Form for admin.
 */
function uw_student_fee_estimator_admin_email_form(array $form, array &$form_state) {
  $form['group0'] = [
    '#type' => 'fieldset',
    '#title' => t('Email settings and template'),
  ];

  $form['group0']['uw_student_fee_estimator_email_from'] = [
    '#type' => 'textfield',
    '#title' => t('From email address'),
    '#description' => t('This address will be used as from address on emails sent by application.'),
    '#default_value' => variable_get('uw_student_fee_estimator_email_from', 'sfs@uwateroo.ca'),
    '#required' => TRUE,
  ];

  $form['group0']['uw_student_fee_estimator_email_subject'] = [
    '#type' => 'textfield',
    '#title' => t('Email subject'),
    '#description' => t('Subject of email message/notification.'),
    '#default_value' => variable_get('uw_student_fee_estimator_email_subject', t('University of Waterloo - Student Fee Estimator')),
    '#size' => 80,
    '#required' => TRUE,
  ];

  $form['group0']['uw_student_fee_estimator_email_body'] = [
    '#type' => 'textarea',
    '#title' => t('Email body'),
    '#default_value' => variable_get('uw_student_fee_estimator_email_body', t('Thank you for using estimator. Your submission id is @sub. You will find PDF attached.')),
    '#description' => t('Use plain text only, HTML is not supported. Only one placeholder is available (@sub - submission id). Email body is the same for all submissions, regardless of program, tuition etc.'),
  ];

  return system_settings_form($form);
}

/**
 * Validation function for email configuration form.
 *
 * @param array $form
 *   Form definition, including fields.
 * @param array $form_state
 *   Form fields values.
 */
function uw_student_fee_estimator_admin_email_form_validate(array $form, array &$form_state) {
  $values = $form_state['values'];

  if (!filter_var($values['uw_student_fee_estimator_email_from'], FILTER_VALIDATE_EMAIL)) {
    form_set_error('uw_student_fee_estimator_email_from', t('Invalid email address. Please provide valid email address.'));
  }
}

/**
 * Email notification.
 *
 * @param string $student_email
 *   Destination email.
 * @param string $pdf_file
 *   URI for generated PDF file.
 * @param string $submission_id
 *   Submission id.
 *
 * @return mixed
 *   Result of email send action.
 */
function uw_student_fee_estimator_email_notification($student_email, $pdf_file, $submission_id) {
  global $language;

  $to = $student_email;

  if (variable_get('uw_student_fee_estimator_email_override_status') &&
    $email_override = variable_get('uw_student_fee_estimator_email_override_email')
  ) {
    $to = $email_override;
  }

  $subject = variable_get('uw_student_fee_estimator_email_subject', 'University of Waterloo - Student Fee Estimator');
  $body = nl2br(variable_get('uw_student_fee_estimator_email_body', 'Thank you for using estimator. Your submission id is @sub. You will find PDF attached.'));

  // Check conditions for sending notification, if true send notification.
  $attachment = [
    'filecontent' => drupal_realpath($pdf_file),
    'filename' => 'UW_Fee_Estimate.pdf',
    'filemime' => 'application/pdf',
  ];

  $params = [
    'key' => 'pdf-estimate',
    'to' => $student_email,
    'subject' => check_markup($subject, 'uw_tf_basic'),
    'body' => t(check_markup($body, 'uw_tf_basic'), ['@sub' => $submission_id]),
    'attachment' => $attachment,
  ];

  $from = variable_get('uw_student_fee_estimator_email_from', 'sfs@uwateroo.ca');
  $result = drupal_mail('uw_student_fee_estimator', 'pdf-estimate', $to, $language->language, $params, $from);

  return $result;
}
